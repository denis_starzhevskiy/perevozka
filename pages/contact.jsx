import React, { useState } from 'react'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import { Box, Button, Container, Typography } from '@mui/material'
import Contacts from '@/components/contacts/Contacts'
import Footer from '@/components/layout/Footer'
import Image from 'next/image'
import contact1 from '../public/images/contactPages1.png'
import contact2 from '../public/images/contactPages2.png'
import contact3 from '../public/images/contactPages3.png'
import contact4 from '../public/images/contactPages4.png'
import contact5 from '../public/images/contactPages5.png'
import contact6 from '../public/images/contactPages6.png'
import contact7 from '../public/images/contactPages7.png'
import Ukraine from '../public/images/Ukraine.png'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import CountrySlovak from '../public/images/CountrySlovak.png'

const Contact = () => {
  const [openDialog, setOpenDialog] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
    <Header />
     <Head>
           <title>Контакти</title>
     </Head>
     <BreadcrumbsComponent
         path={[
            {
             label: 'Головна',
             link: '/',
            },
            {
            label: 'Контакти',
            link: '/blog',
           },
         ]}
     />
      <Contacts />
      <Box sx={{backgroundColor:'white',paddingTop:'40px'}}>
        <Container sx={{
          display: 'flex',
          alignItems: 'start',
          flexDirection: 'column',
          padding: '40px',
          rowGap: '10px',
        }}
        >
          <Typography
            variant="h1"
            component="h1"
            sx={{
              color: 'rgb(0,0,0)',
              marginBottom:'20px',
              fontSize: '20px',
              fontStyle: 'normal',
              fontWeight: 600,
              lineHeight: 'normal',
              '@media (min-width: 900px)': {
                fontSize: '30px',
              },
            }}
          >
            Офіси
          </Typography>
          <Box sx={{display:'grid',gridTemplateColumns:{xs:'1fr', md:'1fr 1fr 1fr'},rowGap:'20px',columnGap:'20px'}}>
            <Box
              sx={{
                display: 'flex',
                width:{xs:'335px', md:'400px'},
                height:{xs:'335px', md:'400px'}
              }}
            >
              <Image src={contact1.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            </Box>
            <Box
              sx={{
                display: 'flex',
                width:{xs:'335px', md:'400px'},
                height:{xs:'335px', md:'400px'}
              }}
            >
              <Image src={contact5.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            </Box>
            <Box
              sx={{
                paddingLeft:'10px',
                display: 'flex',
                flexDirection:'column',
                rowGap:'10px',
                justifyContent:'center',
                fontSize:'14px',
                fontWeight:'700'
              }}
            >
              <Typography sx={{color:'black', display:'flex',columnGap:'10px',fontWeight:700}}>
                <Box component={'img'} src={Ukraine.src} width={'25px'} height={'25px'}></Box>
                Україна
              </Typography>
              <Typography sx={{color:'black', display:'flex',columnGap:'10px',fontSize:'13px',marginBottom:'10px'}}>
                м. Івано-Франківськ, Північний бульвар 12
              </Typography>
              <Button
                fullWidth
                sx={{
                  backgroundColor: 'rgba(249, 205, 150, 1)',
                  borderRadius: '0px',
                  color: 'rgb(3,0,0)',
                  textTransform: 'none',
                  fontSize: '14px',
                  fontWeight: '600',
                  padding: '20px 30px',
                  '&:hover': { background: 'rgba(241,182,132,0.44)' },
                }}
                onClick={() => setOpenDialog(true)}
              >
                {intl.$t({ id: 'general.contactForm.button' })}
              </Button>
            </Box>
          </Box>
          <Box sx={{display:'grid',gridTemplateColumns:{xs:'1fr', md:'1fr 1fr 1fr'},rowGap:'20px',columnGap:'20px',paddingTop:'30px'}}>
            <Box
              sx={{
                display: 'flex',
                width:{xs:'335px', md:'400px'},
                height:{xs:'335px', md:'400px'}
              }}
            >
              <Image src={contact3.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            </Box>
            <Box
              sx={{
                display: 'flex',
                width:{xs:'335px', md:'400px'},
                height:{xs:'335px', md:'400px'}
              }}
            >
              <Image src={contact7.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            </Box>
            <Box
              sx={{
                paddingLeft:'10px',
                display: 'flex',
                rowGap:'10px',
                flexDirection:'column',
                justifyContent:'center',
                fontSize:'14px',
                fontWeight:'700'
              }}
            >
              <Typography sx={{color:'black', display:'flex',columnGap:'10px',fontWeight:700}}>
                <Box component={'img'} src={Ukraine.src} width={'25px'} height={'25px'}></Box>
                Україна
              </Typography>
              <Typography sx={{color:'black', display:'flex',columnGap:'10px',fontSize:'13px',marginBottom:'10px'}}>
                Ужгород, 8-го Березня 4а
              </Typography>
              <Button
                fullWidth
                sx={{
                  backgroundColor: 'rgba(249, 205, 150, 1)',
                  borderRadius: '0px',
                  color: 'rgb(3,0,0)',
                  textTransform: 'none',
                  fontSize: '14px',
                  fontWeight: '600',
                  padding: '20px 30px',
                  '&:hover': { background: 'rgba(241,182,132,0.44)' },
                }}
                onClick={() => setOpenDialog(true)}
              >
                {intl.$t({ id: 'general.contactForm.button' })}
              </Button>
            </Box>
          </Box>
          <Box sx={{display:'grid',gridTemplateColumns:{xs:'1fr', md:'1fr 1fr 1fr'},rowGap:'20px',columnGap:'20px',paddingTop:'30px'}}>
            <Box
              sx={{
                display: 'flex',
                width:{xs:'335px', md:'400px'},
                height:{xs:'335px', md:'400px'}
              }}
            >
              <Image src={contact4.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            </Box>
            <Box
              sx={{
                display: 'flex',
                width:{xs:'335px', md:'400px'},
                height:{xs:'335px', md:'400px'}
              }}
            >
              <Image src={contact6.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            </Box>
            <Box
              sx={{
                paddingLeft:'10px',
                display: 'flex',
                rowGap:'10px',
                flexDirection:'column',
                justifyContent:'center',
                fontSize:'14px',
                fontWeight:'700'
              }}
            >
              <Typography sx={{color:'black', display:'flex',columnGap:'10px',fontWeight:700}}>
                <Box component={'img'} src={CountrySlovak.src} width={'25px'} height={'25px'}></Box>
                Україна
              </Typography>
              <Typography sx={{color:'black', display:'flex',columnGap:'10px',fontSize:'13px',marginBottom:'10px'}}>
                Bratislava, Mickiewiczova 4
              </Typography>
              <Button
                fullWidth
                sx={{
                  backgroundColor: 'rgba(249, 205, 150, 1)',
                  borderRadius: '0px',
                  color: 'rgb(3,0,0)',
                  textTransform: 'none',
                  fontSize: '14px',
                  fontWeight: '600',
                  padding: '20px 30px',
                  '&:hover': { background: 'rgba(241,182,132,0.44)' },
                }}
                onClick={() => setOpenDialog(true)}
              >
                {intl.$t({ id: 'general.contactForm.button' })}
              </Button>
            </Box>
          </Box>
        </Container>
      </Box>
      <ContactDialog open={openDialog} onClose={() => setOpenDialog(false)} />
      <Footer />
    </Box>
  )}

export default Contact
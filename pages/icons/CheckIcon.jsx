import React from 'react';

const CheckIcon = (props) => {
    return (
        <svg width="15" height="11" viewBox="0 0 15 11" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
            <g clipPath="url(#clip0_1_166)">
                <path fillRule="evenodd" clipRule="evenodd" d="M9.29452 3.37715C7.25011 5.2346 5.53021 6.79857 5.47247 6.85263L5.36753 6.95091L3.76012 5.26881C2.87603 4.34366 2.13946 3.59105 2.12328 3.59633C2.1071 3.60162 1.62274 4.03774 1.04694 4.56546L0 5.52496L0.092152 5.62931C0.14283 5.6867 1.31791 6.9186 2.70345 8.36684L5.22256 11L5.38535 10.8453C5.47487 10.7603 7.62549 8.80644 10.1645 6.50356C12.7035 4.20065 14.8302 2.26976 14.8904 2.21269L15 2.10892L14.1757 1.23294C13.7223 0.751149 13.2749 0.276655 13.1815 0.178481L13.0116 0L9.29452 3.37715Z" fill="#CF893C"/>
            </g>
            <defs>
                <clipPath id="clip0_1_166">
                    <rect width="15" height="11" fill="white"/>
                </clipPath>
            </defs>
        </svg>

    );
};

export default CheckIcon;
import React from 'react'

const FacebookIcon = props => {
  return (
    <>
      {props.isLarge ? (
        <svg
          width="13"
          height="29"
          viewBox="0 0 13 29"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M12.7469 0.710449H8.68266C6.27077 0.710449 3.58807 1.80153 3.58807 5.56193C3.59985 6.87219 3.58807 8.12704 3.58807 9.53929H0.797852V14.315H3.67442V28.0634H8.96026V14.2243H12.4491L12.7648 9.52589H8.86918C8.86918 9.52589 8.8779 7.43586 8.86918 6.82891C8.86918 5.34288 10.3068 5.42798 10.3932 5.42798C11.0773 5.42798 12.4075 5.43012 12.7489 5.42798V0.710449H12.7469Z"
            fill="#61676F"
          />
        </svg>
      ) : (
        <svg
          width="7"
          height="16"
          viewBox="0 0 7 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          {...props}
        >
          <path
            d="M6.98958 0H4.61219C3.20136 0 1.63213 0.638225 1.63213 2.83785C1.63902 3.60429 1.63213 4.33831 1.63213 5.1644H0V7.95792H1.68264V16H4.77457V7.90485H6.81535L7 5.15656H4.72129C4.72129 5.15656 4.72639 3.934 4.72129 3.57897C4.72129 2.70972 5.56221 2.7595 5.61279 2.7595C6.01295 2.7595 6.79101 2.76076 6.99075 2.7595V0H6.98958Z"
            fill="#61676F"
          />
        </svg>
      )}
    </>
  )
}

export default FacebookIcon

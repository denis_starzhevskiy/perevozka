import React, { useState } from 'react'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import { Box, Button, Container, Typography, useMediaQuery } from '@mui/material'
import Footer from '@/components/layout/Footer'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import bg from '../public/images/bgCountriesPages.png'
import bg2 from '../public/images/bg2countries.png'
import EastIcon from '@mui/icons-material/East'
import { countries } from '@/components/countryComponents/CountryComponents'

const Countries = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [openDialog, setOpenDialog] = useState(false)
  const [loadMore, setLoadMore] = useState(false)
  const [countryForModel, setCountryForModel] = useState(null)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Країни</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: 'Країни',
            link: '/country',
          },
        ]}
      />
      <Box>
        <Box
          sx={{
            backgroundImage: `url(${bg.src})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            padding: { xs: '20px 10px', md: '60px' },
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
          }}
        >
          <Box sx={{display:'flex', flexDirection: 'column',justifyContent: 'center', marginLeft:{xs:'0px' , md:'230px'}}}>
            <Typography
              sx={{
                textAlign:'center',
                color:'#F9CD96',
                fontSize: { xs: '32px', md: '46px' },
                fontWeight: { xs: '700', md: '800' },
                maxWidth: { xs: '100%', md: '80%' },
                textTransform: 'uppercase',
                marginTop:{xs:'30px', md:'0px'}
              }}
              variant={'h1'}
            >
              {intl.$t({ id: 'countries.title' })}
            </Typography>
          </Box>

          <Typography
            sx={{
              fontSize: '16px',
              fontWeight: '400',
              textAlign:'center'
            }}
            variant={'h4'}
          >
            {intl.$t({ id: 'countries.subTitle' })}
          </Typography>
          <Box
            sx={{
              display: 'grid',
              gridTemplateColumns: { xs: '1fr 1fr', md: '1fr 1fr 1fr 1fr' },
              gap: '15px',
            }}
          >
            {(loadMore || !mdDown ? countries : countries.slice(0, 20)).map((elem, idx) => {
              return (
                <Box
                  key={idx}
                  sx={{
                    backgroundImage: `url(${bg2.src})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    display: 'flex',
                    columnGap: '15px',
                    fontWeight: '500',
                    fontSize: '18px',
                    height: '70px',
                    alignItems: 'center',
                    padding: '0px 10px',

                    cursor: 'pointer',
                    '&:hover': {
                      border: theme => `1px solid ${theme.palette.primary.main}`,
                      color: theme => theme.palette.primary.main,
                      transition: '0.9s',
                    },
                  }}
                  onClick={() => setCountryForModel(elem.name)}
                >
                  <a
                    href={elem.link}
                    style={{display:'flex',flexDirection:'row' ,columnGap:'20px'}}
                  >
                  <Box
                    component={'img'}
                    src={elem.url}
                    alt={'how'}
                    sx={{ width: '35px', height: '25px' }}
                  />
                  <Typography>{intl.$t({ id: `countries.country.${elem.name}` })}</Typography>
                  </a>
                </Box>

              )
            })}
          </Box>
          {mdDown && (
            <Button
              variant={'outlined'}
              sx={{
                mb: '30px',
                background: 'rgba(255,255,255,0)',
                color: 'rgba(249, 205, 150, 1)',
                width: '400px',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
              }}
              endIcon={<EastIcon />}
              onClick={() => setLoadMore(prev => !prev)}
            >
              {loadMore ? 'Прикрити' : 'Завантажити ще'}
            </Button>
          )}
        </Box>
      </Box>
      <ContactDialog open={openDialog} onClose={() => setOpenDialog(false)} />
      <Footer />
    </Box>
  )}

export default Countries
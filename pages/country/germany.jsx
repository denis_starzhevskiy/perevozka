import React, { useState } from 'react'
import { Box, Button, Container, Divider, Typography, useMediaQuery } from '@mui/material'
import bg from 'public/images/countries/bggermany.png'
import bg2 from 'public/images/countries/bg2Ger.png'
import arrow from 'public/icons/arrow.svg'

import ContactForm from '@/components/title/ContactForm'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Contacts from '@/components/contacts/Contacts'
import Footer from '@/components/layout/Footer'
import howImage from '../../public/images/howImage2.png'
import FAQ from '@/components/faq/faq'
import Calculator from '@/components/calculator/Calculator'

const icon = [
  {
    name: 'GE',
    url: 'https://www.worldometers.info//img/flags/small/tn_gm-flag.gif',
  },
]

const procesing = [
  {
    text:'Підготовки необхідних документів;',
  },
  {
    text:'Відстані від місця смерті до місця поховання в Україні;',
  },
  {
    text:'Переліку додаткових послуг і побажань;',
  },
  {
    text:'Перевезення труни або урни з прахом тощо.',
  }
]

const Germany = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
    <Header />
  <Head>
    <title>Германія</title>
  </Head>
  <BreadcrumbsComponent
    path={[
      {
        label: 'Головна',
        link: '/',
      },
      {
        label: 'Країни',
        link: '/countries',
      },{
        label: 'Німеччина',
        link: '/country/germany',
      },
    ]}
  />
    <Box
      sx={{
        padding: '70px 0px',
        backgroundImage: `url(${bg.src})`,
        backgroundColor: '#181D24',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        px: { xs: '25px', md: '115px' },
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <Box
        sx={{
          width:{ xs: '385px', md: '80%' },
          display: 'flex',
          flexDirection: 'column',
          rowGap: '30px',
          alignItems: 'start' ,
          paddingRight:{ xs: '0px', md: '40px' }
        }}
      >
        {icon.map((elem, idx) => (
        <Box
          key={idx}
          component={'img'}
          src={elem.url}
          alt={'how'}
          sx={{ width: '90px', height: '55px' }}
        />
        ))}
        <Typography
          variant={'h1'}
          sx={{
            fontSize: { xs: '32px', md: '50px' },
            fontWeight: '800',
            textTransform: 'uppercase',
            textAlign: 'start',
          }}
        >
          Німеччина
        </Typography>
        <Typography
          sx={{
            width: { xs: '100%', md: '80%' },
            fontSize:  '26px',
            fontWeight: '600',
            textAlign: 'start' ,
          }}
        >
          Особливості перевезення тіла померлого з Німеччини: що потрібно знати родичам
        </Typography>
        <Typography
          sx={{
            fontSize:  '14px',
            fontWeight: '400',
            textAlign: 'start',
          }}
        >
          Смерть близької людини є трагедією для кожної родини. Якщо ж особа гине чи помирає в іншій країні, наприклад у Німеччині, до суму та переживань додаються ще й бюрократичні справи, пов’язані з перевезенням тіла до України.
        </Typography>
        <Typography
          sx={{
            fontSize:  '14px',
            fontWeight: '400',
            textAlign: 'start' ,
          }}
        >
          Зазвичай повідомлення про смерть людини у Німеччині отримують від місцевих органів влади, прокуратури або поліції, роботодавця, друзів чи знайомих померлого. У такому випадку родичі мають відразу звернутися до міжнародного похоронного бюро, щоб замовити відповідну послугу під ключ. Попередньо варто записатися на консультацію, щоб дізнатися всі деталі.
        </Typography>
      </Box>

      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
      {!mdDown && <ContactForm />}
    </Box>
      <Container sx={{
        backgroundImage: `url(${bg2.src})`,
        backgroundColor: '#181D24',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
            columnGap: '50px',
          }}
        >
          <Typography
            sx={{
              fontSize: '36px',
              fontWeight: '550',
              margin: { xs: '70px 0px 10px 0px', md: '100px 0px 50px 0px' },
              lineHeight: '40px',
            }}
            variant={'h1'}
          >
            Процес організації та ціна транспортування тіла
          </Typography>
          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: '400',
              fontStyle: 'italic',
              margin: { xs: '20px 0px 10px 0px', md: '100px 0px 10px 0px' },
              lineHeight: '21px',
              color:'#9f9f9f'
            }}
            variant={'h1'}
          >
            Це складний процес, що вимагає серйозної підготовки та ретельного планування. Ми чітко дотримуємося відповідних правил і процедур, маємо консульську підтримку. Виконуємо процес транспортування тіла померлого у найкоротші терміни. Pohovai.in.ua гарантує адекватні та доступні ціни.
          </Typography>
        </Box>
        <Box
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr', md:'1fr 1fr'},
            paddingBottom:'70px'
          }}
        >
          <Box
          >
            <Typography
            sx={{
              fontSize: '15px',
              fontWeight: '400',
            }}
          >
            Ціна перевезення тіла з Німеччини не є фіксованою та визначається індивідуально на приватній консультації. Вартість послуг залежить від:
          </Typography>
            <Box
              sx={{
                paddingTop:'30px',
                paddingBottom:'30px',
              }}
            >
            {procesing.map((item , idx) => (
              <Box
                key={idx}
                sx={{
                  display:'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  columnGap:'10px',
                  padding:'15px 0px'
                }}
              >
                <Box
                  key={idx}
                  component={'img'}
                  src={arrow.src}
                  alt={'how'}
                  sx={{ width: '21px', height: '7px' }}
                />
                <Typography
                  sx={{
                    fontSize:'16px',
                    fontWeight:'600'
                  }}
                >
                  {item.text}
                </Typography>
              </Box>
            ))}
            </Box>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              У вартість транспортування тіла з усіх федеральних земель Німеччини в Україну також входить сучасна труна для перевезення та, за потреби, поховання тіла. Тож про це можна не турбуватися. Станом на серпень 2024 року ця послуга коштує від 1100 до 3000 євро.
            </Typography>
          </Box>
        </Box>

      </Container>
      <Box sx={{backgroundColor:'white', paddingTop:{xs:'70px', md:'150px'}}}>
      <Container sx={{ color: 'black', mb: '50px' }} >
        <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
          {!mdDown && (
            <Box
              component={'img'}
              src={howImage.src}
              alt={'how'}
              sx={{ width: { xs: '400px', md: '500px' } }}
            />
          )}
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              rowGap: '30px',
              marginTop: { xs: '40px', md: '0px' },
            }}
          >
              <Typography sx={{ fontSize: '32px', fontWeight: '700' }}>
                Підготовка тіла до перевезення та поховання: можливі варіанти й оплата
              </Typography>
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              Спочатку родичам померлого потрібно підписати доручення. Це можна зробити в електронному варіанті або в офісі одного з великих міст Німеччини. Після цього вам не потрібно нічого робити, все виконують спеціалісти нашої агенції. Далі відбувається підготовка необхідних документів. Це дуже важливий етап, який варто довірити професіоналам. Вони мають бути перекладені й завірені нотаріально.
            </Typography>
            {mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '350px', md: '400px' } }}
              />
            )}
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              Також похоронне бюро радить замовити бальзамування тіла. Воно не є обов&apos;язковою умовою, але його дуже бажано зробити. Так, це коштує грошей, але після цієї процедури тіло не має запаху та зберігається потрібний час.
            </Typography>
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              Дуже часто кремація за кордоном, зокрема в Німеччині, для іноземців передбачена соціальним коштом. Проте забрати урну з прахом після кремації та перевезти її в Україну коштує грошей. Зверніть на це увагу, коли будете складати бюджет на повернення тіла родича.
            </Typography>
          </Box>
        </Box>
      </Container>
        <Box sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', pb: '70px', pt: '70px' }}>
      <Container sx={{ color: 'black', mb: '50px' }} >
        <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              rowGap: '30px',
              marginTop: { xs: '40px', md: '100px' },
            }}
          >
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              Якщо українець, який проживав у Європі, мав дійсний поліс страхування життя з покриттям вартості ритуальних послуг, транспортування його тіла на батьківщину буде для родини безплатним.
            </Typography>
            {mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '350px', md: '400px' } }}
              />
            )}
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              Для оплати послуг ритуальна агенція надає родині офіційний рахунок-фактуру. Сплатити відповідну суму можна в будь-якому банку або готівкою в німецьких офісах похоронного бюро та в Україні. Клієнту завжди пропонують зручний спосіб оплати.            </Typography>
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              Перевезення праху або тіла померлої людини з-за кордону є делікатною справою, що вимагає врахування багатьох моментів, тому вибирайте для цього надійну компанію. Це має бути агенція з досвідом роботи, позитивними відгуками на просторах Інтернету, найкоротшими строками й оптимальними цінами.            </Typography>
          </Box>
          {!mdDown && (
            <Box
              component={'img'}
              src={howImage.src}
              alt={'how'}
              sx={{ width: { xs: '400px', md: '500px' } }}
            />
          )}
        </Box>
      </Container>
        </Box>
       </Box>
      <Calculator/>
      <FAQ/>
      <Contacts/>
      <Footer/>
    </Box>
  )
}

export default Germany

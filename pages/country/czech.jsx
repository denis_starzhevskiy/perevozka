import React, { useState } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Container,
  Typography,
  useMediaQuery,
} from '@mui/material'
import bg from 'public/images/countries/bggermany.png'
import bg2 from 'public/images/countries/bgczech.png'
import arrow from 'public/icons/arrow.svg'

import ContactForm from '@/components/title/ContactForm'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Contacts from '@/components/contacts/Contacts'
import Footer from '@/components/layout/Footer'
import howImage from '../../public/images/howImage2.png'
import faq from '../../public/icons/faqArrow.svg'
import Calculator from '@/components/calculator/Calculator'

const FAq = [
  {
    id: 1,
    question: 'one.question',
    answer:
      'one.answer',
  },
  {
    id: 2,
    question: 'two.question',
    answer: 'two.answer'
  },
  {
    id: 3,
    question: 'three.question',
    answer: 'three.answer',
  },

];

const icon = [
  {
    name: 'CZ',
    url: 'https://www.worldometers.info//img/flags/small/tn_ez-flag.gif',
  },
]

const procesing = [
  {
    text:'text',
  },
  {
    text:'text1',
  },
  {
    text:'text2',
  },
  {
    text:'text3',
  },
  {
    text:'text4',
  },
  {
    text:'text5',
  },
  {
    text:'text6',
  },
]

const Czenia = [
  {
    text:'text',
  },
  {
    text:'text1',
  },
  {
    text:'text2',
  },
]

const Czech = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>{intl.$t({ id: 'czech.title' })}</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: intl.$t({ id: 'general.header.navigation.countries' }),
            link: '/countries',
          },{
            label: intl.$t({ id: 'czech.title' }),
            link: '/country/italy',
          },
        ]}
      />
      <Box
        sx={{
          padding: '70px 0px',
          backgroundImage: `url(${bg.src})`,
          backgroundColor: '#181D24',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          px: { xs: '25px', md: '115px' },
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Box
          sx={{
            width:{ xs: '385px', md: '80%' },
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
            alignItems: 'start' ,
            paddingRight:{ xs: '0px', md: '40px' }
          }}
        >
          {icon.map((elem, idx) => (
            <Box
              key={idx}
              component={'img'}
              src={elem.url}
              alt={'how'}
              sx={{ width: '90px', height: '55px' }}
            />
          ))}
          <Typography
            variant={'h1'}
            sx={{
              fontSize: { xs: '32px', md: '50px' },
              fontWeight: '800',
              textTransform: 'uppercase',
              textAlign: 'start',
            }}
          >
            {intl.$t({ id: 'czech.title' })}
          </Typography>
          <Typography
            sx={{
              width: { xs: '100%', md: '80%' },
              fontSize:  '26px',
              fontWeight: '600',
              textAlign: 'start' ,
            }}
          >
            {intl.$t({ id: 'czech.text' })}
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start',
            }}
          >
            {intl.$t({ id: 'czech.text1' })}
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start' ,
            }}
          >
            {intl.$t({ id: 'czech.text2' })}
          </Typography>
        </Box>

        <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
        {!mdDown && <ContactForm />}
      </Box>
      <Container sx={{
        backgroundImage: `url(${bg2.src})`,
        backgroundColor: '#181D24',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
            columnGap: '50px',
          }}
        >
          <Typography
            sx={{
              fontSize: '36px',
              fontWeight: '550',
              margin: { xs: '70px 0px 10px 0px', md: '100px 0px 50px 0px' },
              lineHeight: '40px',
            }}
            variant={'h1'}
          >
            {intl.$t({ id: 'czech.container1.title' })}
          </Typography>
          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: '400',
              fontStyle: 'italic',
              margin: { xs: '20px 0px 10px 0px', md: '100px 0px 10px 0px' },
              lineHeight: '21px',
              color:'#9f9f9f'
            }}
            variant={'h1'}
          >
            {intl.$t({ id: 'czech.container1.description' })}
          </Typography>
        </Box>
        <Box
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr', md:'1fr 1fr'},
            paddingBottom:'70px'
          }}
        >
          <Box>
            <Box
              sx={{
                paddingTop:'10px',
                paddingBottom:'30px',
              }}
            >
              {procesing.map((item , idx) => (
                <Box
                  key={idx}
                  sx={{
                    display:'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    columnGap:'10px',
                    padding:'15px 0px'
                  }}
                >
                  <Box
                    key={idx}
                    component={'img'}
                    src={arrow.src}
                    alt={'how'}
                    sx={{ width: '21px', height: '7px' }}
                  />
                  <Typography
                    sx={{
                      fontSize:'16px',
                      fontWeight:'600'
                    }}
                  >
                    {intl.$t({ id: `czech.container1.processing.${item.text}` })}
                  </Typography>
                </Box>
              ))}
            </Box>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              {intl.$t({ id: 'czech.container1.text' })}
            </Typography>
          </Box>
        </Box>

      </Container>
      <Box sx={{backgroundColor:'white', paddingTop:{xs:'70px', md:'150px'}}}>
        <Container sx={{ color: 'black', mb: '50px' }} >
          <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
            {!mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '400px', md: '500px' } }}
              />
            )}
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                rowGap: '30px',
                marginTop: { xs: '40px', md: '0px' },
              }}
            >
              <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                {intl.$t({ id: 'czech.container2.title' })}
              </Typography>
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                {intl.$t({ id: 'czech.container2.description' })}
              </Typography>
              {mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '350px', md: '400px' } }}
                />
              )}
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                {intl.$t({ id: 'czech.container2.text' })}
              </Typography>
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                {intl.$t({ id: 'czech.container2.text2' })}
              </Typography>
              {Czenia.map((item , idx) => (
              <Box
                key={idx}
                sx={{
                  display:'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  columnGap:'10px',
                  padding:'0px 0px'
                }}
              >
                <Typography
                  sx={{
                    fontSize:'16px',
                    fontWeight:'600'
                  }}
                >
                  {intl.$t({ id: `czech.container2.czenia.${item.text}` })}
                </Typography>
              </Box>
              ))}
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                {intl.$t({ id: 'czech.container2.text3' })}
              </Typography>
            </Box>
          </Box>
        </Container>
        <Box sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', pb: '70px', pt: '70px' }}>
          <Container sx={{ color: 'black', mb: '50px' }} >
            <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  rowGap: '30px',
                  marginTop: { xs: '20px', md: '50px' },
                  marginRight:'10px'
                }}
              >
                <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                  {intl.$t({ id: 'czech.container3.title' })}
                </Typography>
                {mdDown && (
                  <Box
                    component={'img'}
                    src={howImage.src}
                    alt={'how'}
                    sx={{ width: { xs: '350px', md: '400px' } }}
                  />
                )}
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  {intl.$t({ id: 'czech.container3.description' })}                </Typography>
                <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                  {intl.$t({ id: 'czech.container3.text' })}
                </Typography>
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  {intl.$t({ id: 'czech.container3.text1' })}
                </Typography>
                </Box>
              {!mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '400px', md: '500px' } }}
                />
              )}
            </Box>
          </Container>
        </Box>
      </Box>
      <Calculator/>
      <Box sx={{backgroundColor:'white', padding:'70px 0px'}}>
        <Container sx={{color:'black'}}>
          <Typography
            sx={{
              fontSize:'27px',
              fontWeight:'700',
              textAlign:'center'
            }}
          >
            FAQ
          </Typography>
          <Box
            sx={{
              display:'flex',
              flexDirection:'column',
              rowGap:'20px',
              padding:'40px',
            }}
          >
            {FAq.map((item, index) => (
              <Accordion
                key={index}
                sx={{
                  backgroundColor:'#F9F9F9',
                  padding:'30px 50px',
                  color:'black',
                  '&:hover': {
                    backgroundColor: 'rgba(209,184,152,0.2)',
                    border: '1px solid rgba(249, 205, 150, 1)',
                    width:'100%'
                  },
                }}
              >
                <AccordionSummary
                  expandIcon={<faq/>}
                  sx={{
                    fontSize:'16px',
                    fontWeight:'700',
                  }}
                >
                  {intl.$t({ id: `czech.faq.${item.question}` })}
                </AccordionSummary>
                <AccordionDetails>
                  {intl.$t({ id: `czech.faq.${item.answer}` })}
                </AccordionDetails>
              </Accordion>
            ))}
          </Box>
        </Container>
      </Box>
      <Contacts/>
      <Footer/>
    </Box>
  )
}

export default Czech

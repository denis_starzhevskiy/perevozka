import React, { useState } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Container,
  Typography,
  useMediaQuery,
} from '@mui/material'
import bg from 'public/images/countries/bggermany.png'
import bg2 from 'public/images/countries/bgItaly.png'
import arrow from 'public/icons/arrow.svg'

import ContactForm from '@/components/title/ContactForm'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Contacts from '@/components/contacts/Contacts'
import Footer from '@/components/layout/Footer'
import howImage from '../../public/images/howImage2.png'
import faq from '../../public/icons/faqArrow.svg'
import Calculator from '@/components/calculator/Calculator'

const FAq = [
  {
    id: 1,
    question: '1.Є варіанти безкоштовного перевезення праху або тіла померлого з Італії?',
    answer:
      'Так, можна. Щодня ритуальні катафалки повертають померлих на батьківщину для поховання.',
  },
  {
    id: 2,
    question: '2.Бажаємо замовити кремацію, але родичка померла у маленькому італійському містечку, як бути?',
    answer: 'Ми надаємо ритуальні послуги з поховання чи кремації в будь-якому місті Італії. Телефонуйте та дізнавайтеся деталі.'
  },
  {
    id: 3,
    question: '3.Скільки часу триває підготовка та транспортування тіла в Україну?',
    answer: 'Як правило, за тиждень-півтора вдається підготувати документи та повернути тіло на батьківщину, але в окремих випадках може бути затримка у кілька днів. У будь-якому випадку конкретний строк доставки тіла з-за кордону визначається з огляду на обставини та місце смерті.',
  },
  {
    id: 4,
    question: '4.Що впливає на вартість перевезення тіла з Європи?',
    answer: 'Ціна за перевезення тіла померлого українця залежить міста та країни смерті, а також від місця в Україні, куди треба його доставити. Також на вартість впливають додаткові послуги, такі як бальзамування, макіяж, одяг, труна тощо.',
  },
  {
    id: 5,
    question: '5.Яким транспортом відбувається перевезення тіла померлого?',
    answer: 'Як правило, це автоперевезення за допомогою ритуальних катафалків або авіаперевезення. Все залежить від країни, побажань і фінансових можливостей родичів померлого.',
  },
];

const icon = [
  {
    name: 'IT',
    url: 'https://www.worldometers.info//img/flags/small/tn_it-flag.gif',
  },
]

const procesing = [
  {
    text:'підписати доручення похоронному бюро представляти ваші інтереси;',
  },
  {
    text:'надати всю необхідну інформацію про померлого;',
  },
  {
    text:'бути на зв’язку, щоб постійно дізнаватися, як іде організація послуги та на якому вона етапі.',
  },
]

const italy = [
  {
    text:'свідоцтво про смерть, видане відповідним місцевим органом;',
  },
  {
    text:'довідка про причину смерті;',
  },
  {
    text:'дозвіл від місцевої влади на перевезення труни з тілом;',
  },
  {
    text:'консульський дозвіл на ввезення померлого до України.',
  },
]

const Italy = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Італія</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: intl.$t({ id: 'general.header.navigation.countries' }),
            link: '/countries',
          },{
            label: 'Італія',
            link: '/country/italy',
          },
        ]}
      />
      <Box
        sx={{
          padding: '70px 0px',
          backgroundImage: `url(${bg.src})`,
          backgroundColor: '#181D24',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          px: { xs: '25px', md: '115px' },
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Box
          sx={{
            width:{ xs: '385px', md: '80%' },
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
            alignItems: 'start' ,
            paddingRight:{ xs: '0px', md: '40px' }
          }}
        >
          {icon.map((elem, idx) => (
            <Box
              key={idx}
              component={'img'}
              src={elem.url}
              alt={'how'}
              sx={{ width: '90px', height: '55px' }}
            />
          ))}
          <Typography
            variant={'h1'}
            sx={{
              fontSize: { xs: '32px', md: '50px' },
              fontWeight: '800',
              textTransform: 'uppercase',
              textAlign: 'start',
            }}
          >
            Італія
          </Typography>
          <Typography
            sx={{
              width: { xs: '100%', md: '80%' },
              fontSize:  '26px',
              fontWeight: '600',
              textAlign: 'start' ,
            }}
          >
            Перевезення тіла померлого з Італії: особливості та вартість послуги
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start',
            }}
          >
            Смерть рідної людини – велике горе для родичів та близьких, а коли це трапляється в іншій країні, зокрема в Італії, виникають додаткові проблеми. Померлого необхідно підготувати та перевезти в Україну. Часто потрібні такі додаткові послуги, як кремація, бальзамування тіла, макіяж, вибір труни, оформлення документів про смерть тощо.          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start' ,
            }}
          >
            Що ж робити, якщо ваш родич помер в Італії? Повідомте Консульство про факт смерті чи загибелі родича та надайте всю відому вам інформацію. Після цього зателефонуйте в авторитетне похоронне бюро. Ми пропонуємо безкоштовну консультацію щодо організації перевезення надання ритуальних послуг в Італії або перевезення труни з тілом/урни з прахом в Україну. Для цього залиште заявку на сайті або зателефонуйте нам. Ми працюємо цілком легально, маємо гарну репутацію, багато схвальних відгуків і пропонуємо якісні послуги за доступною ціною.          </Typography>
        </Box>

        <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
        {!mdDown && <ContactForm />}
      </Box>
      <Container sx={{
        backgroundImage: `url(${bg2.src})`,
        backgroundColor: '#181D24',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
            columnGap: '50px',
          }}
        >
          <Typography
            sx={{
              fontSize: '36px',
              fontWeight: '550',
              margin: { xs: '70px 0px 10px 0px', md: '100px 0px 50px 0px' },
              lineHeight: '40px',
            }}
            variant={'h1'}
          >
            Як організовано транспортування тіла з Європи
          </Typography>
          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: '400',
              fontStyle: 'italic',
              margin: { xs: '20px 0px 10px 0px', md: '100px 0px 10px 0px' },
              lineHeight: '21px',
              color:'#9f9f9f'
            }}
            variant={'h1'}
          >
            При замовленні ритуальних послуг у спеціалізованій агенції родині померлого потрібно виконати такі прості дії:
          </Typography>
        </Box>
        <Box
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr', md:'1fr 1fr'},
            paddingBottom:'70px'
          }}
        >
          <Box
          >
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Ціна перевезення тіла з Німеччини не є фіксованою та визначається індивідуально на приватній консультації. Вартість послуг залежить від:
            </Typography>
            <Box
              sx={{
                paddingTop:'30px',
                paddingBottom:'30px',
              }}
            >
              {procesing.map((item , idx) => (
                <Box
                  key={idx}
                  sx={{
                    display:'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    columnGap:'10px',
                    padding:'15px 0px'
                  }}
                >
                  <Box
                    key={idx}
                    component={'img'}
                    src={arrow.src}
                    alt={'how'}
                    sx={{ width: '21px', height: '7px' }}
                  />
                  <Typography
                    sx={{
                      fontSize:'16px',
                      fontWeight:'600'
                    }}
                  >
                    {item.text}
                  </Typography>
                </Box>
              ))}
            </Box>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Згідно з чинним законодавством Італійської Республіки, ритуальні послуги можуть надаватися лише ліцензованою організацією, тобто найняти бус і самостійно перевезти померлого родича ви не зможете.            </Typography>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Наша компанія має офіси по всіх великих містах Італії та може запропонувати замовнику репатріацію тіла з будь-якого міста європейської країни до будь-якого міста нашої держави.
            </Typography>
          </Box>
        </Box>

      </Container>
      <Box sx={{backgroundColor:'white', paddingTop:{xs:'70px', md:'150px'}}}>
        <Container sx={{ color: 'black', mb: '50px' }} >
          <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
            {!mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '400px', md: '500px' } }}
              />
            )}
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                rowGap: '30px',
                marginTop: { xs: '40px', md: '0px' },
              }}
            >
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Документи, які готує агенція, перекладаються та нотаріально завіряються відповідно до законодавства Італії. Це:
              </Typography>
              {italy.map((item , idx) => (
                <Box
                  key={idx}
                  sx={{
                    display:'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    columnGap:'10px',
                    padding:'0px 0px'
                  }}
                >
                  <Box
                    key={idx}
                    component={'img'}
                    src={arrow.src}
                    alt={'how'}
                    sx={{ width: '21px', height: '7px' }}
                  />
                  <Typography
                    sx={{
                      fontSize:'16px',
                      fontWeight:'600'
                    }}
                  >
                    {item.text}
                  </Typography>
                </Box>
              ))}
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                На підготовку документів і репатріацію тіла зазвичай іде тиждень-півтора.
              </Typography>
              {mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '350px', md: '400px' } }}
                />
              )}
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Що стосується оплати, за наявності чинної страховки цим займається страхова компанія. Якщо у родини немає коштів на перевезення тіла, але дуже потрібно повернути померлого, є варіанти збору грошей у соцмережах або допомоги представників української спільноти в Італії. Ми пропонуємо найнижчу ціну на послуги.
              </Typography>
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Надаємо безкоштовні консультації, які можна отримати, залишивши заявку на сайті або зателефонувавши за одним із вказаних номерів. Станом на липень 2024 року репатріацію тіла з Італії в Україну можна замовити за суму від 1500 до 4000 євро.              </Typography>
            </Box>
          </Box>
        </Container>
        <Box sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', pb: '70px', pt: '70px' }}>
          <Container sx={{ color: 'black', mb: '50px' }} >
            <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  rowGap: '30px',
                  marginTop: { xs: '20px', md: '50px' },
                  marginRight:'10px'
                }}
              >
                <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                  Які додаткові послуги пропонує ритуальна агенція
                </Typography>
                {mdDown && (
                  <Box
                    component={'img'}
                    src={howImage.src}
                    alt={'how'}
                    sx={{ width: { xs: '350px', md: '400px' } }}
                  />
                )}
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  Крім одягу та макіяжу, бюро радить замовити бальзамування. Це не обов’язкова, але бажана процедура, що дозволить рідним спокійно провести близьку людину в останню путь після повернення на батьківщину.
                </Typography>
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  Запит на кремацію за кордоном постійно зростає, оскільки її вартість нижча за вартість перевезення покійного в Україну. Строки кремації та перевезення урни з прахом через кордон менші. Та й поховання у цьому випадку коштуватиме дешевше.
                </Typography>
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  Для оплати послуг родина отримує офіційний рахунок-фактуру, який сплачується у банках України та Італії або в офісах похоронного бюро. Клієнту пропонують не лише доступні ціни, але й зручний спосіб оплати: банківською карткою або готівкою в Україні.
                </Typography>
              </Box>
              {!mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '400px', md: '500px' } }}
                />
              )}
            </Box>
          </Container>
        </Box>
      </Box>
      <Calculator/>
      <Box sx={{backgroundColor:'white', padding:'70px 0px'}}>
        <Container sx={{color:'black'}}>
          <Typography
            sx={{
              fontSize:'27px',
              fontWeight:'700',
              textAlign:'center'
            }}
          >
            FAQ
          </Typography>
          <Box
            sx={{
              display:'flex',
              flexDirection:'column',
              rowGap:'20px',
              padding:'40px',
            }}
          >
            {FAq.map((item, index) => (
              <Accordion
                key={index}
                sx={{
                  backgroundColor:'#F9F9F9',
                  padding:'30px 50px',
                  color:'black',
                  '&:hover': {
                    backgroundColor: 'rgba(209,184,152,0.2)',
                    border: '1px solid rgba(249, 205, 150, 1)',
                    width:'100%'
                  },
                }}
              >
                <AccordionSummary
                  expandIcon={<faq/>}
                  sx={{
                    fontSize:'16px',
                    fontWeight:'700',
                  }}
                >
                  {item.question}
                </AccordionSummary>
                <AccordionDetails>
                  {item.answer}
                </AccordionDetails>
              </Accordion>
            ))}
          </Box>
        </Container>
      </Box>
      <Contacts/>
      <Footer/>
    </Box>
  )
}

export default Italy

import React, { useState } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,

  Container,

  Typography,
  useMediaQuery,
} from '@mui/material'
import bg from 'public/images/countries/bggermany.png'
import bg2 from 'public/images/countries/bgpoland.png'
import arrow from 'public/icons/arrow.svg'

import ContactForm from '@/components/title/ContactForm'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Contacts from '@/components/contacts/Contacts'
import Footer from '@/components/layout/Footer'
import howImage from '../../public/images/howImage2.png'
import faq from '../../public/icons/faqArrow.svg'
import Calculator from '@/components/calculator/Calculator'

const FAq = [
  {
    id: 1,
    question: '1.Чи потрібна особиста присутність близьких у Польщі?',
    answer:
      'Перевезення тіла померлого в Україну відбувається представниками похоронної компанії. Присутність родичів та друзів не потрібна.',
  },
  {
    id: 2,
    question: '2.Скільки коштує репатріація в Україну з Польщі?',
    answer: 'Сума перевезення тіла з Польщі в Україну залежить від багатьох факторів, зокрема особистих побажань родичів щодо організації процесу. Приблизна ціна варіюється від 1400 до 1900 євро.'
  },
  {
    id: 3,
    question: '3.У які терміни можна зробити кремацію в Польщі?',
    answer: 'Все обговорюється в індивідуальному порядку, фіксується на початку співпраці з ритуальним бюро. За сприятливими умовами послуга відбувається за 1 робочий день.',
  },
];

const icon = [
  {
    name: 'PL',
    url: 'https://www.worldometers.info//img/flags/small/tn_pl-flag.gif',
  },
]

const procesing = [
  {
    text:'укладення договору з ритуальним бюро;',
  },
  {
    text:'отримання свідоцтва про смерть;',
  },
  {
    text:'отримання дозволу на переміщення тіла від місцевого Inspektorat Sanitarny;',
  },
  {
    text:'отримання дозволу від польського консульства;',
  },
  {
    text:'отримання дозволу на експорт тіла з Польщі від місцевої поліції;',
  },
  {
    text:'оформлення сертифіката на бальзамування або герметичне пакування труни.',
  },
]

const poland = [
  {
    text:'1.Медичні довідки. Видаються медичними установами в Польщі. Документи підтверджують факт смерті, відсутність інфекційних захворювань.',
  },
  {
    text:'2.Державне гербове свідоцтво про смерть за кордоном. Видається відповідними державними органами Польщі.',
  },
  {
    text:'3.Дозволи на перевезення труни. Необхідно отримати у всіх закордонних установах, які мають відношення до цього процесу. Документи свідчать про відсутність перешкод для переміщення небіжчика.',
  },
  {
    text:'4.Консульський документ для транспортування тіла. Підтверджує виконання формальностей, офіційно санкціонує репатріацію.\n',
  },
  {
    text:'5.Нотаріально завірений переклад документів українською мовою.',
  },
]


const poland2 = [
  {
    text:'спеціальна труна;',
  },
  {
    text:'послуги відповідного крематорію в країні;',
  },
  {
    text:'транспортування небіжчика;',
  },
  {
    text:'організація прощання з померлим (за потребою);',
  },
  {
    text:'урна для праху;',
  },
  {
    text:'видача медичної довідки, свідоцтва про смерть;',
  },
  {
    text:'видача свідоцтва про кремацію;',
  },
  {
    text:'переклад документів на українську мову.',
  },
]



const Poland = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Польща</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: intl.$t({ id: 'general.header.navigation.countries' }),
            link: '/countries',
          },{
            label: 'Польща',
            link: '/country/poland',
          },
        ]}
      />
      <Box
        sx={{
          padding: '70px 0px',
          backgroundImage: `url(${bg.src})`,
          backgroundColor: '#181D24',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          px: { xs: '25px', md: '115px' },
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Box
          sx={{
            width:{ xs: '385px', md: '80%' },
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
            alignItems: 'start' ,
            paddingRight:{ xs: '0px', md: '40px' }
          }}
        >
          {icon.map((elem, idx) => (
            <Box
              key={idx}
              component={'img'}
              src={elem.url}
              alt={'how'}
              sx={{ width: '90px', height: '55px' }}
            />
          ))}
          <Typography
            variant={'h1'}
            sx={{
              fontSize: { xs: '32px', md: '50px' },
              fontWeight: '800',
              textTransform: 'uppercase',
              textAlign: 'start',
            }}
          >
            Польща
          </Typography>
          <Typography
            sx={{
              width: { xs: '100%', md: '80%' },
              fontSize:  '26px',
              fontWeight: '600',
              textAlign: 'start' ,
            }}
          >
            Перевезення померлого з Польщі: вимоги та покрокові дії
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start',
            }}
          >
            Життя людини непередбачуване. Ніхто не знає, де їй призначено народитися, прожити та померти. Станом на сьогодні випадки, коли людина завершує життя за кордоном, значно почастішали. Смерть може статися під час зустрічі з друзями, туристичної подорожі, поїздки з особистою ціллю. Мільйони українців живуть та працюють за кордоном.
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start' ,
            }}
          >
            У такому випадку у пригоді стане надійна фірма, яка пропонує послуги репатріації померлих. Кваліфіковані спеціалісти оперативно розв’яжуть всі питання, пов’язані з організацією поховання або транспортуванням небіжчика додому.            </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start' ,
            }}
          >
            Родичі небіжчика, крім важких переживань, стикаються з численними проблемами. Адже треба подбати про перевезення тіла, організувати поховання тощо. Можливо, знадобиться кремація за кордоном. Для цього вони мають оформити необхідні документи в найкоротший термін. Однак через власні емоції, брак певних знань не можуть цього зробити.
            </Typography>
        </Box>

        <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
        {!mdDown && <ContactForm />}
      </Box>
      <Container sx={{
        backgroundImage: `url(${bg2.src})`,
        backgroundColor: '#181D24',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
            columnGap: '50px',
          }}
        >
          <Typography
            sx={{
              fontSize: '36px',
              fontWeight: '550',
              margin: { xs: '70px 0px 10px 0px', md: '100px 0px 50px 0px' },
              lineHeight: '40px',
            }}
            variant={'h1'}
          >
            Етапи перевезення
          </Typography>
          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: '400',
              fontStyle: 'italic',
              margin: { xs: '20px 0px 10px 0px', md: '100px 0px 10px 0px' },
              lineHeight: '21px',
              color:'#9f9f9f'
            }}
            variant={'h1'}
          >
            Репатріації небіжчиків відрізняються залежно від вимог місцевого законодавства. Однак загальний алгоритм дій дуже схожий. За наявності всіх необхідних документів процес перевезення тіла з Польщі значно спрощується. Довіряйте професіоналам, тоді ви будете впевнені у коректному оформленні перекладів і легалізації пакета документів відповідно до міжнародних вимог.
          </Typography>
        </Box>
        <Box
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr', md:'1fr 1fr'},
            paddingBottom:'70px'
          }}
        >
          <Box
          >
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Перевезення померлих з Польщі до України відбувається наступним чином:
            </Typography>
            <Box
              sx={{
                paddingTop:'30px',
                paddingBottom:'30px',
              }}
            >
              {procesing.map((item , idx) => (
                <Box
                  key={idx}
                  sx={{
                    display:'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    columnGap:'10px',
                    padding:'15px 0px'
                  }}
                >
                  <Box
                    key={idx}
                    component={'img'}
                    src={arrow.src}
                    alt={'how'}
                    sx={{ width: '21px', height: '7px' }}
                  />
                  <Typography
                    sx={{
                      fontSize:'16px',
                      fontWeight:'600'
                    }}
                  >
                    {item.text}
                  </Typography>
                </Box>
              ))}
            </Box>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              До документів обов’язково додається переклад, завірений нотаріусом. Чинне законодавство Республіки Польща забороняє самостійне перевезення тіла. Замовнику доведеться обрати компанію, яка надає відповідні послуги (наявність польської ліцензії обов’язкова).            </Typography>
          </Box>
        </Box>

      </Container>
      <Box sx={{backgroundColor:'white', paddingTop:{xs:'70px', md:'150px'}}}>
        <Container sx={{ color: 'black', mb: '50px' }} >
          <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
            {!mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '400px', md: '500px' } }}
              />
            )}
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                rowGap: '30px',
                marginTop: { xs: '40px', md: '0px' },
              }}
            >
              <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                Документальне оформлення
              </Typography>
              {poland.map((item , idx) => (
                <Box
                  key={idx}
                  sx={{
                    display:'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    columnGap:'10px',
                    padding:'0px 0px'
                  }}
                >
                  <Typography
                    sx={{
                      fontSize:'16px',
                      fontWeight:'600'
                    }}
                  >
                    {item.text}
                  </Typography>
                </Box>
              ))}
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Наявність всіх необхідних документів гарантує безперешкодне та законне перевезення небіжчика з Польщі до України. Специфіка, вимоги до кожного папірця відрізняються, тому кращий варіант – звернутися до компанії, яка спеціалізується на міжнародному перевезенні померлих. Це дозволить уникнути можливих проблем.              </Typography>
              {mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '350px', md: '400px' } }}
                />
              )}
            </Box>
          </Box>
        </Container>
        <Box sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', pb: '70px', pt: '70px' }}>
          <Container sx={{ color: 'black', mb: '50px' }} >
            <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  rowGap: '30px',
                  marginTop: { xs: '20px', md: '50px' },
                  marginRight:'10px'
                }}
              >
                <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                  Процедура кремації
                </Typography>
                {mdDown && (
                  <Box
                    component={'img'}
                    src={howImage.src}
                    alt={'how'}
                    sx={{ width: { xs: '350px', md: '400px' } }}
                  />
                )}
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  Відбувається з дотриманням встановлених правил після отримання всіх документів. Мінімальна вартість складає від €500. Складається з наступного:                </Typography>
                {poland2.map((item , idx) => (
                  <Box
                    key={idx}
                    sx={{
                      display:'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      columnGap:'10px',
                      padding:'0px 0px'
                    }}
                  >
                    <Box
                      key={idx}
                      component={'img'}
                      src={arrow.src}
                      alt={'how'}
                      sx={{ width: '21px', height: '7px' }}
                    />
                    <Typography
                      sx={{
                        fontSize:'16px',
                        fontWeight:'600'
                      }}
                    >
                      {item.text}
                    </Typography>
                  </Box>
                ))}
                <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                  Згідно з домовленістю ритуальне бюро організовує перевезення праху через кордон з Польщі до України з усіма документами.                </Typography>
              </Box>
              {!mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '400px', md: '500px' } }}
                />
              )}
            </Box>
          </Container>
        </Box>
      </Box>
      <Calculator/>
      <Box sx={{backgroundColor:'white', padding:'70px 0px'}}>
        <Container sx={{color:'black'}}>
          <Typography
            sx={{
              fontSize:'27px',
              fontWeight:'700',
              textAlign:'center'
            }}
          >
            FAQ
          </Typography>
          <Box
            sx={{
              display:'flex',
              flexDirection:'column',
              rowGap:'20px',
              padding:'40px',
            }}
          >
            {FAq.map((item, index) => (
              <Accordion
                key={index}
                sx={{
                  backgroundColor:'#F9F9F9',
                  padding:'30px 50px',
                  color:'black',
                  '&:hover': {
                    backgroundColor: 'rgba(209,184,152,0.2)',
                    border: '1px solid rgba(249, 205, 150, 1)',
                    width:'100%'
                  },
                }}
              >
                <AccordionSummary
                  expandIcon={<faq/>}
                  sx={{
                    fontSize:'16px',
                    fontWeight:'700',
                  }}
                >
                  {item.question}
                </AccordionSummary>
                <AccordionDetails>
                  {item.answer}
                </AccordionDetails>
              </Accordion>
            ))}
          </Box>
        </Container>
      </Box>
      <Contacts/>
      <Footer/>
    </Box>
  )
}

export default Poland

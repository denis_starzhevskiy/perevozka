import React from 'react'
import { Box, Button, Container, Typography } from '@mui/material'
import { useRouter } from 'next/router'
import blogPreview from '../../public/images/bllog1.JPG'
import blog1 from '../../public/images/blog/1.JPG'
import blog2 from '../../public/images/blog/2.JPG'
import blog3 from '../../public/images/blog/3.JPG'
import blog4 from '../../public/images/blog/4.JPG'
import Head from 'next/head'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import BlogCard, { blogs } from '@/components/blog/BlogCard'
import Image from 'next/image'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Header from '@/components/layout/Header'
import Footer from '@/components/layout/Footer'
import { useIntl } from 'react-intl'
import Contacts from '@/components/contacts/Contacts'

const Blog1 = () => {
  const router = useRouter()
  const intl = useIntl()
  const blogData = {
    title: 'Процес міжнародного перевезення померлих: інструкція та вимоги',
    title1: 'first.address',
    text: 'Останнім часом нікого не здивуєш великою кількістю українців за кордоном. Вони виїжджають до Європи...',
    date: 'date',
    image: blogPreview,
    link:'blog1'
  }
  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Блог</title>
      </Head>

      <Container>
        <Box></Box>
      </Container>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: 'Блог',
            link: '/blog',
          },
          {
            label: `${intl.$t({ id: `blog.blogs.${blogData.title1}` })}`
          },
        ]}
      />

      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          height: '600px',
          position: 'relative',
          '&:before': {
            content: "' '",
            display: 'block',
            position: 'absolute',
            left: '0',
            top: '0',
            width: '100%',
            height: '100%',
            opacity: 0.1,
            backgroundImage: `url(${blogData.image.src})`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 0',
            backgroundSize: 'cover',
            zIndex: '1',
          },
        }}
      >
        <Container>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              height: '100%',
              flexDirection: 'column',
              rowGap: '10px',
            }}
          >
            <Box
              component={'span'}
              sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', color: 'white' }}
            >
              <CalendarMonthIcon />
              {intl.$t({ id: `blog.blogId.head.${blogData.date}` })}
            </Box>
            <Typography variant={'h1'} sx={{ fontSize: '32px', fontWeight: 600 ,color:'white'}}>
              {intl.$t({ id: 'blog.blogId.head.one' })}
            </Typography>
            <Typography>{intl.$t({ id: 'blog.blogId.head.two' })}</Typography>
          </Box>
        </Container>
      </Box>

      <Container sx={{backgroundColor:'#181D24'}}>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', paddingTop: '70px' }}>
          <Typography variant={'h6'} sx={{ fontWeight: 800 , textAlign:'start'}}>
            {intl.$t({ id: 'blog.blogId.container1.one.title' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.one.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.one.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.one.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.one.list3' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.one.text2' })}</Typography>
          <Box
            sx={{
              display: 'flex',
              justifyContent:'center',
              height:'350px'
            }}
          >
            <Image src={blog3.src} width={450} height={700} alt={'icon'} style={{alignItems:'center'}} />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '30px 0px' }}>
          <Typography variant={'h5'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.two.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.two.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.two.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.two.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.two.list3' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.two.text2' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.two.text3' })}</Typography>
          <Box
            sx={{
              display: 'grid',
              gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
              columnGap: '30px',
            }}
          >
            <Image src={blog1.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
            <Image src={blog2.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '30px 10px 0px 0px' }}>
          <Typography variant={'h5'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.three.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.three.text1' })}</Typography>
          <Box
            component={'ol'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,

              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.three.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,

              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.three.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,

              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.three.list3' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.three.text2' })}</Typography>
          <Box
            sx={{
              display: 'flex',
              justifyContent:'center',
              height:'350px'
            }}
          >
            <Image src={blog4.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
          </Box>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingTop: '30px',
            paddingBottom: '70px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.four.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.four.text' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',

            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.five.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.five.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.five.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.five.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.five.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.five.list4' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.five.text2' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',

            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.six.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.six.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.six.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.six.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.six.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.six.list4' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.six.list5' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.six.text2' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.seven.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.seven.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.seven.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.seven.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.seven.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.seven.list4' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.seven.text2' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.seven.text3' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.eight.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.eight.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eight.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eight.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eight.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eight.list4' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eight.list5' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eight.list6' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.eight.text2' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.nine.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.nine.text1' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.eight.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.ten.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.ten.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.ten.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.ten.list3' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.ten.text2' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.eleven.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.eleven.text1' })}</Typography>
          <Box
            component={'ul'}
            sx={{
              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list4' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.eleven.text2' })}</Typography>
          <Box
            component={'ol'}
            sx={{
              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list5' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list6' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container1.eleven.list7' })}
            </Box>

          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.eleven.text3' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingTop: '30px',
            paddingBottom: '70px',
          }}
        >
          <Typography variant={'h5'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container1.conclusion.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container1.conclusion.text' })}</Typography>

        </Box>
      </Container>

      <Box sx={{ width: '100%', background: 'white', padding: '70px 0px' }}>
        <Container>
          <Box sx={{ marginBottom: '70px', textAlign: 'center' }}>
            <Typography
              variant={'h2'}
              align={'center'}
              sx={{
                fontSize: '40px',
                fontWeight: 600,
                marginBottom: '40px',
                color: 'rgba(207, 137, 60, 1)',
              }}
            >
              {intl.$t({ id: 'blog.blogId.button' })}
            </Typography>
            <Box
              sx={{
                display: 'grid',
                // gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr' },
                columnGap: '30px',
                rowGap: '30px',
              }}
            >
              {blogs.slice(0.3).map((item, idx) => {
                return <BlogCard key={idx} blogData={item} />
              })}
            </Box>
            <Button
              variant={'contained'}
              sx={{
                boxShadow: 'box-shadow: rgba(249, 205, 150, 1) 0px 5px 15px;',
                backgroundColor: 'rgba(249, 205, 150, 1)',
                color: 'rgba(0, 0, 0, 1)',
                width: '350px',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'white' },
                marginLeft: 'auto',
                marginRight: 'auto',
              }}
              onClick={() => router.push('/blog')}
            >
              {intl.$t({ id: 'blog.blogId.button2' })}
            </Button>
          </Box>
        </Container>
      </Box>
      <Contacts/>
      <Footer />
    </Box>
  )
}

export default Blog1

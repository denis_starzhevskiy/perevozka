import React from 'react'
import { Box, Button, Container, Typography } from '@mui/material'
import { useRouter } from 'next/router'
import blogPreview from '../../public/images/blogg1.JPG'
import blog5 from '../../public/images//blog/5.JPG'
import blog6 from '../../public/images//blog/6.JPG'
import blog7 from '../../public/images//blog/7.JPG'

import Head from 'next/head'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import BlogCard, { blogs } from '@/components/blog/BlogCard'
import Image from 'next/image'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Header from '@/components/layout/Header'
import Footer from '@/components/layout/Footer'
import { useIntl } from 'react-intl'
import Contacts from '@/components/contacts/Contacts'

const Blog2 = () => {
  const router = useRouter()
  const intl = useIntl()
  const blogData = {
    title: 'Ритуальні послуги за кордоном: особливості та поради',
    title1:'second.address',
    text: '...',
    date: 'date1',
    image: blogPreview,
    link:'blog2'
  }
  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Блог</title>
      </Head>

      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: 'Блог',
            link: '/blog',
          },
          {
            label: `${intl.$t({ id: `blog.blogs.${blogData.title1}` })}`
          },
        ]}
      />

      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          height: '600px',
          position: 'relative',
          '&:before': {
            content: "' '",
            display: 'block',
            position: 'absolute',
            left: '0',
            top: '0',
            width: '100%',
            height: '100%',
            opacity: 0.3,
            backgroundImage: `url(${blogData.image.src})`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 0',
            backgroundSize: 'cover',
            zIndex: '1',
          },
        }}
      >
        <Container>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              height: '100%',
              flexDirection: 'column',
              rowGap: '10px',
            }}
          >
            <Box
              component={'span'}
              sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', color: 'white' }}
            >
              <CalendarMonthIcon />
              {intl.$t({ id: `blog.blogId.head.${blogData.date}` })}
            </Box>
            <Typography variant={'h1'} sx={{ fontSize: '32px', fontWeight: 600 ,color:'white'}}>
              {intl.$t({ id: 'blog.blogId.head.third' })}
            </Typography>
            <Typography>{intl.$t({ id: 'blog.blogId.head.four' })}</Typography>
          </Box>
        </Container>
      </Box>

      <Container>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', paddingTop: '70px' }}>
          <Typography variant={'h6'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container2.one.title' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.one.text1' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.one.text2' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.one.text3' })}</Typography>
          <Box
            sx={{
              display: 'flex',
              justifyContent:'center',
              height:'350px'
            }}
          >
            <Image src={blog5.src} width={400} height={400} alt={'icon'} style={{alignItems:'center'}} />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '30px 0px' }}>
          <Typography variant={'h6'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container2.two.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.two.text1' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.two.text2' })}</Typography>
          <Box
            component={'ul'}
            sx={{

              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.two.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.two.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.two.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.two.list4' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.two.list5' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
                whiteSpace: { xs: 'normal', md: 'nowrap' },
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.two.list6' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.two.text3' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.two.text4' })}</Typography>
          <Box
            sx={{
              display: 'flex',
              justifyContent:'center',

            }}
          >
            <Image src={blog6.src} width={'500px'} height={400} alt={'icon'} style={{alignItems:'center'}} />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '30px 0px' }}>
          <Typography variant={'h6'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container2.three.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.three.text1' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.three.text2' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.three.text3' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h6'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container2.four.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.four.text1' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.four.text2' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.four.text3' })}</Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '40px',
          }}
        >
          <Typography variant={'h6'} sx={{ fontWeight: 600, textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container2.five.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.five.text1' })}</Typography>
          <Box
            component={'ol'}
            sx={{
              listStylePosition: 'inside',
              display: 'flex',
              marginLeft: '40px',
              flexDirection: 'column',
              rowGap: '30px',
            }}
          >
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,

              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.five.list1' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,

              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.five.list2' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,

              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.five.list3' })}
            </Box>
            <Box
              component={'li'}
              sx={{
                lineHeight: '20px',
                fontWeight: 700,
              }}
            >
              {intl.$t({ id: 'blog.blogId.container2.five.list4' })}
            </Box>
          </Box>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.five.text2' })}</Typography>
          <Box
            sx={{
              display: 'flex',
              justifyContent:'center',

            }}
          >
            <Image src={blog7.src} width={'500px'} height={400} alt={'icon'} style={{alignItems:'center'}} />
          </Box>
        </Box>

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingBottom: '80px',
          }}
        >
          <Typography variant={'h6'} sx={{ fontWeight: 600,textAlign:'start' }}>
            {intl.$t({ id: 'blog.blogId.container2.conclusion.title1' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.conclusion.text' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container2.conclusion.text1' })}</Typography>

        </Box>
      </Container>

      <Box sx={{ width: '100%', background: 'white', padding: '70px 0px' }}>
        <Container>
          <Box sx={{ marginBottom: '70px', textAlign: 'center' }}>
            <Typography
              variant={'h2'}
              align={'center'}
              sx={{
                fontSize: '40px',
                fontWeight: 600,
                marginBottom: '40px',
                color: 'rgba(207, 137, 60, 1)',
              }}
            >
              {intl.$t({ id: 'blog.blogId.button' })}
            </Typography>
            <Box
              sx={{
                display: 'grid',
                // gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr' },
                columnGap: '30px',
                rowGap: '30px',
              }}
            >
              {blogs.slice(0.3).map((item, idx) => {
                return <BlogCard key={idx} blogData={item} />
              })}
            </Box>
            <Button
              variant={'contained'}
              sx={{
                boxShadow: 'box-shadow: rgba(249, 205, 150, 1) 0px 5px 15px;',
                backgroundColor: 'rgba(249, 205, 150, 1)',
                color: 'rgba(0, 0, 0, 1)',
                width: '350px',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'white' },
                marginLeft: 'auto',
                marginRight: 'auto',
              }}
              onClick={() => router.push('/blog')}
            >
              {intl.$t({ id: 'blog.blogId.button2' })}
            </Button>
          </Box>
        </Container>
      </Box>
      <Contacts/>
      <Footer />
    </Box>
  )
}

export default Blog2

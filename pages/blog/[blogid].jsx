import React from 'react'
import { Box, Button, Container, Typography } from '@mui/material'
import { useRouter } from 'next/router'
import blogPreview from '../../public/images/blogimage.png'
import Head from 'next/head'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import BlogCard, { blogs } from '@/components/blog/BlogCard'
import Image from 'next/image'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Header from '@/components/layout/Header'
import Footer from '@/components/layout/Footer'
import { useIntl } from 'react-intl'

const Blogid = () => {
  const router = useRouter()
  const intl = useIntl()
  const blogData = {
    title: 'Перевезення померлих з країн Європи',
    text: 'Останнім часом нікого не здивуєш великою кількістю українців за кордоном. Вони виїжджають до Європи...',
    date: 'date',
    image: blogPreview,
  }
  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Блог</title>
      </Head>

      <Container>
        <Box></Box>
      </Container>
      <BreadcrumbsComponent
        path={[
          {
            label: 'Головна',
            link: '/',
          },
          {
            label: 'Блог',
            link: '/blog',
          },
          {
            label: blogData.title,
          },
        ]}
      />

      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          height: '600px',
          position: 'relative',
          '&:before': {
            content: "' '",
            display: 'block',
            position: 'absolute',
            left: '0',
            top: '0',
            width: '100%',
            height: '100%',
            opacity: 0.6,
            backgroundImage: `url(${blogData.image.src})`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 0',
            backgroundSize: 'cover',
            zIndex: '1',
          },
        }}
      >
        <Container>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              height: '100%',
              flexDirection: 'column',
              rowGap: '10px',
            }}
          >
            <Box
              component={'span'}
              sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', color: 'white' }}
            >
              <CalendarMonthIcon />
              {intl.$t({ id: `blog.blogId.head.${blogData.date}` })}
            </Box>
            <Typography variant={'h1'} sx={{ fontSize: '60px', fontWeight: 600 }}>
              {intl.$t({ id: 'blog.blogId.head.one' })}
            </Typography>
            <Typography>{intl.$t({ id: 'blog.blogId.head.two' })}</Typography>
          </Box>
        </Container>
      </Box>

      <Container>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '70px 0px' }}>
          <Typography variant={'h6'} sx={{ fontWeight: 600 }}>
            {intl.$t({ id: 'blog.blogId.container.title' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text1' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text2' })}</Typography>
          <Box
            sx={{
              display: 'grid',
              gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
              columnGap: '30px',
            }}
          >
            <Image src={blogData.image.src} width={'400px'} height={'408px'} alt={'icon'} />
            <Image src={blogData.image.src} width={'400px'} height={'408px'} alt={'icon'} />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '30px 0px' }}>
          <Typography variant={'h6'} sx={{ fontWeight: 600 }}>
            {intl.$t({ id: 'blog.blogId.container.title2' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text3' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text4' })}</Typography>
          <Image src={blogData.image.src} width={'400px'} height={'408px'} alt={'icon'} />
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '40px', padding: '30px 0px' }}>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text5' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text6' })}</Typography>
          <Box
            sx={{
              display: 'grid',
              gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
              columnGap: '30px',
            }}
          >
            <Image src={blogData.image.src} width={'400px'} height={'408px'} alt={'icon'} />
            <Image src={blogData.image.src} width={'400px'} height={'408px'} alt={'icon'} />
          </Box>
          <Image src={blogData.image.src} width={'400px'} height={'408px'} alt={'icon'} />
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '40px',
            paddingTop: '30px',
            paddingBottom: '70px',
          }}
        >
          <Typography variant={'h6'} sx={{ fontWeight: 600 }}>
            {intl.$t({ id: 'blog.blogId.container.title3' })}
          </Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text7' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text8' })}</Typography>
          <Typography>{intl.$t({ id: 'blog.blogId.container.text9' })}</Typography>
        </Box>
      </Container>

      <Box sx={{ width: '100%', background: 'white', padding: '70px 0px' }}>
        <Container>
          <Box sx={{ marginBottom: '70px', textAlign: 'center' }}>
            <Typography
              variant={'h2'}
              align={'center'}
              sx={{
                fontSize: '40px',
                fontWeight: 600,
                marginBottom: '40px',
                color: 'rgba(207, 137, 60, 1)',
              }}
            >
              {intl.$t({ id: 'blog.blogId.button' })}
            </Typography>
            <Box
              sx={{
                display: 'grid',
                // gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr' },
                columnGap: '30px',
                rowGap: '30px',
              }}
            >
              {blogs.slice(0.3).map((item, idx) => {
                return <BlogCard key={idx} blogData={item} />
              })}
            </Box>
            <Button
              variant={'contained'}
              sx={{
                boxShadow: 'box-shadow: rgba(249, 205, 150, 1) 0px 5px 15px;',
                backgroundColor: 'rgba(249, 205, 150, 1)',
                color: 'rgba(0, 0, 0, 1)',
                width: '350px',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'white' },
                marginLeft: 'auto',
                marginRight: 'auto',
              }}
              onClick={() => router.push('/blog')}
            >
              {intl.$t({ id: 'blog.blogId.button2' })}
            </Button>
          </Box>
        </Container>
      </Box>
      <Footer />
    </Box>
  )
}

export default Blogid

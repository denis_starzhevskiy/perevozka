import React from 'react'
import Head from 'next/head'
import { Box, Container, Pagination, Typography } from '@mui/material'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Button from '@mui/material/Button'
import blogPreview from 'public/images/blogimage.png'
import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward'

import BlogCard from '@/components/blog/BlogCard'
import Footer from '@/components/layout/Footer'
import Header from '@/components/layout/Header'
import { useIntl } from 'react-intl'
import Contacts from '@/components/contacts/Contacts'

export const blogs = [
  {
    name: 'Перевезення померлих з країн Європи',
    description:
      'Останнім часом нікого не здивуєш великою кількістю українців за кордоном. Вони виїжджають до Європи...',
    date: '13 квітня, 2024',
    image: blogPreview,
  },
  {
    name: 'Перевезення померлих з країн Європи',
    description:
      'Останнім часом нікого не здивуєш великою кількістю українців за кордоном. Вони виїжджають до Європи...',
    date: '13 квітня, 2024',
    image: blogPreview,
  },
  {
    name: 'Перевезення померлих з країн Європи',
    description:
      'Останнім часом нікого не здивуєш великою кількістю українців за кордоном. Вони виїжджають до Європи...',
    date: '13 квітня, 2024',
    image: blogPreview,
  },
]

const Index = () => {
  const intl = useIntl()
  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Блог</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: 'Блог',
            link: '/blog',
          },
        ]}
      />
      <Container>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
            padding: '30px',
            rowGap: '10px',
          }}
        >
          <Typography
            sx={{
              color: 'white',
              fontSize: '32px',
              fontWeight: 600,
              textAlign: 'center',
            }}
          >
            Блог
          </Typography>
        </Box>

        <BlogCard />
        <Box sx={{ display: 'flex', justifyContent: 'center', marginBottom: '70px' }}>
          <Pagination count={8} shape="rounded" color={'primary'} />
        </Box>

        <Box sx={{ marginBottom: '70px' }}>
          <Button
            sx={{
              width: '100%',
              border: '1px solid rgba(207, 137, 60, 1)',
              textTransform: 'none',
              fontWeight: 600,
              padding: '10px',
            }}
            endIcon={<ArrowOutwardIcon />}
          >
            {intl.$t({ id: 'blog.blogIndex.button' })}
          </Button>
        </Box>
      </Container>
      <Footer />
    </Box>
  )
}

export default Index

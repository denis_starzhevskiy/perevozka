const handler = async (req, res) => {
  const { text } = req.body

  try {
    return await fetch(
      `https://api.telegram.org/${process.env.NEXT_BOT_TOKEN}/sendMessage?chat_id=${process.env.NEXT_BOT_CHAT_ID}&text=${text}`
    )
  } catch (error) {
    return Promise.reject()
  }
}

export default handler

import { CacheProvider } from '@emotion/react'
import Head from 'next/head'
import { CssBaseline, ThemeProvider } from '@mui/material'
import createEmotionCache from '../utils/createEmotionCache'
import theme from '../styles/theme/theme'
import '../styles/globals.css'
import InternalizationProvider from '../providers/InternalizationProvider'

const clientSideEmotionCache = createEmotionCache()

const MyApp = ({ Component, emotionCache = clientSideEmotionCache, pageProps }) => {
  return (
    <>
      <CacheProvider value={emotionCache}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Head>
            <title>Perevozka</title>
          </Head>
          <InternalizationProvider>
            <Component {...pageProps} />
          </InternalizationProvider>
        </ThemeProvider>
      </CacheProvider>
    </>
  )
}

export default MyApp

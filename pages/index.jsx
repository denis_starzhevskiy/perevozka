import React from 'react'
import Head from 'next/head'
import Layout from '@/components/layout/Layout'
import TitleComponent from '@/components/title/TitleComponent'
import HowWeWorkComponent from '@/components/howWeWork/HowWeWorkComponent'
import CountryComponents from '@/components/countryComponents/CountryComponents'
import TransportationOfTheDead from '@/components/transportationOfTheDead/TransportationOfTheDead'
import ServiceAndAdvantages from '@/components/serviseAndAdvantages/ServiseAndAdventages'
import WhyUs from '@/components/whyUs/WhyUs'
import Contacts from '@/components/contacts/Contacts'
import Gallery from '@/components/gallery/Gallery'
import Reviews from '@/components/reviews/Reviews'
import BlogTitle from '@/components/blog/BlogTitle'
import { useIntl } from 'react-intl'
import Calculator from '@/components/calculator/Calculator'
import { useMediaQuery } from '@mui/material'
import Certificate from '@/components/certificate/Certificate'

const Home = () => {
  const intl = useIntl()
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))

  return (
    <>
      <Head>
        <title>{intl.$t({ id: 'general.Head' })}</title>
        <meta name="description" content="Надаємо якісні ритуальні послуги (like)" />
      </Head>
      <Layout>
        <TitleComponent />
        <HowWeWorkComponent />
        <Calculator/>
        <CountryComponents />
        <TransportationOfTheDead />
        <Gallery />
        <Certificate/>
        <ServiceAndAdvantages />
        <WhyUs />
        <BlogTitle />
        <Reviews mdDown={mdDown}/>
        <Contacts />
      </Layout>
    </>
  )
}

export default Home

import React, { useState } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Container,
  Typography,
  useMediaQuery,
} from '@mui/material'
import bg from 'public/images/countries/bggermany.png'
import bg2 from 'public/images/countries/bgItaly.png'
import arrow from 'public/icons/arrow.svg'

import ContactForm from '@/components/title/ContactForm'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import Header from '@/components/layout/Header'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import Contacts from '@/components/contacts/Contacts'
import Footer from '@/components/layout/Footer'
import howImage from '../public/images/howImage2.png'
import faq from '../public/icons/faqArrow.svg'
import Calculator from '@/components/calculator/Calculator'

const FAq = [
  {
    id: 1,
    question: '1.Як перевезти урну з прахом в Україну та скільки це буде коштувати?',
    answer:
      'Написати чітку та конкретну ціну перевезення праху з-за кордону неможливо. Є лише орієнтовні суми. Вартість перевезення урни з прахом з-за кордону в Україну значно відрізняється залежно від країни та міста смерті, наявності потрібних документів, обсягу додаткових послуг, особистих побажань родини.',
  },
  {
    id: 2,
    question: '2.Чому вартість кремації та перевезення праху нижча, ніж репатріація тіла?',
    answer: 'Труна для кремації набагато дешевша, оскільки виробляється з картону, потрібно менше ритуальної атрибутики, ніж для традиційного захоронення, не потрібно багато місця для поховання, можна обійтися без траурної процесії та пам’ятника. '
  },
  {
    id: 3,
    question: '3.Як замовити кремацію, якщо родич помер у невеличкому європейському містечку?',
    answer: 'Ми надаємо ритуальні послуги з поховання чи кремації в будь-якому місті Європи. Зателефонуйте нам чи залиште заявку, щоб отримати більш детальну інформацію.',
  },
  {
    id: 4,
    question: '4.Чи можна родині попрощатись з померлим перед кремацією?',
    answer: 'Так, це дозволяється. Ми можемо організувати церемонію прощання з померлою людиною у ритуальній залі або церкві. Аби заздалегідь узгодити всі важливі деталі щодо прощання, потрібно телефонувати до нашого похоронного бюро та замовити консультацію.',
  },
  {
    id: 5,
    question: '5.Що отримає родина після виконання кремації за кордоном?',
    answer: 'Після кремації родичі померлого отримують урну з прахом, свідоцтво про кремацію в тій країні, де помер українець, медичну довідку про причину смерті та свідоцтво про смерть.',
  },
  {
    id:6,
    question: '6.Чи можна провести кремацію за кордоном безкоштовно?',
    answer: 'Якщо померлий мав діючий поліс страхування життя, який покриває вартість ритуальних послуг, можна розраховувати на те, що всі витрати по кремації візьме на себе страхова.'
  }
];

const processing = [
  {
    text:'Свідоцтво про смерть, видане місцевими органами влади;',
  },
  {
    text:'Довідка про причину смерті;',
  },
  {
    text:'Довідка про відсутність у померлого будь-яких інфекцій, що видається місцевими медиками;',
  },
  {
    text:'Дозвіл від місцевої влади на перевезення урни з прахом;'
  },
  {
    text:'Консульський дозвіл на ввезення урни з прахом до України.'
  }
]

const italy = [
  {
    text:'свідоцтво про смерть, видане відповідним місцевим органом;',
  },
  {
    text:'довідка про причину смерті;',
  },
  {
    text:'дозвіл від місцевої влади на перевезення труни з тілом;',
  },
  {
    text:'консульський дозвіл на ввезення померлого до України.',
  },
]

const Cremation = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>Кремація за кордоном</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: intl.$t({ id: 'general.header.navigation.countries' }),
            link: '/countries',
          },{
            label: 'Кремація за кордоном',
            link: '/country/italy',
          },
        ]}
      />
      <Box
        sx={{
          padding: '70px 0px',
          backgroundImage: `url(${bg.src})`,
          backgroundColor: '#181D24',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          px: { xs: '25px', md: '115px' },
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Box
          sx={{
            width:{ xs: '385px', md: '80%' },
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
            alignItems: 'start' ,
            paddingRight:{ xs: '0px', md: '40px' }
          }}
        >
          <Typography
            variant={'h1'}
            sx={{
              fontSize: { xs: '32px', md: '50px' },
              fontWeight: '800',
              textTransform: 'uppercase',
              textAlign: 'start',
            }}
          >
            <Box sx={{fontSize: { xs: '32px', md: '50px' },
              fontWeight: '800',
              textTransform: 'uppercase',
              textAlign: 'start',
              color:'#F9CD96'}}>Кремація за кордоном:</Box> етапи та вартість послуги
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start' ,
            }}
          >
            Якщо ваш родич помер або загинув за кордоном, потрібно забезпечити перевезення тіла в Україну для поховання. Для цього потрібно виконати ряд умов, зібрати документи, підготувати тіло. Але часто родина обирає таку послугу, як кремація. Це найбільш простий та економічно вигідний варіант для близьких померлого.
          </Typography>
          <Typography
            sx={{
              fontSize:  '14px',
              fontWeight: '400',
              textAlign: 'start',
            }}
          >
            Тіло забирають з місця смерті в морг. Наша агенція підготує необхідну документацію. Тим часом призначається дата кремації. Родина може особисто забрати урну з прахом або домовитися про транспортування. Кремацію можна організувати без необхідності особистого відвідування похоронного бюро чи крематорія. Всі організаційні питання ми візьмемо на себе. Це важливий момент для кожної родини, адже біль та страждання від втрати близької людини не дозволяють з холодною головою зосередитися на організації підготовки та поховання. До того ж термін поховання урни з прахом необмежений.
          </Typography>
        </Box>

        <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
        {!mdDown && <ContactForm />}
      </Box>
      <Box sx={{backgroundColor:'white', paddingTop:{xs:'70px', md:'150px'}}}>
        <Container sx={{ color: 'black', mb: '50px' }} >
          <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
            {!mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '400px', md: '500px' } }}
              />
            )}
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                rowGap: '30px',
                marginTop: { xs: '40px', md: '0px' },
              }}
            >
              <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
                Як працює ритуальна агенція
              </Typography>
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Після смерті родича його близькі зв’язуються з похоронним бюро, підписують на нього доручення та укладають договір про співпрацю. Після цього потрібно лише бути на зв’язку, адже решту питань бере на себе наша ритуальна компанія.ра.
              </Typography>
              {mdDown && (
                <Box
                  component={'img'}
                  src={howImage.src}
                  alt={'how'}
                  sx={{ width: { xs: '350px', md: '400px' } }}
                />
              )}
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Часто українські родини за кордоном самостійно замовляють послуги крематорію, оформлюють всі документи про смерть, забирають урну з прахом. Але в деяких країнах, зокрема, в Німеччині, самостійно зробити це або неможливо, або дуже довго. Саме тому для замовлення послуги кремації за кордоном потрібно звернутися до офіційного міжнародного похоронного бюро Pohovai.in.ua.
              </Typography>
              <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
                Ми надійно та в найкоротші строки доставимо урну з прахом з будь-якої країни Європи в Україну. Візьмемо на себе оформлення всієї необхідної документації. Запропонуємо замовникам найнижчі ціни та офіційний рахунок-фактуру. Наша агенція має гарну репутацію, багато схвальних відгуків та достатній досвід роботи.
              </Typography>
            </Box>
          </Box>
        </Container>
      </Box>
      <Container
        sx={{
          backgroundImage: `url(${bg2.src})`,
          backgroundColor: '#181D24',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }}>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
            columnGap: '50px',
          }}
        >
          <Typography
            sx={{
              fontSize: '36px',
              fontWeight: '550',
              margin: { xs: '70px 0px 10px 0px', md: '100px 0px 50px 0px' },
              lineHeight: '40px',
            }}
            variant={'h1'}
          >
            Документи, потрібні для кремації та перевезення урни в Україну
          </Typography>
          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: '400',
              fontStyle: 'italic',
              margin: { xs: '20px 0px 10px 0px', md: '100px 0px 10px 0px' },
              lineHeight: '21px',
              color:'#9f9f9f'
            }}
            variant={'h1'}
          >
            На підготовку документів та перевезення урни зазвичай витрачається близько тижня, іноді більше, в залежності від ситуації.          </Typography>
        </Box>
        <Box
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr', md:'1fr 1fr'},
            paddingBottom:'70px'
          }}
        >
          <Box
          >
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Документи, які готує агенція, перекладаються та нотаріально завіряються відповідно до місцевого законодавства. Це:            </Typography>
            <Box
              sx={{
                paddingTop:'30px',
                paddingBottom:'30px',
              }}
            >
              {processing.map((item , idx) => (
                <Box
                  key={idx}
                  sx={{
                    display:'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    columnGap:'10px',
                    padding:'15px 0px'
                  }}
                >
                  <Box
                    key={idx}
                    component={'img'}
                    src={arrow.src}
                    alt={'how'}
                    sx={{ width: '21px', height: '7px' }}
                  />
                  <Typography
                    sx={{
                      fontSize:'16px',
                      fontWeight:'600'
                    }}
                  >
                    {item.text}
                  </Typography>
                </Box>
              ))}
            </Box>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Згідно з чинним законодавством Італійської Республіки, ритуальні послуги можуть надаватися лише ліцензованою організацією, тобто найняти бус і самостійно перевезти померлого родича ви не зможете.            </Typography>
            <Typography
              sx={{
                fontSize: '15px',
                fontWeight: '400',
              }}
            >
              Наша компанія має офіси по всіх великих містах Італії та може запропонувати замовнику репатріацію тіла з будь-якого міста європейської країни до будь-якого міста нашої держави.
            </Typography>
          </Box>
        </Box>

      </Container>
      <Box sx={{ display: 'grid',gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
            marginTop: { xs: '40px', md: '100px' },
          }}
        >
          <Typography sx={{ fontSize: '32px', fontWeight: '600px' }}>
            Цінові пропозиції
          </Typography>
          <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
            Кремація стала дуже популярною послугою, адже у цьому випадку потрібно менше ритуальної атрибутики, ніж для традиційного захоронення. Необхідна лише найпростіша труна, що буде спалюватися разом із тілом. Також можна заощадити на пошуку місця для поховання та обійтися без пам’ятника. До того ж поховати урну з прахом простіше, ніж труну з тілом.          </Typography>
          {mdDown && (
            <Box
              component={'img'}
              src={howImage.src}
              alt={'how'}
              sx={{ width: { xs: '350px', md: '400px' } }}
            />
          )}
          <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
            Ціна кремації за кордоном буде різною, в залежності від країни та міста смерті людини, місцевих законів та обраних додаткових послуг, які замовляє родина померлого. Але, якщо ми говоримо про транспортування урни через кордон, то тут, на жаль, ситуація виглядає ідентично, як з перевезенням труни, тому що враховується кілометраж.
          </Typography>
          <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
            Так, станом на липень 2024 року мінімальна вартість кремації в Польщі складає 500 євро, а така ж послуга в Німеччині обійдеться родині мінімум у 600 євро, з Італії – 900 євро. Щоб дізнатися точну ціну, залиште заявку на сайті або зателефонуйте в нашу агенцію та отримайте безкоштовну консультацію. Наші спеціалісти заберуть урну з прахом в будь-якій країні Європи та перевезуть її до будь-якого міста України. Це буде швидко та зручно для рідних померлого.
          </Typography>
        </Box>
        {!mdDown && (
          <Box
            component={'img'}
            src={howImage.src}
            alt={'how'}
            sx={{ width: { xs: '400px', md: '500px' } }}
          />
        )}
      </Box>
      <Calculator/>
      <Contacts/>
      <Footer/>
    </Box>
  )
}

export default Cremation

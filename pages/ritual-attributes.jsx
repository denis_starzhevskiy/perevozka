import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import { Box, Button, Container, Typography } from '@mui/material'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import busautoup1 from '../public/images/contactPages3.png'

import Footer from '@/components/layout/Footer'
import Header from '@/components/layout/Header'
import { useIntl } from 'react-intl'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { getRitualAttributes } from '../api/ritualAttributes'

export const attributes = [
  {
    image: busautoup1,
    text:'photo',
  },
  {
    image: busautoup1,
    text:'photo',
  },
  {
    image: busautoup1,
    text:'photo',
  },
  {
    image: busautoup1,
    text:'photo',
  },
  {
    image: busautoup1,
    text:'photo',
  },
  {
    image: busautoup1,
    text:'photo',
  },
]

const RitualAttributes = () => {
  const intl = useIntl()
  const [ritualAttributes, setRitualAttributes] = useState([])
  const [contactFormOpen, setContactFormOpen] = useState(false)

  useEffect(() => {
    getRitualAttributes().then(result => {
      setRitualAttributes(result.data)
    })
  }, [])

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Header />
      <Head>
        <title>{intl.$t({ id: 'RitualAttributes.title' })}</title>
      </Head>
      <BreadcrumbsComponent
        path={[
          {
            label: intl.$t({ id: 'general.header.navigation.main' }),
            link: '/',
          },
          {
            label: intl.$t({ id: 'RitualAttributes.title' }),
            link: '/RitualAttributes',
          },
        ]}
      />
      <Container sx={{paddingBottom:'40px'}}>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
            padding: { xs: '20px', md: '40px' },
            rowGap: '10px',
          }}
        >
          <Typography
            variant="h1"
            component="h1"
            sx={{
              textAlign:'center',
              color: 'rgba(249, 205, 150, 1)',
              fontSize: { xs: '36px', md: '40px' },
              fontStyle: 'normal',
              fontWeight: 700,
              textTransform:'uppercase',
              marginBottom: { xs: '20px', md: '0px' },
              lineHeight: 'normal',
              '@media (min-width: 900px)': {
                fontSize: '60px',
              },
            }}
          >
            {intl.$t({ id: 'RitualAttributes.title' })}
          </Typography>
          <Typography
            sx={{
              fontSize: { xs: '14px', md: '16px' },
              fontWeight: '400',
              textAlign:  'center' ,
              marginBottom: { xs: '0px', md: '30px' },
              width:{xs:'400px', md:'100%'}
            }}
            variant={'h1'}
          >
            {intl.$t({ id: 'RitualAttributes.text' })}
          </Typography>
        </Box>

        <Container
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr', md:'1fr 1fr 1fr'},
            columnGap:'10px',
            rowGap:'10px'
        }}
        >
        {ritualAttributes.map((image, idx) => (
        <Box
          key={idx}
          sx={{
            marginTop:'20px'
        }}
        >
          <Box
            sx={{
            display: 'flex',
            flexDirection: 'column-reverse',
            backgroundImage: `linear-gradient(to bottom, rgba(21, 24, 30, 0),rgba(21, 24, 30, 1)), url(${image.attributes.image.data.attributes.url || busautoup1.src})`,
            width:'350px',
            height:'350px'
          }}
          >
          <Typography sx={{
            color: '#ffffff',
            fontSize: '16px',
            marginBottom: '20px',
            display:'flex',
            justifyContent:'center'
          }}
          >
            {image.attributes.description}
          </Typography>
          </Box>
          <Button
            variant={'outlined'}
            sx={{
              border: '1px solid rgba(249, 205, 150, 1)',
              color: 'rgba(249, 205, 150, 1)',
              width: '350px',
              height: '45px',
              fontSize: '11px',
              fontWeight: '400',

            }}
            onClick={() => setContactFormOpen(true)}
          >
              {intl.$t({ id: `RitualAttributes.button` })}
          </Button>
        </Box>
        ))}
        </Container>

      </Container>
      <Footer />
      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
    </Box>
  )
}

export default RitualAttributes
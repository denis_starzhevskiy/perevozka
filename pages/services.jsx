import React, { useState } from 'react'
import Head from 'next/head'
import BreadcrumbsComponent from '@/components/blog/BreadcrumbsComponent'
import { Box, Button, Container, Typography, useMediaQuery } from '@mui/material'
import Contacts from '@/components/contacts/Contacts'

import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'

import AngelServise from '../public/images/AngelServise.png'
import serviceBack from '../public/images/serviceItemBackground.png'
import Elipse from './icons/elipse'
import Reviews from '@/components/reviews/Reviews'
import Layout from '@/components/layout/Layout'

const services = [
  {
    coloredTitle: 'one.coloredTitle',
    title: 'one.title',
  },
  {
    coloredTitle: 'two.coloredTitle',
    title: 'two.title',
  },
  {
    coloredTitle: 'three.coloredTitle',
    title: 'three.title',
  },
  {
    coloredTitle: 'four.coloredTitle',
    title: 'four.title',
  },
  {
    coloredTitle: 'five.coloredTitle',
    title: 'five.title',
  },
  {
    coloredTitle: 'six.coloredTitle',
    title: 'six.title',
  },
]

const Services = () => {
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Head>
        <title>Наші послуги</title>
      </Head>
      <Layout>
        <BreadcrumbsComponent
          path={[
            {
              label: 'Головна',
              link: '/',
            },
            {
              label: 'Послуги',
              link: '/services',
            },
          ]}
        />
        <Box sx={{ backgroundImage: `url(${AngelServise.src})`, padding: '70px 0px' }}>
              <Container
                maxWidth={'lg'}
                sx={{
                  margin: 'auto',
                  px: { xs: '10px', md: '10px' },
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  color: 'white',
                  display: 'grid',
                  gridTemplateColumns: { xs: '1fr', md: '1fr   ' },
                }}
              >
                <Box sx={{ display: 'flex',flexDirection:'column',width:{xs:'100%', md:'1200px'}, alignItems:'center'}}>
                  <Typography
                    sx={{
                      marginLeft: { xs: '10px', md: '20px' },
                      fontSize: { xs: '26px', md: '50px' },
                      fontWeight: '700',
                      mt: '30px',
                      color:'#F9CD96',
                      textAlign:{xs:'center', md:'start'}
                    }}
                    variant={'h1'}
                  >
                    {intl.$t({ id: 'serviseAndAdvantages.title1' })}
                  </Typography>
                  <Typography
                    sx={{
                      textAlign:'center',
                      fontSize: '16px',
                      fontWeight: '500',
                      mt: '30px',
                      maxWidth: { xs: '100%', md: '100%' },
                      lineHeight: '24px',
                      paddingBottom:'30px'
                    }}
                    variant={'h4'}
                  >
                      {intl.$t({ id: 'serviseAndAdvantages.text1' })}{' '}
                       <span style={{ color: 'rgba(249, 205, 150, 1)' }}>
                       {intl.$t({ id: 'serviseAndAdvantages.text2' })}
                       </span>
                  </Typography>
                </Box>
                <Box
                  sx={{
                    display: 'grid',
                    gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr  ' },
                  }}
                >
                  {services.map((service, idx) => {
                    return (
                      <Box
                        key={idx}
                        sx={{
                          justifyContent:'space-between',
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'center',
                          rowGap: '15px',
                          backgroundImage: `url(${serviceBack.src})`,
                          backgroundSize: 'cover',
                          backgroundRepeat: 'no-repeat',
                          margin: '5px',
                          padding: '45px 15px 20px 15px',
                        }}
                      >
                        <Elipse width={50} height={20} />
                        <Typography sx={{ textAlign: 'center', fontSize: '14px' }}>
                    <span style={{ color: 'rgba(249, 205, 150, 1)' }}>
                      {intl.$t({ id: `serviseAndAdvantages.services.${service.coloredTitle}` })}
                    </span>{' '}
                          {intl.$t({ id: `serviseAndAdvantages.services.${service.title}` })}
                        </Typography>
                        <Button
                          variant={'outlined'}
                          sx={{
                            border: '1px solid rgba(249, 205, 150, 1)',
                            color: 'rgba(249, 205, 150, 1)',
                            width: '350px',
                            height: '45px',
                            fontSize: '11px',
                            fontWeight: '400',

                          }}
                          href='/сremation'
                        >
                          {intl.$t({ id: `RitualAttributes.button1` })}
                        </Button>
                      </Box>
                    )
                  })}
                </Box>
              </Container>
            </Box>
        <Reviews/>
        <Contacts />
        <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} isQuestion />
      </Layout>
    </Box>
  )}

export default Services
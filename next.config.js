/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  optimizeFonts: true,
  images: {
    domains: [],
  },
  env: {
    NEXT_BOT_TOKEN: process.env.NEXT_BOT_TOKEN,
    NEXT_BOT_CHAT_ID: process.env.NEXT_BOT_CHAT_ID,
  },
}

module.exports = nextConfig

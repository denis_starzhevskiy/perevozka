import React, { useEffect, useRef, useState } from 'react'
import { Box, Container, Typography, useMediaQuery } from '@mui/material'
import ArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import ArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft'
import { Swiper, SwiperSlide } from 'swiper/react'
import { A11y, Navigation, Pagination, Scrollbar } from 'swiper/modules'
import 'swiper/css'
import 'swiper/css/navigation'
import busautoup1 from '../../public/images/busautoup1.JPEG'
import busautodown1 from 'public/images/busautodown1.jpg'
import busautoup2 from '../../public/images/busautoup2.jpg'
import busautodown2 from 'public/images/busautodown2.jpg'
import busautoup3 from '../../public/images/busautoup3.jpg'
import busautodown3 from 'public/images/busautodown3.jpg'

const gallery = [
  {
    image: busautoup1,
    imageBack: busautodown1,
  },
  {
    image: busautoup2,
    imageBack: busautodown2,
  },
  {
    image: busautoup3,
    imageBack: busautodown3,
  },
]

const Gallery = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const media = useMediaQuery('(min-width: 900px)')
  const [swiper, setSwiper] = useState()
  const prevRef = useRef()
  const nextRef = useRef()

  useEffect(() => {
    if (swiper) {
      swiper.params.navigation.prevEl = prevRef.current
      swiper.params.navigation.nextEl = nextRef.current
      swiper.navigation.init()
      swiper.navigation.update()
    }
  }, [swiper])

  return (
    <Box
      sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', padding: { xs: '70px 10px', md: '70px' } }}
    >
      <a name={'Gallery'}>
        <Typography
          sx={{
            color: 'black',
            fontSize: '32px',
            fontWeight: 600,
            textAlign: 'center',
            marginBottom: '30px',
          }}
        >
          Транспорт
        </Typography>
      </a>

      <Box sx={{ display: 'flex', columnGap: '10px', alignItems: 'center' }}>
        {!mdDown && (
          <Box ref={prevRef}>
            <ArrowLeftIcon
              sx={{
                fontSize: '30px',
                color: theme => theme.palette.primary.main,
                border: theme => `1px solid ${theme.palette.primary.main}`,
                borderRadius: '50%',
                width: '40px',
                height: '40px',
                '&:hover': {
                  backgroundColor: 'rgba(207, 137, 60, 1)',
                  color: 'white',
                },
              }}
            />
          </Box>
        )}
        <Container maxWidth={'lg'}>
          <Swiper
            modules={[Navigation, Pagination, Scrollbar, A11y]}
            spaceBetween={'50px'}
            slidesPerView={media ? 3 : 1}
            pagination={{ clickable: true }}
            scrollbar={{ draggable: true }}
            onSlideChange={() => console.log('slide change')}
            navigation={{
              prevEl: prevRef?.current,
              nextEl: nextRef?.current,
            }}
            updateOnWindowResize
            observer
            observeParents
            onSwiper={setSwiper}
            sx={{
              '.swiper-container .swiper-pagination': {
                bottom: '-30px!important',
              },
            }}
          >
            {gallery.map((image, idx) => (
              <SwiperSlide key={idx}>
                <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '30px' }}>
                  <Box
                    sx={{
                      backgroundImage: `linear-gradient(to bottom, rgba(21, 24, 30, 0),rgba(21, 24, 30, 1)), url(${image.image.src})`,
                      backgroundSize: 'cover',
                      width: '350px',
                      minHeight: '300px',
                      display: 'flex',
                      alignItems: 'flex-end',
                      justifyContent: 'center',
                      padding: '10px 0px',
                    }}
                  >
                    <Typography sx={{ textAlign: 'center', fontSize: '14px' }}>
                      {image.text}
                    </Typography>
                  </Box>
                  {!mdDown && (
                    <Box
                      sx={{
                        backgroundImage: `linear-gradient(to bottom, rgba(21, 24, 30, 0),rgba(21, 24, 30, 1)), url(${image.imageBack.src})`,
                        backgroundSize: 'cover',
                        width: '350px',
                        minHeight: '300px',
                        display: 'flex',
                        alignItems: 'flex-end',
                        justifyContent: 'center',
                        padding: '10px 0px',
                      }}
                    >
                      <Typography sx={{ textAlign: 'center', fontSize: '14px' }}>
                        {image.text}
                      </Typography>
                    </Box>
                  )}
                </Box>
              </SwiperSlide>
            ))}
          </Swiper>
        </Container>
        {!mdDown && (
          <Box ref={nextRef}>
            <ArrowRightIcon
              sx={{
                fontSize: '20px',
                color: theme => theme.palette.primary.main,
                border: theme => `1px solid ${theme.palette.primary.main}`,
                borderRadius: '50%',
                width: '40px',
                height: '40px',
                '&:hover': {
                  backgroundColor: 'rgba(207, 137, 60, 1)',
                  color: 'white',
                },
              }}
            />
          </Box>
        )}
      </Box>
    </Box>
  )
}

export default Gallery

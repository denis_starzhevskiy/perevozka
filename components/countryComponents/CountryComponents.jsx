import React, { useState } from 'react'
import { Box, Button, Container, Typography, useMediaQuery } from '@mui/material'
import bg from 'public/images/background_country_1.png'
import EastIcon from '@mui/icons-material/East'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'

export const countries = [
  {
    name: 'AT',
    url: 'https://www.worldometers.info//img/flags/small/tn_au-flag.gif',
    link:''
  },
  {
    name: 'AL',
    url: 'https://www.worldometers.info//img/flags/small/tn_al-flag.gif',
    link:''
  },
  {
    name: 'UK',
    url: 'https://www.worldometers.info//img/flags/small/tn_uk-flag.gif',
    link:''
  },
  {
    name: 'BG',
    url: 'https://www.worldometers.info//img/flags/small/tn_bu-flag.gif',
    link:''
  },
  {
    name: 'BE',
    url: 'https://www.worldometers.info//img/flags/small/tn_be-flag.gif',
    link:''
  },
  {
    name: 'GR',
    url: 'https://www.worldometers.info//img/flags/small/tn_gr-flag.gif',
    link:''
  },
  {
    name: 'DK',
    url: 'https://www.worldometers.info//img/flags/small/tn_da-flag.gif',
    link:''
  },
  {
    name: 'EE',
    url: 'https://www.worldometers.info//img/flags/small/tn_en-flag.gif',
    link:''
  },
  {
    name: 'IT',
    url: 'https://www.worldometers.info//img/flags/small/tn_it-flag.gif',
    link:'/country/italy'
  },
  {
    name: 'ES',
    url: 'https://www.worldometers.info//img/flags/small/tn_sp-flag.gif',
    link:''
  },
  {
    name: 'LV',
    url: 'https://www.worldometers.info//img/flags/small/tn_lg-flag.gif',
    link:''
  },
  {
    name: 'LT',
    url: 'https://www.worldometers.info//img/flags/small/tn_lh-flag.gif',
    link:''
  },
  {
    name: 'MD',
    url: 'https://www.worldometers.info//img/flags/small/tn_md-flag.gif',
    link:''
  },
  {
    name: 'NL',
    url: 'https://www.worldometers.info//img/flags/small/tn_nl-flag.gif',
    link:''
  },
  {
    name: 'GE',
    url: 'https://www.worldometers.info//img/flags/small/tn_gm-flag.gif',
    link:'/country/germany'
  },
  {
    name: 'NO',
    url: 'https://www.worldometers.info//img/flags/small/tn_no-flag.gif',
    link:''
  },
  {
    name: 'PL',
    url: 'https://www.worldometers.info//img/flags/small/tn_pl-flag.gif',
    link:'/country/poland'
  },
  {
    name: 'PT',
    url: 'https://www.worldometers.info//img/flags/small/tn_po-flag.gif',
    link:''
  },
  {
    name: 'RO',
    url: 'https://www.worldometers.info//img/flags/small/tn_ro-flag.gif',
    link:''
  },
  {
    name: 'RS',
    url: 'https://www.worldometers.info//img/flags/small/tn_ri-flag.gif',
    link:''
  },
  {
    name: 'SK',
    url: 'https://www.worldometers.info//img/flags/small/tn_lo-flag.gif',
    link:''
  },
  {
    name: 'SI',
    url: 'https://www.worldometers.info//img/flags/small/tn_si-flag.gif',
    link:''
  },
  {
    name: 'TR',
    url: 'https://www.worldometers.info//img/flags/small/tn_tu-flag.gif',
    link:''
  },
  {
    name: 'HU',
    url: 'https://www.worldometers.info//img/flags/small/tn_hu-flag.gif',
    link:''
  },
  {
    name: 'FI',
    url: 'https://www.worldometers.info//img/flags/small/tn_fi-flag.gif',
    link:''
  },
  {
    name: 'FR',
    url: 'https://www.worldometers.info//img/flags/small/tn_fr-flag.gif',
    link:''
  },
  {
    name: 'HR',
    url: 'https://www.worldometers.info//img/flags/small/tn_hr-flag.gif',
    link:''
  },
  {
    name: 'CZ',
    url: 'https://www.worldometers.info//img/flags/small/tn_ez-flag.gif',
    link:'/country/czech'
  },
  {
    name: 'ME',
    url: 'https://www.worldometers.info//img/flags/small/tn_mj-flag.gif',
    link:''
  },
  {
    name: 'CH',
    url: 'https://www.worldometers.info//img/flags/small/tn_sz-flag.gif',
    link:''
  },
  {
    name: 'SE',
    url: 'https://www.worldometers.info//img/flags/small/tn_sw-flag.gif',
    link:''
  },
]
const CountryComponents = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [loadMore, setLoadMore] = useState(false)
  const [countryForModel, setCountryForModel] = useState(null)
  const intl = useIntl()
  console.log(countryForModel)

  return (
    <>
      <Container
        sx={{
          color: 'black',
          backgroundColor: '#eaeaea',
          backgroundImage: `url(${bg.src})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          marginTop: '70px',
        }}
        maxWidth={'lg'}
      >
        <Box
          sx={{
            padding: { xs: '20px 10px', md: '60px' },
            display: 'flex',
            flexDirection: 'column',
            rowGap: '30px',
          }}
        >
          <a name={'Countries'}>
            <Typography
              sx={{
                fontSize: { xs: '24px', md: '42px' },
                fontWeight: { xs: '700', md: '500' },
                maxWidth: { xs: '100%', md: '600px' },
              }}
              variant={'h1'}
            >
              {intl.$t({ id: 'countries.title' })}
            </Typography>
          </a>
          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: '400',
            }}
            variant={'h4'}
          >
            {intl.$t({ id: 'countries.subTitle' })}
          </Typography>
          <Box
            sx={{
              display: 'grid',
              gridTemplateColumns: { xs: '1fr 1fr', md: '1fr 1fr 1fr 1fr' },
              gap: '15px',
            }}
          >
            {(loadMore || !mdDown ? countries : countries.slice(0, 20)).map((elem, idx) => {
              return (
                <Box
                  key={idx}
                  sx={{
                    backgroundColor: 'white',
                    display: 'flex',
                    columnGap: '15px',
                    fontWeight: '500',
                    fontSize: '18px',
                    height: '70px',
                    alignItems: 'center',
                    padding: '0px 10px',
                    border: '1px solid rgba(223, 231, 240, 1)',
                    cursor: 'pointer',
                    '&:hover': {
                      border: theme => `1px solid ${theme.palette.primary.main}`,
                      color: theme => theme.palette.primary.main,
                      transition: '0.9s',
                    },
                  }}
                  onClick={() => setCountryForModel(elem.name)}
                >
                  <a
                    href={elem.link}
                    style={{ display: 'flex', flexDirection: 'row', columnGap: '20px' }}
                  >
                    <Box
                      component={'img'}
                      src={elem.url}
                      alt={'how'}
                      sx={{ width: '35px', height: '25px' }}
                    />
                    <Typography>{intl.$t({ id: `countries.country.${elem.name}` })}</Typography>
                  </a>
                </Box>
            )
            })}
            </Box>
              {
                mdDown && (
                  <Button
                    variant={'outlined'}
                    sx={{
                mb: '30px',
                backgroundColor: 'white',
                color: 'rgba(249, 205, 150, 1)',
                width: '350px',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'white' },
              }}
              endIcon={<EastIcon />}
              onClick={() => setLoadMore(prev => !prev)}
            >
              {loadMore ? 'Прикрити' : 'Завантажити ще'}
            </Button>
          )}
        </Box>
      </Container>
      <ContactDialog
        open={!!countryForModel}
        onClose={() => setCountryForModel(null)}
        chosenCountry={countryForModel}
      />
    </>
  )
}

export default CountryComponents

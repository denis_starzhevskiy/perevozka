import React, { useState } from 'react'
import { Box, Button, Container, Divider, Typography, useMediaQuery } from '@mui/material'
import EastIcon from '@mui/icons-material/East'
import howImage from '../../public/images/howImage2.png'
import howImage3 from '../../public/images/howImage3.png'
import CheckIcon from '../../pages/icons/CheckIcon'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import history1 from '../../public/images/history1.png'
import history2 from '../../public/images/history2.png'
import history3 from '../../public/images/history3.png'
import history4 from '../../public/images/history4.png'
import history6 from '../../public/images/history6.png'
import history7 from '../../public/images/history7.png'
import history8 from '../../public/images/history8.png'
import history9 from '../../public/images/history9.png'
import Trased from '../../pages/icons/TrasedIcon'
const HowWeWorkComponent = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [openDialog, setOpenDialog] = useState(false)
  const intl = useIntl()
  return (
    <>
      {mdDown ?
        <Box
          sx={{
          backgroundColor:'#181D24',
          paddingBottom:'70px',
          paddingTop:'70px',
        }}>
          <Box sx={{
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            marginLeft:'20px',
            marginRight:'20px',
          }}
          >
            <Box
              component={'img'}
              src={history9.src}
              alt={'how'}
              sx={{ width: '63px',height:'80px',marginBottom:'15px'  }}
            />
            <Box sx={{
              display:'flex',
              flexDirection:'column',
              alignItems:'center'
            }}>
              <Typography sx={{fontSize:'32px', fontWeight:'700'}}>
                2022<
                /Typography>
              <Typography sx={{textAlign:'center',fontSize:'14px', fontWeight:'400'}}>
                {intl.$t({ id: 'howWork.new.one' })}
              </Typography>
            </Box>

          </Box>
          <Box sx={{
            marginTop:'30px',
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            marginLeft:'20px',
            marginRight:'20px',
          }}
          >
            <Box
              component={'img'}
              src={history8.src}
              alt={'how'}
              sx={{ width: '63px',height:'80px',marginBottom:'15px'  }}
            />
            <Box sx={{
              display:'flex',
              flexDirection:'column',
              alignItems:'center'
            }}>
              <Typography sx={{fontSize:'32px', fontWeight:'700'}}>
                2022<
                /Typography>
              <Typography sx={{textAlign:'center',fontSize:'14px', fontWeight:'400'}}>
                {intl.$t({ id: 'howWork.new.two' })}
              </Typography>
            </Box>

          </Box>
          <Box sx={{
            marginTop:'30px',
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            marginLeft:'20px',
            marginRight:'20px',
          }}
          >
            <Box
              component={'img'}
              src={history7.src}
              alt={'how'}
              sx={{ width: '63px',height:'80px',marginBottom:'15px'  }}
            />
            <Box sx={{
              display:'flex',
              flexDirection:'column',
              alignItems:'center'
            }}>
              <Typography sx={{fontSize:'32px', fontWeight:'700'}}>
                2023
              </Typography>
              <Typography sx={{textAlign:'center',fontSize:'14px', fontWeight:'400'}}>
                {intl.$t({ id: 'howWork.new.three' })}
              </Typography>
            </Box>
          </Box>
          <Box sx={{
            marginTop:'30px',
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            marginLeft:'20px',
            marginRight:'20px',
          }}
          >
            <Box
              component={'img'}
              src={history6.src}
              alt={'how'}
              sx={{ width: '63px',height:'80px',marginBottom:'15px'  }}
            />
            <Box sx={{
              display:'flex',
              flexDirection:'column',
              alignItems:'center'
            }}>
              <Typography sx={{fontSize:'32px', fontWeight:'700'}}>
                2024<
                /Typography>
              <Typography sx={{textAlign:'center',fontSize:'14px', fontWeight:'400'}}>
                {intl.$t({ id: 'howWork.new.four' })}
              </Typography>
            </Box>

          </Box>
        </Box>
        :
        <Box
          sx={{
            backgroundColor:'#181D24',
            paddingBottom:'70px',
            paddingTop:'70px',
          }}
        >
          <Container>
            <Box
              sx={{display: 'grid',gridTemplateColumns:'1fr'}}
            >
              <Box
                sx={{
                  display:'grid',
                  gridTemplateColumns:'1fr 1fr 1fr 1fr',
                  columnGap:'30px',
                  paddingLeft:'15px'
                }}
              >
                <Box
                  component={'img'}
                  src={history1.src}
                  alt={'how'}
                  sx={{ width: '250px',height:'80px'  }}
                />
                <Box
                  component={'img'}
                  src={history2.src}
                  alt={'how'}
                  sx={{ width:  '250px',height:'80px',paddingLeft:'10px'  }}
                />
                <Box
                  component={'img'}
                  src={history3.src}
                  alt={'how'}
                  sx={{ width:  '250px' ,height:'80px',paddingLeft:'10px'}}
                />

                <Box
                  component={'img'}
                  src={history4.src}
                  alt={'how'}
                  sx={{ width:  '53px'  , height:'80px'}}
                />
              </Box>
              <Box
                sx={{
                  paddingTop:'20px',
                  paddingBottom:'20px',
                  display:'grid',
                  gridTemplateColumns:'1fr 1fr 1fr 1fr',
                  columnGap:'30px'
                }}
              >
                <Box >
                  <Typography
                    sx={{
                      fontSize:'32px',
                      fontWeight:'700'
                    }}
                  >
                    2022
                  </Typography>
                  <Typography
                    sx={{
                      fontSize:'14px',
                      fontWeight:'400'
                    }}
                  >
                    {intl.$t({ id: 'howWork.new.one' })}
                  </Typography>
                </Box>
                <Box >
                  <Typography
                    sx={{
                      fontSize:'32px',
                      fontWeight:'700'
                    }}
                  >
                    2022
                  </Typography>
                  <Typography
                    sx={{
                      fontSize:'14px',
                      fontWeight:'400'
                    }}
                  >
                    {intl.$t({ id: 'howWork.new.two' })}
                  </Typography>
                </Box>
                <Box >
                  <Typography
                    sx={{
                      fontSize:'32px',
                      fontWeight:'700'
                    }}
                  >
                    2023
                  </Typography>
                  <Typography
                    sx={{
                      fontSize:'14px',
                      fontWeight:'400'
                    }}
                  >
                    {intl.$t({ id: 'howWork.new.three' })}
                  </Typography>
                </Box>
                <Box>
                  <Typography
                    sx={{
                      fontSize:'32px',
                      fontWeight:'700'
                    }}
                  >2024
                  </Typography>
                  <Typography
                    sx={{
                      fontSize:'14px',
                      fontWeight:'400'
                    }}
                  >
                    {intl.$t({ id: 'howWork.new.four' })}
                  </Typography>
                </Box>
              </Box>

            </Box>

          </Container>

        </Box>
      }


      <Container sx={{ color: 'black', mb: '50px' }} maxWidth={'lg'}>
        {!mdDown && (
        <Box
          sx={{
            color:'#CF893C',
            columnGap:'20px',
            display:'flex',
            flexDirection:'row',
            alignItems:'center',
            paddingTop:'60px'}}>
          <Typography sx={{fontSize:'12px', fontWeight:'700'}}>02</Typography>
          <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '20px 0px', width: '80px' }}  />
          <Typography sx={{fontSize:'12px', fontWeight:'700',textTransform:'uppercase'}}>{intl.$t({ id: 'general.header.navigation.aboutUs' })}</Typography>
        </Box>
        )}
        {!mdDown && (
          <Typography
            sx={{
              marginLeft: '130px',
              fontSize: '36px',
              fontWeight: '700',
              mt: '50px',
              mb: '50px',
              textTransform: 'uppercase',
            }}
            variant={'h1'}
          >
            {intl.$t({ id: 'howWork.howWork.title' })}
          </Typography>
        )}
        <Box sx={{ display: 'grid', gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' } }}>
          {!mdDown && (
            <Box
              component={'img'}
              src={howImage.src}
              alt={'how'}
              sx={{ width: { xs: '400px', md: '400px' } }}
            />
          )}
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              rowGap: '30px',
              marginTop: { xs: '40px', md: '0px' },
            }}
          >
            <a name={'AboutUs'}>
              <Typography sx={{ fontSize: '34px', fontWeight: { xs: '800', md: '600' } }}>
                {intl.$t({ id: 'howWork.howWork.subTitle' })}
              </Typography>
            </a>
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              {intl.$t({ id: 'howWork.howWork.subText1' })}
            </Typography>
            {mdDown && (
              <Box
                component={'img'}
                src={howImage.src}
                alt={'how'}
                sx={{ width: { xs: '350px', md: '400px' } }}
              />
            )}
            <Typography sx={{ fontSize: '16px', fontWeight: '400px' }}>
              {intl.$t({ id: 'howWork.howWork.subText2' })}
            </Typography>
            <Button
              variant={'outlined'}
              sx={{
                color: 'rgba(0, 0, 0, 1)',
                width: '350px',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'white' },
              }}
              endIcon={<EastIcon />}
              onClick={() => setOpenDialog(true)}
            >
              {intl.$t({ id: 'howWork.howWork.button' })}
            </Button>
          </Box>
        </Box>
      </Container>
      <Box sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', pb: '70px', pt: '10px' }}>
        <Container maxWidth={'lg'}>
          <Typography
            sx={{
              width: { xs: '100%', md: '500px' },
              fontSize: '32px',
              fontWeight: '600',
              mt: { xs: '20px', md: '100px' },
              mb: '50px',
              color: 'rgba(20, 20, 20, 1)',
            }}
            variant={'h1'}
          >
            {intl.$t({ id: 'howWork.internationalServices.title' })}
          </Typography>
          <Box
            sx={{
              color: 'black',
              display: 'grid',
              gridTemplateColumns: { xs: '1fr', md: '2fr 1fr' },
              columnGap: { xs: '0px', md: '30px' },
            }}
          >
            <Box>
              <Typography sx={{ fontSize: '16px', fontWeight: '400' }}>
                {intl.$t({ id: 'howWork.internationalServices.subText' })}
              </Typography>
              {mdDown && (
                <Box
                  component={'img'}
                  src={howImage3.src}
                  alt={'how'}
                  sx={{ width: { xs: '400px', md: '400px' }, marginTop: '30px' }}
                />
              )}
              <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '30px 0px' }} />
              <Box component={'ul'} sx={{ listStyleType: 'none', padding: '0' }}>
                <Box component={'li'} sx={{ display: 'flex', columnGap: '10px' }}>
                  <CheckIcon width={50} height={20} />
                  <Box>
                    <span style={{ fontWeight: 600 }}>
                      {intl.$t({ id: 'howWork.internationalServices.list.firstBold' })}
                    </span>{' '}
                    {intl.$t({ id: 'howWork.internationalServices.list.first' })}
                  </Box>
                </Box>
                <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '30px 0px' }} />
                <Box component={'li'} sx={{ display: 'flex', columnGap: '10px' }}>
                  <CheckIcon width={20} height={20} />
                  <Box>
                    <span style={{ fontWeight: 600 }}>
                      {intl.$t({ id: 'howWork.internationalServices.list.secondBold' })}
                    </span>{' '}
                    {intl.$t({ id: 'howWork.internationalServices.list.second' })}
                  </Box>
                </Box>
                <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '30px 0px' }} />
                <Box component={'li'} sx={{ display: 'flex', columnGap: '10px' }}>
                  <CheckIcon width={22} height={20} />
                  <Box>
                    <span style={{ fontWeight: 600 }}>
                      {intl.$t({ id: 'howWork.internationalServices.list.thirdBold' })}
                    </span>{' '}
                    {intl.$t({ id: 'howWork.internationalServices.list.third' })}
                  </Box>
                </Box>
                <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '30px 0px' }} />
                <Box component={'li'} sx={{ display: 'flex', columnGap: '10px' }}>
                  <CheckIcon width={30} height={20} />
                  <Box>
                    <span style={{ fontWeight: 700 }}>
                      {intl.$t({ id: 'howWork.internationalServices.list.fourthBold' })}
                    </span>{' '}
                    {intl.$t({ id: 'howWork.internationalServices.list.fourth' })}
                  </Box>
                </Box>
              </Box>
              <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '30px 0px' }} />
              <Typography
                sx={{ fontSize: '16px', fontWeight: '300', my: '15px', fontStyle: 'italic' }}
              >
                {intl.$t({ id: 'howWork.internationalServices.subText2' })}
              </Typography>
              <Button
                sx={{
                  marginTop: '30px',
                  backgroundColor: 'rgba(249, 205, 150, 1)',
                  color: 'rgba(0, 0, 0, 1)',
                  width: '350px',
                  textTransform: 'none',
                  fontSize: '14px',
                  fontWeight: '600',
                  padding: '20px 30px',
                  '&:hover': { background: 'white' },
                }}
                endIcon={<EastIcon />}
                onClick={() => setOpenDialog(true)}
              >
                {mdDown
                  ? `${intl.$t({ id: 'howWork.internationalServices.button.mdUp' })}`
                  : `${intl.$t({ id: 'howWork.internationalServices.button.mdDown' })}`}
              </Button>
            </Box>
            {!mdDown && (
              <Box
                component={'img'}
                src={howImage3.src}
                alt={'how'}
                sx={{ width: { xs: '400px', md: '400px' }, marginRight: '90px' }}
              />
            )}
          </Box>
        </Container>
      </Box>
      <ContactDialog open={openDialog} onClose={() => setOpenDialog(false)} />
    </>
  )
}

export default HowWeWorkComponent

import React from 'react';
import {Box} from "@mui/material";
import Header from './Header';
import Footer from "./Footer";

const Layout = ({children}) => {
    return (
        <Box>
            <Header></Header>
            {children}
            <Footer></Footer>
        </Box>
    );
};

export default Layout;
import React, { useState } from 'react'
import { Box, Button, Container, Divider, IconButton, Stack, Typography, useMediaQuery } from '@mui/material'
import Logo from '@/components/layout/Logo'
import WhatsappIcon from '../../pages/icons/WhatsappIcon'
import FacebookIcon from '../../pages/icons/FacebookIcon'
import ViberIcon from '../../pages/icons/ViberIcon'
import TelegramIcon from '../../pages/icons/TelegramIcon'
import Link from 'next/link'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import InstagramIcon from '@mui/icons-material/Instagram'
import { useIntl } from 'react-intl'
import howImage3 from '../../public/images/logoblack2.svg'
import useScrollPosition from "../../hooks/useScrollPosition";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";

const Footer = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()
  const scroll = useScrollPosition();
  const handleUpToTop = () => window.scrollTo(0, 0);

  return (
    <Box sx={{ backgroundColor: 'white' }}>
      {mdDown ? (
        <Container
          maxWidth={'lg'}
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            rowGap: '15px',
            padding: '15px 15px',
          }}
        >
          <Box
            component={'img'}
            src={howImage3.src}
            alt={'how'}
            sx={{ width: '112px', height: '22' }}
          />
          <Button
            variant={'outlined'}
            sx={{
              border: '1px solid rgb(193,141,60)',
              color: 'rgb(193,141,60)',
              width: '140px',
              height: '40px',
              fontSize: '11px',
              fontWeight: '400',
              marginLeft: '35px',
            }}
            onClick={() => setContactFormOpen(true)}
          >
            {intl.$t({ id: 'general.header.connection' })}
          </Button>
          {scroll > 200 && (
            <IconButton
              aria-label="up"
              onClick={handleUpToTop}
              color="black"
              sx={{
                position: "fixed",
                right: "20px",
                bottom: "20px",
                background: "#d1a25d",
                zIndex: 10,
                "&:hover": {
                  background: "#d1a25d",
                },
              }}
            >
              <ArrowUpwardIcon fill={"#000"} />
            </IconButton>
          )}
        </Container>
      ) : (
        <Container maxWidth={'lg'}>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Logo isBlack={true} />
            <Stack
              direction="row"
              spacing={2}
              sx={{
                height: '100px',
                marginLeft: '50px',
                '& > div': {
                  color: 'black',
                  whiteSpace: 'nowrap',
                  fontSize: '16px',
                  px: '10px',
                  display: 'flex',
                  alignItems: 'center',
                  borderBottom: `1px solid white`,
                },
              }}
            >
              <Box>{intl.$t({ id: 'general.header.navigation.main' })}</Box>
              <Box>
                <Link href={'#AboutUs'}>
                  {intl.$t({ id: 'general.header.navigation.aboutUs' })}
                </Link>
              </Box>
              <Box>
                <Link href={'#Countries'}>
                  {intl.$t({ id: 'general.header.navigation.countries' })}
                </Link>
              </Box>
              <Box>
                <Link href={'#Services'}>
                  {intl.$t({ id: 'general.header.navigation.services' })}
                </Link>
              </Box>
              <Box>
                <Link href={'#Gallery'}>
                  {intl.$t({ id: 'general.header.navigation.gallery' })}
                </Link>
              </Box>
              <Box>
                <Link href={'#Feedbacks'}>
                  {intl.$t({ id: 'general.header.navigation.reviews' })}
                </Link>
              </Box>
              <Box>
                <Link href='/contact'>
                  {intl.$t({ id: 'general.header.navigation.contacts' })}
                </Link>
              </Box>
            </Stack>

            <Stack
              direction="row"
              sx={{
                marginLeft: '100px',
                columnGap: '20px',
                '& > svg': {
                  '&:hover': {
                    '& *': {
                      fill: 'rgba(249, 205, 150, 1)',
                    },
                  },
                },
              }}
            >
              <WhatsappIcon />
              <a href="https://www.facebook.com/pochovai.in.ua" target="_blank" rel="noreferrer">
                <FacebookIcon />
              </a>
              <ViberIcon />
              <a href="https://t.me/perevozka_angel" target="_blank" rel="noreferrer">
                <TelegramIcon />
              </a>
              <a href="https://www.instagram.com/pohovai.in.ua" target="_blank" rel="noreferrer">
                {' '}
                <InstagramIcon sx={{ color: 'grey', width: '20px', height: '20px' }} />{' '}
              </a>
            </Stack>
            <Button
              variant={'outlined'}
              sx={{
                minWidth:'130px',
                border: '1px solid rgba(249, 205, 150, 1)',
                color: 'rgba(249, 205, 150, 1)',
                height: '55px',
                fontSize: '11px',
                fontWeight: '400',
                marginLeft: '35px',
              }}
              onClick={() => setContactFormOpen(true)}
            >
              {intl.$t({ id: 'general.header.connection' })}
            </Button>
          </Box>
          <Divider sx={{ borderColor: 'rgba(234, 234, 234, 1)' }} />
          <Box sx={{ display: 'flex', justifyContent: 'space-between', padding: '20px' }}>
            <Typography
              sx={{ color: 'rgba(153, 163, 175, 1)', fontSize: '14px' }}
            >{`© ${new Date().getFullYear()}. Всі права захищені.`}</Typography>
            <Box sx={{ display: 'flex', columnGap: '30px' }}>
              <Typography
                sx={{
                  color: 'rgba(153, 163, 175, 1)',
                  borderBottom: '1px solid white',
                  fontSize: '14px',
                  '&:hover': {
                    color: 'rgba(249, 205, 150, 1)',
                    borderBottom: '1px solid rgba(249, 205, 150, 1)',
                  },
                }}
              >
                {intl.$t({ id: 'general.header.political' })}
              </Typography>
              <Typography
                sx={{
                  color: 'rgba(153, 163, 175, 1)',
                  borderBottom: '1px solid white',
                  fontSize: '14px',
                  '&:hover': {
                    color: 'rgba(249, 205, 150, 1)',
                    borderBottom: '1px solid rgba(249, 205, 150, 1)',
                  },
                }}
              >
                {intl.$t({ id: 'general.header.rules' })}
              </Typography>
            </Box>
          </Box>
        </Container>
      )}

      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
    </Box>
  )
}

export default Footer

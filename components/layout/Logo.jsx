import React from 'react'
import logo from 'public/images/logowhite.svg'
import logoBlack from 'public/images/logo-darkkk.svg'
const Logo = ({ isBlack, ...props }) => {
  return (
    <img
      src={isBlack ? logoBlack.src : logo.src}
      alt={'logo'}
      width={'130px'}
      height={'70px'}
      {...props}
    />
  )
}

export default Logo

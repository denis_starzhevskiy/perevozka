import React, { useState } from 'react'
import {
  Box,
  Button,
  Container,
  Dialog,
  Divider,
  FormControl,

  MenuItem,
  Select,
  Stack,
  Tooltip,
  Typography,
  useMediaQuery,
} from '@mui/material'
import InstagramIcon from '@mui/icons-material/Instagram'
import EmailIcon from '@mui/icons-material/Email'
import Ukraine from 'public/images/Ukraine.png'
import Slovak from 'public/images/Slovak.png'
import Poland from 'public/images/Poland.png'
import Italia from 'public/images/Italia.png'
import Logo from '@/components/layout/Logo'
import FacebookIcon from '../../pages/icons/FacebookIcon'
import ViberIcon from '../../pages/icons/ViberIcon'
import TelegramIcon from '../../pages/icons/TelegramIcon'
import WhatsappIcon from '../../pages/icons/WhatsappIcon'
import MenuIcon from '@mui/icons-material/Menu'
import CloseIcon from '@mui/icons-material/Close'
import Link from 'next/link'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import { LOCALES } from '../../public/i18/locales'
import { useLanguageStore } from '../../providers/StateProvider'

export const HeaderComponent = ({ mdDown }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const changeLanguage = useLanguageStore(state => state.changeLanguage)
  const language = useLanguageStore(state => state.language)
  const intl = useIntl()

  const handleChange = event => {
    changeLanguage(event.target.value)
  }


  if (mdDown) {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'space-between', padding: '20px', }}>
        <Logo />
        <Box sx={{ display: 'flex', alignItems: 'center', columnGap: '5px' }}>
          <Button
            variant={'outlined'}
            sx={{
              border: '1px solid rgba(249, 205, 150, 1)',
              color: 'rgba(249, 205, 150, 1)',
              width: '170px',
              height: '40px',
              fontSize: '11px',
              fontWeight: '500',
              marginLeft: '15px',
            }}
            onClick={() => setContactFormOpen(true)}
          >
            {intl.$t({ id: 'general.contactForm.button' })}
          </Button>
          {isMenuOpen ? (
            <CloseIcon fontSize={'large'} />
          ) : (
            <MenuIcon fontSize={'large'} onClick={() => setIsMenuOpen(true)} />
          )}
        </Box>
        {isMenuOpen && (
          <Dialog open={isMenuOpen} fullScreen>
            <Box
              sx={{
                backgroundColor: 'rgba(22, 26, 32, 1)',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: '15px',
                  width: '100%',
                  height: '80px',
                }}
              >
                <Logo />

                <Button
                  variant={'outlined'}
                  sx={{
                    border: '1px solid rgba(249, 205, 150, 1)',
                    color: 'rgba(249, 205, 150, 1)',
                    width: '200px',
                    minWidth:'150px',
                    height: '40px',
                    fontSize: '11px',
                    fontWeight: '400',
                    marginLeft: '20px',
                  }}
                  onClick={() => setContactFormOpen(true)}
                >
                  {intl.$t({ id: 'general.header.connection2' })}
                </Button>

                <CloseIcon fontSize={'large'} onClick={() => setIsMenuOpen(false)} />
              </Box>

              <Divider sx={{ margin: '5px 0px 15px 0px', width: '100%' }} />

              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <Stack
                  spacing={2}
                  sx={{
                    '& > div': {
                      whiteSpace: 'nowrap',
                      fontSize: { xs: '25px', md: '16px' },
                      fontWeight: 600,
                      display: 'flex',
                      alignItems: 'center',
                      rowGap: '15px',
                      justifyContent: 'center',
                      '&:hover': {
                        color: 'rgba(249, 205, 150, 1)',
                      },
                    },
                  }}
                >
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href={"/"}>
                      {intl.$t({ id: 'general.header.navigation.main' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href={'#AboutUs'}>
                      {intl.$t({ id: 'general.header.navigation.aboutUs' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href='/countries'>
                      {intl.$t({ id: 'general.header.navigation.countries' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href='/services'>
                      {intl.$t({ id: 'general.header.navigation.services' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href={'#Gallery'}>
                      {intl.$t({ id: 'general.header.navigation.gallery' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href={'#Feedbacks'}>
                      {intl.$t({ id: 'general.header.navigation.reviews' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href='/contact'>
                      {intl.$t({ id: 'general.header.navigation.contacts' })}
                    </Link>
                  </Box>
                  <Box onClick={() => setIsMenuOpen(false)}>
                    <Link href='/ritual-attributes'>
                      {intl.$t({ id: 'general.header.navigation.blog' })}
                    </Link>
                  </Box>
                </Stack>
                <Box sx={{ display: 'flex', flexDirection: 'column', rowGap: '20px' }}>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'center',
                      minWidth: 200,
                      marginLeft: '10px',
                      marginTop: '20px',
                      marginBottom: '10px',
                    }}
                  >
                    <Select
                      sx={{
                        "&.MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
                          border: "none"
                        },
                        width:'80px',
                        fontSize: '15px',
                        color: 'grey',
                        '& > svg' : {
                          color: 'grey'
                        }
                      }}
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={language}
                      label="мова"
                      onChange={handleChange}
                    >
                      <MenuItem value={LOCALES.UA}>UA</MenuItem>
                      <MenuItem value={LOCALES.RU}>RU</MenuItem>
                    </Select>
                  </Box>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      justifyContent: 'space-between',
                      rowGap: '20px',
                      fontSize: '20px',
                      alignItems: 'center',
                    }}
                  >
                    <a href="tel:+38-099-183-34-94">
                      <Box
                        component={'span'}
                        sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                      >
                        <img
                          src={Ukraine.src}
                          alt={'ukraine'}
                          style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                        />
                        +38 (099) 183-34-94
                      </Box>
                    </a>

                    <a href="tel:+421 919 196 568">
                      <Box
                        component={'span'}
                        sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                      >
                        <img
                          src={Slovak.src}
                          alt={'slovak'}
                          style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                        />
                        +421 919 196 568
                      </Box>
                    </a>
                    <a href="tel:+393519190937">
                      <Box
                        component={'span'}
                        sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                      >
                        <img
                          src={Italia.src}
                          alt={'Cz'}
                          style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                        />
                        +393519190937
                      </Box>
                    </a>
                    <a href="tel:+48513355559">
                      <Box
                        component={'span'}
                        sx={{ display: 'flex', alignItems: 'center', columnGap: '10px'}}
                      >
                        <img
                          src={Poland.src}
                          alt={'slovak'}
                          style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                        />
                        +48513355559
                      </Box>
                    </a>

                      <Box
                        direction="row"
                        sx={{
                          display: 'flex',
                          marginTop: 'auto',
                          marginBottom: '30px',
                          columnGap: '20px',
                          '& > svg': {
                            '&:hover': {
                              '& *': {
                                fill: 'rgba(249, 205, 150, 1)',
                              },
                            },
                          },
                        }}
                      >
                        <a href="tel:+38-099-183-34-94">
                          <WhatsappIcon />
                        </a>
                        <a
                          href="https://www.facebook.com/pochovai.in.ua"
                          target="_blank"
                          rel="noreferrer"
                        >
                          <FacebookIcon />
                        </a>
                        <a href="tel:+38-099-183-34-94">
                          <ViberIcon />
                        </a>
                        <a href="https://t.me/perevozka_angel" target="_blank" rel="noreferrer">
                          <TelegramIcon />
                        </a>
                        <a
                          href="https://www.instagram.com/pohovai.in.ua"
                          target="_blank"
                          rel="noreferrer"
                        >
                          {' '}
                          <InstagramIcon sx={{ color: 'grey', width: '20px', height: '20px' }} />{' '}
                        </a>
                      </Box>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Dialog>
        )}
        <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
      </Box>
    )
  }

  return (
    <>
      <Logo style={{marginRight: '5px'}}/>
      <Select
        sx={{
          borderRadius: 0,
          "& .MuiOutlinedInput-notchedOutline": {
            border: 0
          },
          "&.MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
            border: "none"
          },
          fontSize: '13px',
          color: 'grey',
          '& > svg' : {
            color: 'grey'
          }
        }}
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={language}
        label="мова"
        onChange={handleChange}
      >
        <MenuItem value={LOCALES.UA}>UA</MenuItem>
        <MenuItem value={LOCALES.RU}>RU</MenuItem>
      </Select>
      <Stack
        direction="row"
        spacing={2}
        sx={{
          marginLeft: '5px',
          '& > div': {
            whiteSpace: 'nowrap',
            fontSize: '13px',
            height: '80px',
            px: '5px',
            display: 'flex',
            alignItems: 'center',
            borderBottom: `1px solid rgba(22, 26, 32, 1)`,
            '&:hover': {
              color: 'rgba(249, 205, 150, 1)',
              borderBottom: `1px solid rgba(249, 205, 150, 1)`,
            },
          },
        }}
      >
        <Box>
          <Link href={"/"}>
          {intl.$t({ id: 'general.header.navigation.main' })}
          </Link>
        </Box>
        <Box>
          <Link href={'#AboutUs'}>{intl.$t({ id: 'general.header.navigation.aboutUs' })}</Link>
        </Box>
        <Box>
          <Link href='/countries'>{intl.$t({ id: 'general.header.navigation.countries' })}</Link>
        </Box>
        <Box>
          <Link href='/services'>{intl.$t({ id: 'general.header.navigation.services' })}</Link>
        </Box>
        <Box>
          <Link href={'#Gallery'}>{intl.$t({ id: 'general.header.navigation.gallery' })}</Link>
        </Box>
        <Box>
          <Link href={'#Feedbacks'}>{intl.$t({ id: 'general.header.navigation.reviews' })}</Link>
        </Box>
        <Box>
          <Link href="/contact">{intl.$t({ id: 'general.header.navigation.contacts' })}</Link>
        </Box>
        <Box>
          <Link href="/ritual-attributes">{intl.$t({ id: 'general.header.navigation.blog' })}</Link>
        </Box>
      </Stack>

      <Stack
        direction="row"
        sx={{
          marginLeft: '20px',
          columnGap: '20px',
          '& > svg': {
            '&:hover': {
              '& *': {
                fill: 'rgba(249, 205, 150, 1)',
              },
            },
          },
        }}
      >
        <a href="https://www.facebook.com/pochovai.in.ua" target="_blank" rel="noreferrer">
          <FacebookIcon />
        </a>
        <Tooltip title="+38-099-183-34-94" placement="top">
          <Box>
            <ViberIcon />
          </Box>
        </Tooltip>
        <a href="https://t.me/perevozka_angel" target="_blank" rel="noreferrer">
          <TelegramIcon />
        </a>
        <a href="https://www.instagram.com/pohovai.in.ua" target="_blank" rel="noreferrer">
          {' '}
          <InstagramIcon sx={{ color: 'grey', width: '20px', height: '20px' }} />{' '}
        </a>
      </Stack>

      <Button
        variant={'outlined'}
        sx={{
          border: '1px solid rgba(249, 205, 150, 1)',
          color: 'rgba(249, 205, 150, 1)',
          width: '200px',
          minWidth:'150px',
          height: '40px',
          fontSize: '11px',
          fontWeight: '400',
          marginLeft: '20px',
        }}
        onClick={() => setContactFormOpen(true)}
      >
        {intl.$t({ id: 'general.header.connection2' })}
      </Button>

      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />

      </>
  )
}
const Header = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  return (
    <Box sx={{ backgroundColor: 'rgba(22, 26, 32, 1)' }}>
      {mdDown ? (
        <HeaderComponent mdDown />
      ) : (
        <>
          <Box sx={{ borderBottom: '1px solid white', padding: '10px 0px' }}>
            <Container maxWidth={'lg'} sx={{ display: 'flex', justifyContent: 'space-between' }}>
              <Box sx={{ display: 'flex', columnGap: '5px', alignItems: 'center' }}>
                <EmailIcon sx={{ color: 'rgba(126, 126, 126, 1)' }} />
                <address><a
                  href="mailto:pohovai.in.ua@gmail.com">pohovai.in.ua@gmail.com</a>
                </address>


              </Box>

              <Box
                sx={{ display: 'flex', columnGap: '20px', fontSize: '16px', alignItems: 'center' }}
              >
                <a href="tel:+38-099-183-34-94">
                <Box
                    component={'span'}
                    sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                  >
                    <img
                      src={Ukraine.src}
                      alt={'ukraine'}
                      style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                    />
                    +38 099 183-34-94
                  </Box>
                </a>
                <a href="tel:+421 919 196 568">
                  <Box
                    component={'span'}
                    sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                  >
                    <img
                      src={Slovak.src}
                      alt={'slovak'}
                      style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                    />
                    +421 919 196 568
                  </Box>
                </a>
                <a href="tel:+393519190937">
                  <Box
                    component={'span'}
                    sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                  >
                    <img
                      src={Italia.src}
                      alt={'Cz'}
                      style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                    />
                    +39 351 919 0937
                  </Box>
                </a>
                <a href="tel:+48513355559">
                  <Box
                    component={'span'}
                    sx={{ display: 'flex', alignItems: 'center', columnGap: '10px' }}
                  >
                    <img
                      src={Poland.src}
                      alt={'slovak'}
                      style={{ borderRadius: '50%', width: '20px', height: '20px' }}
                    />
                    +48 513 355 559
                  </Box>
                </a>
              </Box>
            </Container>
          </Box>
          <Box>
            <Container maxWidth={'lg'} sx={{ display: 'flex', alignItems: 'center' }}>
              <HeaderComponent />
            </Container>
          </Box>
        </>
      )}
    </Box>
  )
}

export default Header

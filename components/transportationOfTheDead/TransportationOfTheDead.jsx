import React from 'react'
import { Box, Container, Divider, Typography, useMediaQuery } from '@mui/material'
import Trased from '../../pages/icons/TrasedIcon'
import DocumentIcon from '../../pages/icons/DocumentIcon'
import BuidingIcon from '../../pages/icons/BuidingIcon'
import CardIcon from '../../pages/icons/CardIcon'
import { useIntl } from 'react-intl'

const TransportationOfTheDead = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const intl = useIntl()

  return (
    <Container
      sx={{
        color: 'black',
        display: 'flex',
        flexDirection: 'column',
        rowGap: '50px',
        paddingTop: { xs: '50px', md: '100px' },
        paddingBottom: { xs: '0px', md: '110px' },
      }}
      maxWidth={'lg'}
    >
      <a name={'TransportationOfTheDead'}>
        <Typography
          sx={{
            fontSize: '32px',
            fontWeight: '600',
            textAlign: { xs: 'start', md: 'center' },
          }}
          variant={'h1'}
        >
          {intl.$t({ id: 'transportation.title' })}{' '}
          <Box component={'span'} sx={mdDown ? {} : { color: theme => theme.palette.primary.main }}>
            Pohovai.in.ua
          </Box>
        </Typography>
      </a>

      <Typography
        sx={{ fontSize: '16px', fontWeight: '500', textAlign: { xs: 'start', md: 'center' } }}
        variant={'h1'}
      >
        {intl.$t({ id: 'transportation.subTitle' })}
      </Typography>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr 1fr ' },
          gap: '20px',
          '& > div': {
            minHeight: '350px',
            position: 'relative',
          },
        }}
      >
        <Box
          sx={{
            padding: '15px',
            backgroundColor: 'rgba(249, 249, 249, 1)',
            border: '1px solid rgba(223, 231, 240, 1)',
            '&:hover': {transition: '0.9s', background: 'linear-gradient(98.79deg, #15181E 0%, #1F252D 50.52%, #15181E 100%)', color:'white' },
          }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', height: '40px' }}>
            <Trased width={50} height={20} />
            <Typography sx={{ fontSize: '18px', fontWeight: '700' }}>
              {intl.$t({ id: 'transportation.steps.step1.title' })}
            </Typography>
          </Box>
          <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '20px 0px' }} />
          <Typography sx={{ fontSize: '14px', fontWeight: '400' }}>
            <span style={{ fontWeight: 600 }}>
              {intl.$t({ id: 'transportation.steps.step1.text' })}
            </span>
            {intl.$t({ id: 'transportation.steps.step1.text2' })}
          </Typography>
        </Box>
        <Box
          sx={{
            padding: '15px',
            backgroundColor: 'rgba(249, 249, 249, 1)',
            border: '1px solid rgba(223, 231, 240, 1)',
            '&:hover': {transition: '0.9s', background: 'linear-gradient(98.79deg, #15181E 0%, #1F252D 50.52%, #15181E 100%)', color:'white' },
          }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', height: '40px' }}>
            <DocumentIcon width={50} height={20} />
            <Typography sx={{ fontSize: '18px', fontWeight: '700' }}>
              {intl.$t({ id: 'transportation.steps.step2.title' })}
            </Typography>
          </Box>
          <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '20px 0px' }} />
          <Typography sx={{ fontSize: '14px', fontWeight: '400' }}>
            <span style={{ fontWeight: 600 }}>
              {intl.$t({ id: 'transportation.steps.step2.text' })}
            </span>
            {intl.$t({ id: 'transportation.steps.step2.text2' })}
          </Typography>
        </Box>
        <Box
          sx={{
            padding: '15px',
            backgroundColor: 'rgba(249, 249, 249, 1)',
            border: '1px solid rgba(223, 231, 240, 1)',
            '&:hover': {transition: '0.9s', background: 'linear-gradient(98.79deg, #15181E 0%, #1F252D 50.52%, #15181E 100%)', color:'white' },
          }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', height: '40px' }}>
            <BuidingIcon width={50} height={30} />
            <Typography sx={{ fontSize: '18px', fontWeight: '700' }}>
              {intl.$t({ id: 'transportation.steps.step3.title' })}
            </Typography>
          </Box>
          <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '20px 0px' }} />
          <Typography sx={{ fontSize: '14px', fontWeight: '400' }}>
            <span style={{ fontWeight: 600 }}>
              {intl.$t({ id: 'transportation.steps.step3.text' })}
            </span>
            {intl.$t({ id: 'transportation.steps.step3.text2' })}
          </Typography>
        </Box>
        <Box
          sx={{
            padding: '15px',
            backgroundColor: 'rgba(249, 249, 249, 1)',
            border: '1px solid rgba(223, 231, 240, 1)',
            '&:hover': {transition: '0.9s', background: 'linear-gradient(98.79deg, #15181E 0%, #1F252D 50.52%, #15181E 100%)', color:'white' },
          }}
        >
          <Box sx={{ display: 'flex', alignItems: 'center', columnGap: '10px', height: '40px' }}>
            <CardIcon width={50} height={20} />
            <Typography sx={{ fontSize: '18px', fontWeight: '700' }}>
              {intl.$t({ id: 'transportation.steps.step4.title' })}
            </Typography>
          </Box>
          <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '20px 0px' }} />
          <Typography sx={{ fontSize: '14px', fontWeight: '400' }}>
            <span style={{ fontWeight: 600 }}>
              {intl.$t({ id: 'transportation.steps.step4.text' })}
            </span>
            {intl.$t({ id: 'transportation.steps.step4.text2' })}
          </Typography>
        </Box>
      </Box>
    </Container>
  )
}

export default TransportationOfTheDead

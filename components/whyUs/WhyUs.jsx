import React from 'react'
import { Box, Container, Divider, Typography, useMediaQuery } from '@mui/material'
import Purse from '../../pages/icons/purse'
import FastIcon from '../../pages/icons/fastIcon'
import Fast2 from '../../pages/icons/fast2'
import { useIntl } from 'react-intl'

const WhyUs = () => {
  const intl = useIntl()
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))

  return (
    <Container sx={{ color: 'black', paddingBottom: '70px' }} maxWidth={'lg'}>
      {!mdDown && (
        <Box
          sx={{
            color:'#CF893C',
            columnGap:'20px',
            display:'flex',
            flexDirection:'row',
            alignItems:'center',
            paddingTop:'50px',
          }}>
          <Typography
            sx={{
              fontSize:'12px',
              fontWeight:'700'
          }}
          >
            04
          </Typography>
          <Divider
            sx={{
              backgroundColor: 'rgba(217, 217, 217, 1)',
              margin: '20px 0px', width: '80px' }}
          />
          <Typography
            sx={{
              fontSize:'12px',
              fontWeight:'700' ,
              textTransform:'uppercase'}}
          >
            {intl.$t({ id: 'whyUs.why' })}
          </Typography>
        </Box>
      )}
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: { xs: '1fr', md: '1fr 1fr' },
          columnGap: '50px',
        }}
      >
        <Typography
          sx={{
            fontSize: '32px',
            fontWeight: '550',
            fontStyle: 'Montserrat',
            margin: { xs: '70px 0px 10px 0px', md: '50px 0px 50px 0px' },
            lineHeight: '40px',
          }}
          variant={'h1'}
        >
          {intl.$t({ id: 'whyUs.title' })}
        </Typography>
        <Typography
          sx={{
            fontSize: '14px',
            fontWeight: '400',
            fontStyle: 'italic',
            margin: { xs: '20px 0px 10px 0px', md: '50px 0px 50px 0px' },
            lineHeight: '21px',
          }}
          variant={'h1'}
        >
          {intl.$t({ id: 'whyUs.subTitle' })}
        </Typography>
      </Box>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr' },
          rowGap: '10px',
          marginTop: '30px',
        }}
      >
        <Box sx={{ border: '1px solid rgba(223, 231, 240, 1)' }}>
          <Box sx={{ display: 'flex' }}>
            <Box sx={{ margin: '20px' }}>
              <Purse width={34} height={34} />
            </Box>
            <Typography
              sx={{
                color: 'rgba(184, 187, 193, 1)',
                fontWeight: '600',
                fontSize: '30px',
                marginLeft: '220px',
                marginTop: '15px',
              }}
            >
              01
            </Typography>
          </Box>
          <Typography sx={{ fontWeight: '700', fontSize: '18px', margin: '20px' }} variant={'h1'}>
            {intl.$t({ id: 'whyUs.card1.title' })}
          </Typography>
          <Typography sx={{ fontWeight: '400', fontSize: '14px', margin: '20px' }}>
            {intl.$t({ id: 'whyUs.card1.text' })}
          </Typography>
        </Box>
        <Box sx={{ border: '1px solid rgba(223, 231, 240, 1)' }}>
          <Box sx={{ display: 'flex' }}>
            <Box sx={{ margin: '20px' }}>
              <FastIcon width={36} height={36} />{' '}
            </Box>
            <Typography
              sx={{
                color: 'rgba(184, 187, 193, 1)',
                fontWeight: '600',
                fontSize: '30px',
                marginLeft: '220px',
                marginTop: '15px',
              }}
            >
              02
            </Typography>
          </Box>
          <Typography sx={{ fontWeight: '700', fontSize: '18px', margin: '20px' }} variant={'h1'}>
            {intl.$t({ id: 'whyUs.card2.title' })}
          </Typography>
          <Typography sx={{ fontWeight: '400', fontSize: '14px', margin: '20px' }}>
            {intl.$t({ id: 'whyUs.card2.text' })}
          </Typography>
        </Box>
        <Box sx={{ border: '1px solid rgba(223, 231, 240, 1)' }}>
          <Box sx={{ display: 'flex' }}>
            <Box sx={{ margin: '20px' }}>
              <Fast2 width={36} height={36} />{' '}
            </Box>
            <Typography
              sx={{
                color: 'rgba(184, 187, 193, 1)',
                fontWeight: '600',
                fontSize: '30px',
                marginLeft: '220px',
                marginTop: '15px',
              }}
            >
              03
            </Typography>
          </Box>
          <Typography sx={{ fontWeight: '700', fontSize: '18px', margin: '20px' }} variant={'h1'}>
            {intl.$t({ id: 'whyUs.card3.title' })}
          </Typography>
          <Typography sx={{ fontWeight: '400', fontSize: '14px', margin: '20px' }}>
            {intl.$t({ id: 'whyUs.card3.text' })}
          </Typography>
        </Box>
      </Box>
    </Container>
  )
}

export default WhyUs

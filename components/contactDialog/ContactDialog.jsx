import React from 'react'
import { Dialog } from '@mui/material'
import ContactForm from '@/components/title/ContactForm'

const ContactDialog = ({ open, onClose , ...props }) => {
  return (
    <Dialog open={open} onClose={onClose}>
      <ContactForm isDialog onClose={onClose} {...props}/>
    </Dialog>
  )
}

export default ContactDialog

import React from 'react'
import {
  Box,
  Container,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  useMediaQuery,
} from '@mui/material'
import ContactForm from '@/components/title/ContactForm'
import EmailIcon from '@mui/icons-material/Email'
import Ukraine from '../../public/images/Ukraine.png'
import Poland from 'public/images/Poland.png'
import Italia from 'public/images/Italia.png'
import CountrySlovak from '../../public/images/CountrySlovak.png'
import { useIntl } from 'react-intl'

const Contacts = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const intl = useIntl()

  return (
    <Box sx={{ backgroundColor: 'rgba(249, 249, 249, 1)', padding: '30px 0px 70px 0px' }}>
      <Container sx={{ color: 'black', padding: 'auto', margin: 'auto' }} maxWidth={'lg'}>
        <a name={'Contacts'}>
          <Typography
            sx={{
              margin: 'auto',
              paddingTop: '30px',
              fontSize: '32px',
              fontWeight: '700',
              textAlign:{xs:'center', md:'start'}
            }}
            variant={'h1'}
          >
            {intl.$t({ id: 'general.contact.title' })}
          </Typography>
        </a>
        <Box
          sx={{
            paddingTop: '30px',
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1.5fr 1fr' },
            columnGap: '200px',
          }}
        >
          <Box
            sx={{
              height: 'max-content',
              display: 'flex',
              flexDirection: 'column',
              rowGap: '20px',
              textAlign:{xs:'center', md:'start'}
            }}
          >
            <Typography sx={{ fontSize: '16px', fontWeight: '400' }}>
              {intl.$t({ id: 'general.contact.subTitle' })}
            </Typography>
            <List sx={{ width: '100%' }}>
              <ListItem
                sx={{
                  justifyContent: { xs: 'start', md: 'space-between' },
                  padding: '15px',
                  backgroundColor: 'white',
                  border: '1px solid rgba(223, 231, 240, 1)',
                  '&:hover': {
                    border: theme => `1px solid ${theme.palette.primary.main}`,
                    '& > span': { color: theme => theme.palette.primary.main },
                  },
                }}
              >
                <Box sx={{ display: 'flex', columnGap: '10px', alignItems: 'center' }}>
                  <EmailIcon sx={{ color: 'rgba(207, 137, 60, 1)' }} fontSize={'large'} />
                  <ListItemText
                    sx={{ marginLeft: '20px', '& > span': { fontSize: '16px', fontWeight: 600 } }}
                  >
                    pohovai.in.ua@gmail.com
                  </ListItemText>
                </Box>
              </ListItem>
              <ListItem
                sx={{
                  marginTop: '10px',
                  justifyContent: { xs: 'start', md: 'space-between' },
                  padding: '15px',
                  backgroundColor: 'white',
                  border: '1px solid rgba(223, 231, 240, 1)',
                  '&:hover': {
                    border: theme => `1px solid ${theme.palette.primary.main}`,
                    '& > span': { color: theme => theme.palette.primary.main },
                  },
                }}
              >
                <ListItemIcon>
                  <Box component={'img'} src={Ukraine.src} width={'25px'} height={'25px'}></Box>
                </ListItemIcon>
                {mdDown ? (
                    <a href="tel:+38-099-183-34-94">
                  <Box>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.ua' })}
                    </Box>
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +38 099 183-34-94
                    </ListItemText>
                  </Box>
                    </a>
                ) : (
                  <>
                  <a href="tel:+38-099-183-34-94">
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +38 099 183-34-94
                    </ListItemText>
                  </a>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.ua' })}
                    </Box>
                  </>
                )}
              </ListItem>
              <ListItem
                sx={{
                  marginTop: '10px',
                  justifyContent: { xs: 'start', md: 'space-between' },
                  padding: '15px',
                  backgroundColor: 'white',
                  border: '1px solid rgba(223, 231, 240, 1)',
                  '&:hover': {
                    border: theme => `1px solid ${theme.palette.primary.main}`,
                    '& > span': { color: theme => theme.palette.primary.main },
                  },
                }}
              >
                <ListItemIcon>
                  <Box
                    component={'img'}
                    src={CountrySlovak.src}
                    width={'25px'}
                    height={'25px'}
                  ></Box>
                </ListItemIcon>
                {mdDown ? (
                    <a href="tel:+421919196568">
                  <Box>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.sv' })}
                    </Box>
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +421 919 196 568
                    </ListItemText>

                  </Box>
                    </a>
                ) : (
                  <>
                  <a href="tel:+421919196568">
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +421 919 196 568
                    </ListItemText>
                  </a>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.sv' })}
                    </Box>

                  </>
                )}
              </ListItem>
              <ListItem
                sx={{
                  marginTop: '10px',
                  justifyContent: { xs: 'start', md: 'space-between' },
                  padding: '15px',
                  backgroundColor: 'white',
                  border: '1px solid rgba(223, 231, 240, 1)',
                  '&:hover': {
                    border: theme => `1px solid ${theme.palette.primary.main}`,
                    '& > span': { color: theme => theme.palette.primary.main },
                  },
                }}
              >
                <ListItemIcon>
                  <Box component={'img'} src={Italia.src} width={'25px'} height={'25px'}></Box>
                </ListItemIcon>
                {mdDown ? (
                    <a href="tel:+393519190937">
                  <Box>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.it' })}
                    </Box>
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +39 351 919 0937
                    </ListItemText>
                  </Box>
                    </a>
                ) : (
                  <>
                  <a href="tel:+393519190937">
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +39 351 919 0937
                    </ListItemText>
                  </a>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.it' })}
                    </Box>

                  </>
                )}
              </ListItem>
              <ListItem
                sx={{
                  marginTop: '10px',
                  justifyContent: { xs: 'start', md: 'space-between' },
                  padding: '15px',
                  backgroundColor: 'white',
                  border: '1px solid rgba(223, 231, 240, 1)',
                  '&:hover': {
                    border: theme => `1px solid ${theme.palette.primary.main}`,
                    '& > span': { color: theme => theme.palette.primary.main },
                  },
                }}
              >
                <ListItemIcon>
                  <Box
                    component={'img'}
                    src={Poland.src}
                    width={'25px'}
                    height={'25px'}
                    borderRadius={'30px'}
                  ></Box>
                </ListItemIcon>
                {mdDown ? (
                  <a href="tel:+48513355559">
                  <Box>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.pl' })}
                    </Box>
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +48 513 355 559
                    </ListItemText>
                  </Box>
                  </a>
                ) : (
                  <>
                  <a href="tel:+48513355559">
                    <ListItemText sx={{ '& > span': { fontSize: '16px', fontWeight: 600 } }}>
                      +48 513 355 559
                    </ListItemText>
                  </a>
                    <Box
                      component={'span'}
                      sx={{ color: 'rgba(20, 20, 20, 1)', fontStyle: 'italic' }}
                    >
                      {intl.$t({ id: 'general.contact.countries.pl' })}
                    </Box>

                  </>
                )}
              </ListItem>
            </List>
          </Box>

          <Box sx={mdDown ? { padding: '20px 0px' } : {}}>
            <ContactForm />
          </Box>
        </Box>
      </Container>
    </Box>
  )
}

export default Contacts

import React, { useState } from 'react'
import {
  Box,
  Button,
  Container,
  FormControl,
  FormControlLabel, FormLabel, InputAdornment,
  InputLabel,
  MenuItem, Radio, RadioGroup,
  Select, TextField,
  Typography,
} from '@mui/material'
import bgCalculator from '../../public/images/bgCalculator.png'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import { countries } from '@/components/countryComponents/CountryComponents'
import { Autocomplete } from '@mui/lab'
import handler from '../../pages/api/sent-to-bot'
import CallIcon from '../../pages/icons/CallIcon'

const Calculator = () => {
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const [selectedCountry, setSelectedCountry] = React.useState('');
  const [value, setValue] = React.useState('');
  const intl = useIntl()
  const [data, setData] = useState({
    from: '',
    phone: '',
    kilometer: '',
    interest: '',
  })

  const sentMessage = async () => {
    if (data.from.label === '' || data.phone === '') {
      return
    }

    try {
      await handler({
        body: {
          text: `------  Заявка  ------%0A%0AКраїна:  ${data.from.label}%0AНомер телефону: ${data.phone}%0AКількість кілометрів: ${data.kilometer}%0AВас цікавить: ${data.interest}%0A%0A------  Кінець заявки  ------`,
        },
      })

      setData({ from: '', phone: '', kilometer: '' , interest: '' })
    } catch (e) {
    }
  }

  const handleChange = (event, newValue) => {
    setData(prev => ({
      ...prev,
      from: newValue
    }));
  };

  console.log(data)

  return (
    <Box sx={{ backgroundImage: `url(${bgCalculator.src})`, padding: '70px 0px' }}>
      <Container>
        <Typography
          sx={{
            fontSize: { xs: '24px', md: '32px' },
            fontWeight: '700',
            maxWidth: { xs: '100%', md: '400px' },
          }}
          variant={'h1'}
        >
          <Box component={'span'} sx={{ color: theme => theme.palette.primary.main }}>
            {intl.$t({ id: 'calculator.title' })}
          </Box> {intl.$t({ id: 'calculator.title1' })}
        </Typography>
        <Box
          sx={{
            display:'grid',
            gridTemplateColumns:{xs:'1fr',md: '1fr 1.3fr'},
          }}
        >
          <Box
            sx={{margin:'40px 30px 20px 0px'}}
          >
            <Typography sx={{fontSize:'16px',fontWeight:'600', marginBottom:'20px'}}>
              {intl.$t({ id: 'calculator.formControl1' })}
            </Typography>
            <FormControl
              fullWidth
              sx={{backgroundColor:'#181D24',}} >
              <Autocomplete
                renderOption={(props, option) => {
                  const { key, ...optionProps } = props;
                  return (<Box
                      {...optionProps}{...optionProps}
                      sx={{
                        display:'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        rowGap:'5px',
                        columnGap:'5px'
                      }}
                    >
                      <Box
                        component={'img'}
                        src={option.url}
                        alt={'how'}
                        sx={{ width: '20px', height: '15px' }}
                      />
                      {option.label}
                    </Box>
                  )}

              }
                options={countries.map(country => ({
                  ...country,
                  label: intl.$t({ id: `countries.country.${country.name}` }),
                  id: country.name
                }))}
                value={data.from}
                onChange={handleChange}
                renderInput={(params) => {
                  const selectedCountryIcon = countries.find(country => country.name === data.from?.id)?.url
                  return <TextField
                    {...params}
                    label={intl.$t({ id: 'calculator.country' })}
                    InputProps={{
                    ...params.InputProps,
                    startAdornment: (
                      <InputAdornment position='start'>
                        {selectedCountryIcon ?<Box
                          component={'img'}
                          src={selectedCountryIcon}
                          alt={'countyIcon'}
                          sx={{ width: '20px', height: '15px' }}
                        /> : <></>}
                      </InputAdornment>
                    ),
                  }} />
                }}
                >
              </Autocomplete>
            </FormControl>
            <Typography sx={{fontSize:'16px',fontWeight:'600',margin:'30px 0px 10px 0px'}}>
              {intl.$t({ id: 'calculator.textField' })}
            </Typography>
            <TextField
              fullWidth
              color={'primary'}
              sx={{
                backgroundColor:'#181D24',
                borderColor: 'black',
                color: 'grey',
                fieldset: { borderColor: 'rgba(201, 201, 201, 1)' },
              }}
              variant={'outlined'}
              value={data.kilometer}
              onChange={e => setData(prev => ({ ...prev, kilometer: e.target.value }))}
              InputProps={{
                placeholder: intl.$t({ id: 'calculator.kilometer' }),
                style: {
                  color: 'white',
                },
              }}
            />
          </Box>
          <Box
            sx={{marginTop:{xs:'40px',md:'40px'},marginBottom:{xs:'40px',md:'40px'},marginLeft:{xs:'10px',md:'0px'},marginRight:'0px'}}
          >
            <Typography
              sx={{fontSize:'16px',fontWeight:'600',marginBottom:'20px'}}
            >
              {intl.$t({ id: 'calculator.formControl2' })}
            </Typography>
            <FormControl
              onChange={e => setData(prev => ({ ...prev, interest: e.target.value }))}
              sx={{
                 display: 'flex',
                 flexDirection: 'column',
                 alignItems: 'start',
                 columnGap:'10px',
                 rowGap:'20px'
              }}
            >
              <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name="row-radio-buttons-group"
                sx={{display:'flex', flexDirection: 'column', rowGap:'20px'}}
              >
                <FormControlLabel
                  sx={{
                    backgroundColor:'#181D24',
                    fontSize:'12px',padding:'7px 40px 7px 10px'
                }}
                  value="Перевезення труни з тілом"
                  control={<Radio />}
                  label={intl.$t({ id: 'calculator.radio1' })}
                />
                <FormControlLabel
                  sx={{
                    backgroundColor:'#181D24',
                    fontSize:'12px',padding:'7px 20px 7px 5px'
                }}
                  value="Перевезення праху"
                  control={<Radio />}
                  label={intl.$t({ id: 'calculator.radio2' })}
                />
              </RadioGroup>
            </FormControl>
            <Typography  sx={{fontSize:'16px',fontWeight:'600',marginBottom:'10px',marginTop:'30px'}}>
              {intl.$t({ id: 'calculator.textField2' })}
            </Typography>
            <TextField
              fullWidth
              variant={'outlined'}
              value={data.phone}
              sx={{ backgroundColor:'#181D24', fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
              onChange={e => setData(prev => ({ ...prev, phone: e.target.value }))}
              InputProps={{
                placeholder: intl.$t({ id: 'general.contactForm.textFiled2' }),
                startAdornment: (
                  <InputAdornment position="start">
                    <CallIcon />
                  </InputAdornment>
                ),
                style: {
                  color: 'white',
                },
              }}
            />
            <Button
              fullWidth
              sx={{
                boxShadow: ' rgba(249, 205, 150, 1) 2px 1px 15px',
                marginTop:'30px',
                backgroundColor: 'rgba(249, 205, 150, 1)',
                borderRadius: '0px',
                color: 'rgb(3,0,0)',
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'rgba(241,182,132,0.44)' },
              }}
              onClick={sentMessage}
            >
              {intl.$t({ id: 'calculator.button' })}
            </Button>
          </Box>
        </Box>
      </Container>
      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} isQuestion />
    </Box>
  )
}

export default Calculator

import React from 'react'
import { Box, Button, Container, Typography } from '@mui/material'
import BlogCard from '@/components/blog/BlogCard'
import { useIntl } from 'react-intl'
import Footer from '@/components/layout/Footer'
import Contacts from '@/components/contacts/Contacts'

const BlogTitle = () => {
  const intl = useIntl()
  return (
    <Box sx={{ backgroundColor: 'rgba(24, 29, 36, 1)' }}>
      <Container
        maxWidth={'lg'}
        sx={{
          margin: 'auto',
          px: { xs: '10px', md: '50px' },
          alignItems: 'center',
          justifyContent: 'space-between',
          color: 'white',
          display: 'grid',
          gridTemplateColumns: { xs: '1fr', md: '1fr   ' },
        }}
      >
        <a name={'Blog'}>
          <Typography
            sx={{
              color: 'white',
              fontSize: '32px',
              fontWeight: 600,
              textAlign: 'center',
              marginBottom: '30px',
              marginTop: '30px',
            }}
          >
            Блог
          </Typography>
        </a>
        <BlogCard />
        <Button
          href="/blog"
          sx={{
            boxShadow: ' rgba(249, 205, 150, 1) 2px 1px 15px',
            backgroundColor: 'rgba(249, 205, 150, 1)',
            color: 'rgba(0, 0, 0, 1)',
            width: '350px',
            textTransform: 'none',
            fontSize: '14px',
            fontWeight: '600',
            padding: '20px 30px',
            '&:hover': { background: 'white' },
            marginLeft: 'auto',
            marginRight: 'auto',
            marginBottom: '50px',
          }}
        >
          {intl.$t({ id: 'blog.blogTitle.button' })}
        </Button>
      </Container>
    </Box>
  )
}

export default BlogTitle

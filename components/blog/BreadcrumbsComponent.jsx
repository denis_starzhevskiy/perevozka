import React from 'react'
import { Breadcrumbs, Container, Typography } from '@mui/material'
import Link from 'next/link'
import Box from '@mui/material/Box'

const BreadcrumbsComponent = ({ path = [] }) => {
  return (
    <Box
      sx={{
        borderTop: '1px solid rgba(90, 82, 99, 1)',
        borderBottom: '1px solid rgba(90, 82, 99, 1)',
      }}
    >
      <Container sx={{ padding: '10px' }}>
        <Breadcrumbs separator={'>'} itemscope itemtype="https://schema.org/BreadcrumbList">
          {path.map((item, idx) => {
            if (idx === path.length - 1) {
              return (
                <Typography
                  itemscope
                  itemtype="https://schema.org/ListItem"
                  key={idx}
                  sx={{ fontWeight: 600, color: 'rgba(249, 205, 150, 1)' }}
                >
                  {item.label}
                </Typography>
              )
            }

            return (
              <Link
                itemprop="item"
                itemscope
                itemtype="https://schema.org/ListItem"
                key={idx}
                underline="hover"
                href={item.link}
                style={{ color: 'rgba(249, 205, 150, 1)' }}
              >
                {item.label}
              </Link>
            )
          })}
        </Breadcrumbs>
      </Container>
    </Box>
  )
}

export default BreadcrumbsComponent
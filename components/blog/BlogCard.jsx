import React from 'react'
import { Box, Button, Card, CardActions, CardContent, CardMedia, Typography } from '@mui/material'
import blogPreview from 'public/images/bllog.JPG'
import blogPreview1 from '../../public/images/blogg.JPG'
import { useRouter } from 'next/router'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward'
import moment from 'moment'
import { useIntl } from 'react-intl'

export const blogs = [
  {
    name: 'first.name',
    description: 'first.description',
    date: 'first.date',
    image: blogPreview,
    link:'blog1'
  },
  {
    name: 'second.name',
    description: 'second.description',
    date: 'second.date',
    image: blogPreview1,
    link:'blog2'
  },
]

const BlogCard = ({ blogData, isPresentation }) => {
  const router = useRouter()
  const intl = useIntl()

  return (
    <Card
      sx={{
        height: '533px',
        background: 'white',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <CardMedia
        sx={{ height: '50%' }}
        image={blogData.image.src}
        title="green iguana"
      />
      <CardContent sx={{ display: 'flex', flexDirection: 'column', rowGap: '10px' }}>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          sx={{ color: 'black', fontWeight: 600 }}
        >
          {intl.$t({ id: `blog.blogs.${blogData.name}` })}
        </Typography>
        <Typography variant="body2" sx={{ color: 'black', fontSize: '15px', fontWeight: '500' }}>
          {intl.$t({ id: `blog.blogs.${blogData.description}` })}
        </Typography>
      </CardContent>
      <CardActions
        sx={{ paddingBottom: '20px', marginTop: 'auto', justifyContent: 'space-between' }}
      >
        <Button
          startIcon={<CalendarMonthIcon />}
          size="small"
          sx={{ color: 'rgba(124, 141, 181, 1)' }}
        >
          {moment(`${intl.$t({ id: `blog.blogs.${blogData.date}` })}`).isValid()
            ? moment(`${intl.$t({ id: `blog.blogs.${blogData.date}` })}`).locale('uk').format('LL')
            : `${intl.$t({ id: `blog.blogs.${blogData.date}` })}`}
        </Button>
        {!isPresentation && (
          <Button
            size="small"
            endIcon={<ArrowOutwardIcon />}
            sx={{
              textTransform: 'none',
            }}
            onClick={() => router.push(`/blog/${blogData.link}`)}
          >
            {intl.$t({ id: 'blog.blogs.button' })}
          </Button>
        )}
      </CardActions>
    </Card>
  )
}

const Index = () => {
  return (
    <Box>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr' },
          columnGap: '10px',
          rowGap: '30px',
          marginBottom: '70px',
          marginRight:{ xs: '20px', md: '0px' },
          marginLeft:{ xs: '20px', md: '0px' }
        }}
      >
        {blogs.map((item, idx) => {
          return <BlogCard key={idx} blogData={{ id: idx, ...item }} />
        })}
      </Box>
    </Box>
  )
}

export default Index

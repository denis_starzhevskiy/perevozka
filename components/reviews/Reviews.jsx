import React, { useEffect, useRef, useState } from 'react'
import { Box, Button, Container, Rating, Typography, useMediaQuery } from '@mui/material'
import ArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft'
import { Swiper, SwiperSlide } from 'swiper/react'
import { A11y, Navigation, Pagination, Scrollbar } from 'swiper/modules'
import ArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import avatar from 'public/images/feedbackAvatar.png'
import avatar5 from 'public/images/avatar5.png'
import avatar3 from 'public/images/avatar3.png'
import avatar4 from 'public/images/avatar4.png'
import avatar6 from 'public/images/avatar6.png'
import avatar8 from 'public/images/avatar8.png'
import backImage from 'public/images/backgroundReviews.png'
import angelImage from 'public/images/angelImage.png'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'
import 'swiper/css'

const reviews = [
  [
    {
      name: 'one.name',
      rating: 5,
      feedback: 'one.feedback',
      avatar: avatar5.src,
      date: 'one.date',
    },
    {
      name: 'two.name',
      rating: 5,
      feedback: 'two.feedback',
      avatar: avatar.src,
      date: 'two.date',
    },
  ],
  [
    {
      name: 'three.name',
      rating: 5,
      feedback: 'three.feedback',
      avatar: avatar3.src,
      date: 'three.date',
    },
    {
      name: 'four.name',
      rating: 5,
      feedback: 'four.feedback',
      avatar: avatar4.src,
      date: 'four.date',
    },
  ],
  [
    {
      name: 'five.name',
      rating: 5,
      feedback: 'five.feedback',
      avatar: avatar8.src,
      date: 'five.date',
    },
    {
      name: 'six.name',
      rating: 5,
      feedback: 'six.feedback',
      avatar: avatar6.src,
      date: 'six.date',
    },
  ],
]
const Reviews = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [openDialog, setOpenDialog] = useState(false)
  const [swiper, setSwiper] = useState()
  const prevRef = useRef()
  const nextRef = useRef()
  const intl = useIntl()

  useEffect(() => {
    if (swiper) {
      swiper.params.navigation.prevEl = prevRef.current
      swiper.params.navigation.nextEl = nextRef.current
      swiper.navigation.init()
      swiper.navigation.update()
    }
  }, [swiper])



  return (
    <>
      <Box
        sx={{
          backgroundColor: 'rgba(249, 249, 249, 1)',
          display: 'flex',
          flexDirection: 'column',
          rowGap: '30px',
          padding: { xs: '70px 10px', md: '70px' },
        }}
      >
        <a name={'Feedbacks'}>
          <Typography
            sx={{
              color: 'black',
              fontSize: { xs: '24px', md: '32px' },
              fontWeight: 600,
              textAlign: 'center',
            }}
          >
            {intl.$t({ id: 'review.title' })}
          </Typography>
        </a>
        <Typography sx={{ color: 'black', textAlign: 'center', fontSize: '16px' }}>
          {intl.$t({ id: 'review.subTitle' })}
        </Typography>
        <Box sx={{ display: 'flex', columnGap: '10px', alignItems: 'center' }}>
          {!mdDown && (
            <Box ref={prevRef}>
              <ArrowLeftIcon
                sx={{
                  fontSize: '30px',
                  color: theme => theme.palette.primary.main,
                  border: theme => `1px solid ${theme.palette.primary.main}`,
                  borderRadius: '50%',
                  width: '40px',
                  height: '40px',
                  '&:hover': {
                    backgroundColor: 'rgba(207, 137, 60, 1)',
                    color: 'white',
                  },
                }}
              />
            </Box>
          )}
          <Container maxWidth={'lg'}>
              <Swiper
              breakpoints={{
                0: {
                  slidesPerView: 1,
                },
                900: {
                  slidesPerView: 2,
                },
              }}
              slidesPerView={2}
              modules={[Navigation, Pagination, Scrollbar, A11y]}
              spaceBetween={'10px'}
              pagination={{ clickable: true }}
              scrollbar={{ draggable: true }}
              onSlideChange={() => console.log('slide change')}
              navigation={{
                prevEl: prevRef?.current,
                nextEl: nextRef?.current,
              }}
              updateOnWindowResize
              observer
              observeParents
              onSwiper={setSwiper}
            >
              {reviews.map((review, idx) => (
                <SwiperSlide key={idx}>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      rowGap: '10px',
                      // padding: '20px',
                    }}
                  >
                    {review.map((oneReview, key) => {
                      return (
                        <Box
                          key={key}
                          sx={{
                            backgroundColor: 'white',
                            border: '1px solid rgba(223, 231, 240, 1)',
                            padding: '30px',
                            display: 'flex',
                            flexDirection: 'column',
                            rowGap: '20px',
                          }}
                        >
                          <Box
                            sx={{
                              display: 'flex',
                              justifyContent: 'space-between',
                              alignItems: 'start',
                              width: '100%',
                            }}
                          >
                            <Box sx={{ display: 'flex', columnGap: '10px' }}>
                              <Box
                                component={'img'}
                                src={oneReview.avatar}
                                width={40}
                                height={40}
                                alt={''}
                              />
                              <Box>
                                <Typography
                                  sx={{ fontWeight: 600, color: 'black', fontSize: '15px' }}
                                >
                                  {intl.$t({ id: `review.review.${oneReview.name}` })}
                                </Typography>
                                <Box sx={{ color: 'rgba(140, 146, 157, 1)', fontSize: '12px' }}>
                                  {intl.$t({ id: `review.review.${oneReview.date}` })}
                                </Box>
                              </Box>
                            </Box>
                            <Rating name="reviewRating" value={oneReview.rating} readOnly />
                          </Box>
                          <Typography sx={{ color: 'black', fontSize: '14px' }}>
                            {intl.$t({ id: `review.review.${oneReview.feedback}` })}
                          </Typography>
                        </Box>
                      )
                    })}
                  </Box>
                </SwiperSlide>
              ))}
            </Swiper>
          </Container>
          {!mdDown && (
            <Box ref={nextRef}>
              <ArrowRightIcon
                sx={{
                  fontSize: '20px',
                  color: theme => theme.palette.primary.main,
                  border: theme => `1px solid ${theme.palette.primary.main}`,
                  borderRadius: '50%',
                  width: '40px',
                  height: '40px',
                  '&:hover': {
                    backgroundColor: 'rgba(207, 137, 60, 1)',
                    color: 'white',
                  },
                }}
              />
            </Box>
          )}
        </Box>
      </Box>

      <Box
        sx={{
          background: 'linear-gradient(180deg, rgba(249, 249, 249, 1) 50%, white 50%)',
          padding: '10px 0px 100px 0px',
        }}
      >
        <Container maxWidth={'lg'} sx={{ marginTop: '100px' }}>
          <Box
            sx={{
              backgroundImage: `url(${backImage.src})`,
              width: '100%',
              minHeight: '270px',
              position: 'relative',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              padding: { xs: '20px', md: '50px' },
              rowGap: '30px',
            }}
          >
            <Typography
              sx={{
                fontSize: '16px',
                fontStyle: 'italic',
                maxWidth: { xs: '100%', md: '60%' },
                textAlign: { xs: 'center', md: 'start' },
              }}
            >
              {intl.$t({ id: 'review.text' })}
            </Typography>
            <Button
              sx={{
                boxShadow: ' rgba(249, 205, 150, 1) 2px 1px 15px',
                backgroundColor: 'rgba(249, 205, 150, 1)',
                color: 'rgba(0, 0, 0, 1)',
                width: { xs: '370px', md: '400px' },
                textTransform: 'none',
                fontSize: '14px',
                fontWeight: '600',
                padding: '20px 30px',
                '&:hover': { background: 'white' },
              }}
              onClick={() => setOpenDialog(true)}
            >
              {intl.$t({ id: 'review.button' })}
            </Button>
            {!mdDown && (
              <Box
                component={'img'}
                src={angelImage.src}
                sx={{ position: 'absolute', bottom: 0, right: 0 }}
              ></Box>
            )}
          </Box>
        </Container>
        <ContactDialog open={openDialog} onClose={() => setOpenDialog(false)} />
      </Box>
    </>
  )
}

export default Reviews

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Container,
  Divider,
  Drawer,
  Typography,
} from '@mui/material'
import React from 'react';
import faq from '../../public/icons/faqArrow.svg'
const FAq = [
  {
    id: 1,
    question: '1.Чи можна перевезти тіло померлого в Україну під час воєнного стану?',
    answer:
      'Так, можна. Щодня ритуальні катафалки повертають померлих на батьківщину для поховання.',
    },
  {
    id: 2,
    question: '2.Чи може родина самостійно організувати перевезення тіла з Німеччини в Україну?',
    answer: 'Не можна, адже законодавство Німеччини регламентує кожен крок надання ритуальних послуг та організації міжнародних перевезень померлих. Такі послуги надають лише спеціалізовані ліцензовані компанії.'
  },
  {
    id: 3,
    question: '3.Як швидко забрати тіло померлого з Німеччини?',
    answer: 'Щоб все відбулося швидше, потрібно звернутися до спеціалізованої агенції з відповідною ліцензією та досвідом роботи у цій сфері. Вони візьмуть на себе всі бюрократичні питання та безпосередньо перевезення тіла ритуальним катафалком. За сприятливих обставинах на це може піти від 2 до 5 днів.',
  },
  {
    id: 4,
    question: '4.Чи дозволять родині попрощатися з померлим у Німеччині?',
    answer: 'За бажанням можна влаштувати церемонію прощання з померлим у ритуальній залі, капличці чи церкві Німеччини. Однак варто повідомити про це під час консультації, щоб був час на узгодження деталей та підготовку.',
  },
  {
    id: 5,
    question: '5.Чи можна забрати особисті речі померлого в Німеччині?',
    answer: 'Так, можна, якщо це документ, телефон, ноутбук, рюкзак і про передачу було домовлено заздалегідь. Відправити до України велосипед, меблі, одяг краще спеціалізованим перевізником.',
    },
];

const FAQ = () => {

  return (
    <Box sx={{backgroundColor:'white', padding:'70px 0px'}}>
    <Container sx={{color:'black'}}>
      <Typography
        sx={{
          fontSize:'27px',
          fontWeight:'700',
          textAlign:'center'
        }}
      >
        FAQ
      </Typography>
      <Box
        sx={{
          display:'flex',
          flexDirection:'column',
          rowGap:'20px',
          padding:'40px',
        }}
      >
        {FAq.map((item, index) => (
            <Accordion
              key={index}
              sx={{
                backgroundColor:'#F9F9F9',
                padding:'30px 50px',
                color:'black',
                '&:hover': {
                  backgroundColor: 'rgba(209,184,152,0.2)',
                  border: '1px solid rgba(249, 205, 150, 1)',
                  width:'100%'
                },
              }}
            >
              <AccordionSummary
                expandIcon={<faq/>}
                sx={{
                  fontSize:'16px',
                  fontWeight:'700',
                }}
              >
                {item.question}
              </AccordionSummary>
              <AccordionDetails>
                {item.answer}
              </AccordionDetails>
            </Accordion>
        ))}
      </Box>
    </Container>
    </Box>
  );
};

export default FAQ;

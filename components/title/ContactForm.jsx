import React, { useState } from 'react'
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  InputAdornment,
  Stack,
  TextField,
  Tooltip,
  Typography,
  useMediaQuery,
} from '@mui/material'
import PersonIcon from '../../pages/icons/personIcon'
import CallIcon from '../../pages/icons/CallIcon'
import ViberColoredIcon from '../../pages/icons/ViberColoredIcon'
import FacebookColoredIcon from '../../pages/icons/FacebookColoredIcon'
import TelegramColoredIcon from '../../pages/icons/TelegramColoredIcon'
import InstagramColedIcon from '../../public/images/instaColedIcon.png'

import handler from '../../pages/api/sent-to-bot'
import CloseIcon from '@mui/icons-material/Close'
import { Autocomplete } from '@mui/lab'
import { countries } from '@/components/countryComponents/CountryComponents'
import LanguageIcon from '@mui/icons-material/Language'
import { useIntl } from 'react-intl'
import process from '../../next.config'


const ContactForm = ({ isDialog, isQuestion, chosenCountry, onClose }) => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [data, setData] = useState({
    name: '',
    phone: '',
    question: '',
    from: chosenCountry ? chosenCountry : null,
    to: null,
  })
  const [openSuccessDialog, setOpenSuccessDialog] = useState(false)
  const [checkedFrom, setCheckedFrom] = useState(!!chosenCountry)
  const [checkedTo, setCheckedTo] = useState(false)
  const intl = useIntl()
  const countryOptions = countries.map(elem => {
    return {
      name: intl.$t({ id: `countries.country.${elem.name}` }),
      value: elem.name,
    }
  })
  const sentMessage = async () => {
    if (data.name === '' || data.phone === '') {
      return
    }

    try {
      await handler({
        body: {
          text: isDialog
            ? isQuestion
              ? `------  Заявка  ------%0A%0AІм'я:  ${data.name}%0AНомер телефону: ${
                  data.phone
                }%0AКраїна відправки: ${
                  checkedFrom ? data.from : data.from.name
                }%0AКраїна доставки: ${checkedTo ? data.to : data.to.name}%0AЗадати питання : ${
                  data.question
                }%0A%0A------  Кінець заявки  ------`
              : `------  Заявка  ------%0A%0AІм'я:  ${data.name}%0AНомер телефону: ${data.phone}%0A%0A------  Кінець заявки  ------`
            : isQuestion
              ? `------  Заявка  ------%0A%0AІм'я:  ${data.name}%0AНомер телефону: ${
                  data.phone
                }%0AКраїна відправки: ${
                  checkedFrom ? data.from : data.from.name
                }%0AКраїна доставки: ${checkedTo ? data.to : data.to.name}%0AЗадати питання : ${
                  data.question
                }%0A%0A------  Кінець заявки  ------`
              : `------  Заявка  ------%0A%0AІм'я:  ${data.name}%0AНомер телефону: ${data.phone}%0A%0A------  Кінець заявки  ------`,
        },
      })

      setData({ name: '', phone: '', question: '' })
      if (isDialog) {
        onClose()
      } else {
        setOpenSuccessDialog(true)
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <Box
        sx={{
          marginTop: '0px',
          width: { xs: '100%', md: '440px' },
          height: 'max-content',
          backgroundColor: 'white',
          px: '115px',
          color: 'black',
          display: 'flex',
          flexDirection: 'column',
          rowGap: '10px',
          alignItems: 'center',
          padding: '30px',
          position: 'relative',
          border: '1px solid rgba(224, 224, 224, 1)',
        }}
      >
        {isDialog && (
          <CloseIcon sx={{ position: 'absolute', top: 10, right: 10 }} onClick={onClose} />
        )}
        <Typography sx={{ fontSize: '32px', fontWeight: 700 }} variant="h1">
          {intl.$t({ id: 'general.contactForm.title' })}
        </Typography>
        <Typography
          sx={{
            fontSize: '14px',
            fontWeight: '400',
            color: 'rgba(153, 157, 180, 1)',
            textAlign: 'center',
          }}
        >
          {intl.$t({ id: 'general.contactForm.subTitle' })}
        </Typography>
        <TextField
          color={'primary'}
          fullWidth
          sx={{
            borderColor: 'black',
            color: 'grey',
            fieldset: { borderColor: 'rgba(201, 201, 201, 1)' },
          }}
          variant={'outlined'}
          value={data.name}
          onChange={e => setData(prev => ({ ...prev, name: e.target.value }))}
          InputProps={{
            placeholder: intl.$t({ id: 'general.contactForm.textFiled1' }),
            startAdornment: (
              <InputAdornment position="start">
                <PersonIcon />
              </InputAdornment>
            ),
            style: {
              color: 'black',
            },
          }}
        />
        <TextField
          fullWidth
          variant={'outlined'}
          value={data.phone}
          sx={{ fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
          onChange={e => setData(prev => ({ ...prev, phone: e.target.value }))}
          InputProps={{
            placeholder: intl.$t({ id: 'general.contactForm.textFiled2' }),
            startAdornment: (
              <InputAdornment position="start">
                <CallIcon />
              </InputAdornment>
            ),
            style: {
              color: 'black',
            },
          }}
        />

        {isDialog && (
          <>
            {checkedFrom ? (
              <TextField
                fullWidth
                variant={'outlined'}
                value={data.from}
                sx={{ fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
                onChange={e => setData(prev => ({ ...prev, from: e.target.value }))}
                InputProps={{
                  placeholder: intl.$t({ id: 'general.contactForm.isDialog.textFiled' }),
                  startAdornment: (
                    <InputAdornment position="start">
                      <LanguageIcon color={'primary'} fontSize={'small'} />
                    </InputAdornment>
                  ),
                  style: {
                    color: 'black',
                  },
                }}
              />
            ) : (
              <Autocomplete
                sx={{ width: '100%' }}
                value={data.from}
                onChange={(e, value) => setData(prev => ({ ...prev, from: value }))}
                disablePortal
                id="countriesFrom"
                options={countryOptions}
                getOptionLabel={option => option.name}
                renderInput={params => (
                  <TextField
                    {...params}
                    sx={{ fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
                    InputLabelProps={{
                      style: { color: 'rgba(0,0,0,0.5)' },
                    }}
                    InputProps={{
                      ...params.InputProps,
                      style: {
                        color: 'black',
                      },
                    }}
                    label={intl.$t({ id: 'general.contactForm.isDialog.textFiled' })}
                  />
                )}
              />
            )}

            <FormControlLabel
              control={
                <Checkbox
                  color={'primary'}
                  disabled={!!chosenCountry}
                  sx={{
                    color: 'rgba(249, 205, 150, 1)',
                    '&.Mui-checked': {
                      color: 'rgba(249, 205, 150, 1)',
                    },
                  }}
                  checked={checkedFrom}
                  onChange={() => {
                    setData(prev => ({ ...prev, from: null }))
                    setCheckedFrom(prevState => !prevState)
                  }}
                  inputProps={{ 'aria-label': 'controlled' }}
                />
              }
              label={intl.$t({ id: 'general.contactForm.isDialog.autocomplete' })}
            />

            {checkedTo ? (
              <TextField
                fullWidth
                variant={'outlined'}
                value={data.to}
                sx={{ fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
                onChange={e => setData(prev => ({ ...prev, to: e.target.value }))}
                InputProps={{
                  placeholder: intl.$t({ id: 'general.contactForm.isDialog.textFiled' }),
                  startAdornment: (
                    <InputAdornment position="start">
                      <LanguageIcon color={'primary'} fontSize={'small'} />
                    </InputAdornment>
                  ),
                  style: {
                    color: 'black',
                  },
                }}
              />
            ) : (
              <Autocomplete
                sx={{ width: '100%' }}
                id="countriesTo"
                value={data.to}
                onChange={(e, value) => setData(prev => ({ ...prev, to: value }))}
                options={countryOptions}
                getOptionLabel={option => option.name}
                renderInput={params => (
                  <TextField
                    {...params}
                    sx={{ fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
                    InputLabelProps={{
                      style: { color: 'rgba(0,0,0,0.5)' },
                    }}
                    InputProps={{
                      ...params.InputProps,
                      style: {
                        color: 'black',
                      },
                    }}
                    label={intl.$t({ id: 'general.contactForm.isDialog.textFiled' })}
                  />
                )}
              />
            )}

            <FormControlLabel
              control={
                <Checkbox
                  color={'primary'}
                  sx={{
                    color: 'rgba(249, 205, 150, 1)',
                    '&.Mui-checked': {
                      color: 'rgba(249, 205, 150, 1)',
                    },
                  }}
                  checked={checkedTo}
                  onChange={() => {
                    setData(prev => ({ ...prev, to: null }))
                    setCheckedTo(prevState => !prevState)
                  }}
                  inputProps={{ 'aria-label': 'controlled' }}
                />
              }
              label={intl.$t({ id: 'general.contactForm.isDialog.autocomplete' })}
            />
          </>
        )}

        {isQuestion && (
          <TextField
            fullWidth
            variant={'outlined'}
            value={data.question}
            sx={{ fieldset: { borderColor: 'rgba(201, 201, 201, 1)' } }}
            onChange={e => setData(prev => ({ ...prev, question: e.target.value }))}
            InputProps={{
              placeholder: intl.$t({ id: 'general.contactForm.isQuestion.textFiled' }),
              startAdornment: <InputAdornment position="start"></InputAdornment>,
              style: {
                color: 'black',
              },
            }}
          />
        )}

        <Button
          fullWidth
          sx={{
            backgroundColor: 'rgba(249, 205, 150, 1)',
            borderRadius: '0px',
            color: 'rgb(3,0,0)',
            textTransform: 'none',
            fontSize: '14px',
            fontWeight: '600',
            padding: '20px 30px',
            '&:hover': { background: 'rgba(241,182,132,0.44)' },
          }}
          onClick={sentMessage}
        >
          {intl.$t({ id: 'general.contactForm.button' })}
        </Button>
        <Typography sx={{ fontSize: '12px' }}>
          {intl.$t({ id: 'general.contactForm.subButton' })}
        </Typography>
        {mdDown ? (
          <Stack sx={{ display: 'grid', gridTemplateColumns: '1fr 1fr 1fr 1fr ', gap: '2px' }}>
            <Tooltip title="+38-099-183-34-94" placement="top">
              <Box>
                <ViberColoredIcon />
              </Box>
            </Tooltip>
            <a href="https://www.facebook.com/pochovai.in.ua" target="_blank" rel="noreferrer">
              <FacebookColoredIcon />
            </a>
            <a href="https://t.me/perevozka_angel" target="_blank" rel="noreferrer">
              <TelegramColoredIcon />
            </a>
            <a
              href="https://www.instagram.com/pohovai.in.ua"
              target="_blank"
              rel="noreferrer"
              color={'black'}
            >
              {' '}
              <img src={InstagramColedIcon.src} height={'35px'} />
            </a>
          </Stack>
        ) : (
          <Stack sx={{ display: 'grid', gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr', gap: '2px' }}>
            <Tooltip title="+38-099-183-34-94" placement="top">
              <Box>
                <ViberColoredIcon />
              </Box>
            </Tooltip>
            <a href="https://www.facebook.com/pochovai.in.ua" target="_blank" rel="noreferrer">
              <FacebookColoredIcon />
            </a>
            <a href="https://t.me/perevozka_angel" target="_blank" rel="noreferrer">
              <TelegramColoredIcon />
            </a>
            <a
              href="https://www.instagram.com/pohovai.in.ua"
              target="_blank"
              rel="noreferrer"
              color={'black'}
            >
              {' '}
              <img src={InstagramColedIcon.src} height={'35px'} />
            </a>
          </Stack>
        )}
      </Box>
      <Dialog open={openSuccessDialog} onClose={() => setOpenSuccessDialog(false)}>
        <DialogTitle
          sx={{
            backgroundColor: theme => theme.palette.primary.main,
            color: 'white',
            textAlign: 'center',
          }}
        >
          {intl.$t({ id: 'general.contactForm.dialog1' })}
        </DialogTitle>
        <DialogContent sx={{ backgroundColor: 'white', padding: '20px' }}>
          <Typography sx={{ color: 'black', textAlign: 'center', py: '20px' }}>
            {intl.$t({ id: 'general.contactForm.dialog2' })}
          </Typography>
        </DialogContent>
      </Dialog>
    </>
  )
}

export async function getStaticProps() {
  console.log(process.env.BOT_TOKEN)
  return {
    props: {
      botId: process.env.BOT_TOKEN,
      chatId: process.env.BOT_CHAT_ID,
    },
  }
}

export default ContactForm

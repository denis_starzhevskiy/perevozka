import React, { useState } from 'react'
import { Box, Button, List, ListItem, ListItemIcon, ListItemText, Typography, useMediaQuery } from '@mui/material'
import EastIcon from '@mui/icons-material/East'
import BusAutoIcon from '../../pages/icons/busAutoIcon'
import MapIcon from '../../pages/icons/mapIcon'
import MedalIcon from '../../pages/icons/MedalIcon'
import ContactForm from '@/components/title/ContactForm'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'

const TitleComponent = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box
      sx={{
        position: 'relative',
        padding: '100px 0px',
        backgroundSize: 'cover',
        px: { xs: '5px', md: '115px' },
        display: 'flex',
        alignItems: 'center',
        justifyContent: { xs: 'center', md: 'space-between' },
      }}
    >
      <Box
        sx={{
          overflow: 'hidden',
          position:'absolute',
          top:'0',
          left:'0',
          width:'100%',
          height:'100%',
          overflowX: 'hidden',
          background:'#ccc no-repeat center center'
        }}
      >
        <Box component={'video'} src={'/images/bg.mp4'} autoPlay muted loop
             sx={{width: '100%', maxHeight: '100%', objectFit: 'cover',}}></Box>
      </Box>
      <Box
        sx={{
          overflow: 'hidden',
          position:'absolute',
          top:'0',
          left:'0',
          width:'100%',
          height:'100%',
          overflowX: 'hidden',
          backgroundColor: '#181D24',
          opacity: 0.8
        }}
      ></Box>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          rowGap: '30px',
          alignItems: { xs: 'center', md: 'start' },
        }}
      >
        {mdDown ? <Typography
          variant={'h1'}
          sx={{
            fontSize: { xs: '30px', md: '50px' },
            fontWeight: '800',
            textTransform: 'uppercase',
            textAlign: { xs: 'center', md: 'start' },
            zIndex: 100,
          }}
        >
          {intl.$t({ id: 'general.main.titleMd' })}<br />
          {intl.$t({ id: 'general.main.titleMd1' })}<br />
          {intl.$t({ id: 'general.main.titleMd2' })}
          <Box sx={{ color: 'rgba(249, 205, 150, 1)' }}>
            {intl.$t({ id: 'general.main.title2' })}
          </Box>
        </Typography> : <Typography
          variant={'h1'}
          sx={{
            fontSize: {xs:'30px',md:'50px'},
            fontWeight: '800',
            textTransform: 'uppercase',
            textAlign: { xs: 'center', md: 'start' },
            zIndex: 100
          }}
        >
          {intl.$t({ id: 'general.main.title' })}
          <Box sx={{ color: 'rgba(249, 205, 150, 1)' }}>
            {intl.$t({ id: 'general.main.title2' })}
          </Box>
        </Typography>}

        <List sx={{ display: 'flex', flexDirection: 'column', rowGap: '10px' }}>
          <ListItem sx={{ padding: 0 }}>
            <ListItemIcon>
              <BusAutoIcon />
            </ListItemIcon>
            <ListItemText sx={{ '& > span': { fontSize: '12px' } }}>
              {intl.$t({ id: 'general.main.subTitle1' })}
            </ListItemText>
          </ListItem>
          <ListItem sx={{ padding: 0 }}>
            <ListItemIcon>
              <MapIcon />
            </ListItemIcon>
            <ListItemText sx={{ '& > span': { fontSize: '12px', whiteSpace: 'nowrap' } }}>
              {intl.$t({ id: 'general.main.subTitle2' })}
            </ListItemText>
          </ListItem>
          <ListItem sx={{ padding: 0 }}>
            <ListItemIcon>
              <MedalIcon />
            </ListItemIcon>
            <ListItemText sx={{ '& > span': { fontSize: '12px' } }}>
              {intl.$t({ id: 'general.main.subTitle3' })}
            </ListItemText>
          </ListItem>
        </List>

        <Button
          sx={{
            boxShadow: ' rgba(249, 205, 150, 1) 2px 1px 15px',
            backgroundColor: 'rgba(249, 205, 150, 1)',
            color: 'rgba(0, 0, 0, 1)',
            width: '350px',
            textTransform: 'none',
            fontSize: '14px',
            fontWeight: '600',
            padding: '20px 30px',
            '&:hover': { background: 'white' },
          }}
          endIcon={<EastIcon />}
          onClick={() => setContactFormOpen(true)}
        >
          {intl.$t({ id: 'general.main.button' })}
        </Button>
      </Box>

      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} />
      {!mdDown && <ContactForm />}
    </Box>
  )
}

export default TitleComponent

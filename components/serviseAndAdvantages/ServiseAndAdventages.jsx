import React, { useState } from 'react'
import { Box, Button, Container, Divider, Typography, useMediaQuery } from '@mui/material'
import AngelServise from '../../public/images/AngelServise.png'
import Elipse from '../../pages/icons/elipse'
import serviceBack from 'public/images/serviceItemBackground.png'
import EastIcon from '@mui/icons-material/East'
import ContactDialog from '@/components/contactDialog/ContactDialog'
import { useIntl } from 'react-intl'

const services = [
  {
    coloredTitle: 'one.coloredTitle',
    title: 'one.title',
  },
  {
    coloredTitle: 'two.coloredTitle',
    title: 'two.title',
  },
  {
    coloredTitle: 'three.coloredTitle',
    title: 'three.title',
  },
  {
    coloredTitle: 'four.coloredTitle',
    title: 'four.title',
  },
  {
    coloredTitle: 'five.coloredTitle',
    title: 'five.title',
  },
  {
    coloredTitle: 'six.coloredTitle',
    title: 'six.title',
  },
]
const ServiceAndAdvantages = () => {
  const mdDown = useMediaQuery(theme => theme.breakpoints.down('md'))
  const [contactFormOpen, setContactFormOpen] = useState(false)
  const intl = useIntl()

  return (
    <Box sx={{ backgroundImage: `url(${AngelServise.src})`, padding: '70px 0px' }}>
      <Container
        maxWidth={'lg'}
        sx={{
          margin: 'auto',
          px: { xs: '10px', md: '10px' },
          alignItems: 'center',
          justifyContent: 'space-between',
          color: 'white',
          display: 'grid',
          gridTemplateColumns: { xs: '1fr', md: '1fr   ' },
        }}
      >
        {!mdDown && (
          <Box
            sx={{
              color:'#CF893C',
              columnGap:'20px',
              display:'flex',
              flexDirection:'row',
              alignItems:'center',
              }}>
            <Typography sx={{fontSize:'12px', fontWeight:'700'}}>03</Typography>
            <Divider sx={{ backgroundColor: 'rgba(217, 217, 217, 1)', margin: '20px 0px', width: '80px' }}  />
            <Typography sx={{fontSize:'12px', fontWeight:'700' , textTransform:'uppercase'}}>{intl.$t({ id: 'general.header.navigation.services' })}</Typography>
          </Box>
        )}
        <Box sx={{ display: 'grid', gridTemplateColumns: '1fr ' }} maxWidth={'lg'}>
          <a name={'Services'}>
            {' '}
            <Typography
              sx={{
                marginLeft: { xs: '10px', md: '0px' },
                fontSize: '32px',
                fontWeight: '700',
                mt: '30px',
              }}
              variant={'h1'}
            >
              {intl.$t({ id: 'serviseAndAdvantages.title' })}
            </Typography>
          </a>
          <Typography
            sx={{
              marginLeft: { xs: '10px', md: '0px' },
              fontSize: '16px',
              fontWeight: '500',
              mt: '30px',
              maxWidth: { xs: '100%', md: '50%' },
              lineHeight: '24px',
            }}
            variant={'h4'}
          >
            {intl.$t({ id: 'serviseAndAdvantages.text1' })}{' '}
            <span style={{ color: '#CF893C' }}>
              {intl.$t({ id: 'serviseAndAdvantages.text2' })}
            </span>
          </Typography>
        </Box>
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: { xs: '1fr', md: '1fr 1fr 1fr  ' },
            marginTop: '40px',
            marginBottom: '40px',
            marginLeft: { xs: '10px', md: '00px' },
          }}
        >
          {services.map((service, idx) => {
            return (
              <Box
                key={idx}
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  rowGap: '15px',
                  backgroundImage: `url(${serviceBack.src})`,
                  backgroundSize: 'cover',
                  backgroundRepeat: 'no-repeat',
                  margin: '5px',
                  padding: '45px 15px 20px 15px',
                }}
              >
                <Elipse width={50} height={20} />
                <Typography sx={{ textAlign: 'center', fontSize: '14px' }}>
                  <span style={{ color: '#CF893C' }}>
                    {intl.$t({ id: `serviseAndAdvantages.services.${service.coloredTitle}` })}
                  </span>{' '}
                  {intl.$t({ id: `serviseAndAdvantages.services.${service.title}` })}
                </Typography>
              </Box>
            )
          })}
        </Box>
        <Button
          sx={{
            boxShadow: ' rgba(249, 205, 150, 1) 2px 1px 15px',
            backgroundColor: 'rgba(249, 205, 150, 1)',
            color: 'rgba(0, 0, 0, 1)',
            width: '350px',
            textTransform: 'none',
            fontSize: '14px',
            fontWeight: '600',
            padding: '20px 30px',
            '&:hover': { background: 'white' },
            marginLeft: 'auto',
            marginRight: 'auto',
          }}
          endIcon={<EastIcon />}
          onClick={() => setContactFormOpen(true)}
        >
          {intl.$t({ id: 'serviseAndAdvantages.button' })}
        </Button>
      </Container>
      <ContactDialog open={contactFormOpen} onClose={() => setContactFormOpen(false)} isQuestion />
    </Box>
  )
}

export default ServiceAndAdvantages

import { createTheme } from '@mui/material/styles'

const lightTheme = createTheme({
  typography: {
    fontFamily: [
      'Gotham Pro',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    mode: 'dark',
    primary: {
      main: 'rgba(249, 205, 150, 1)',
    },
  },
})

export default lightTheme

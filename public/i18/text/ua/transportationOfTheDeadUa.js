export const transportationOfTheDeadUa = {
  title: 'Транспортування померлих з-за кордону з компанією ',
  subTitle:
    'Громадяни України можуть стати жертвами злочинів, загинути в автокатастрофі або померти на\n' +
    'роботі чи від хвороби. Родичам цих людей потрібно виконати складні завдання по\n' +
    'транспортуванню померлих, бо їхні тіла знаходяться далеко від Батьківщини. Щоб мати змогу\n' +
    'організувати таке перевезення з-за кордону, потрібно виконати певні дії:',
  steps: {
    step1: {
      title: 'Крок 01.',
      text: 'Зв’язатись з менеджером компанії.',
      text2:
        'На сайті є всі контакти, які можна використати, щоб зателефонувати до головного офісу в Україні або до закордонних представництв.',
    },
    step2: {
      title: 'Крок 02.',
      text: 'Надати всі потрібні документи.',
      text2: ' Це має бути дозвіл на перевезення померлого родича з-за кордону на Батьківщину.',
    },
    step3: {
      title: 'Крок 03.',
      text: 'Укласти відповідну угоду з представниками компанії. ',
      text2:
        'В договорі мають бути прописані всі умови, права та обов’язки сторін, а також передбачені всі ризики щодо перевезення померлих громадян України та інших міжнародних ритуальних послуг з інших країн.',
    },
    step4: {
      title: 'Крок 04.',
      text: 'Оплатити ритуальні послуги. ',
      text2: 'Це можна зробити будь-яким чином згідно з пунктами укладеної угоди.',
    },
  },
}

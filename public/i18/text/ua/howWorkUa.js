export const howWorkUa = {
  howWork: {
    title: 'Як ми працюємо',
    subTitle: 'Перевезення померлих з країн Європи',
    subText1:
      'Останнім часом нікого не здивуєш великою кількістю українців за кордоном. Вони\n' +
      'виїжджають до Європи до родичів або на працевлаштування, щоб заробити грошей для\n' +
      'певної мети. Але, на жаль, трапляються смерті громадян України на території інших\n' +
      'країн Європи. Тому перед родичами постають складні завдання по перевезенню померлих\n' +
      'додому для здійснення ритуальних послуг. Для цього потрібно звернутись до відповідних\n' +
      'компаній, які займаються міжнародними перевезеннями тіл громадян України, померлих за\n' +
      'кордоном.',
    subText2:
      'У такому разі важливий відповідальний підхід до вибору компанії, що надає вищевказані\n' +
      'послуги. На фірмі мають працювати кваліфіковані фахівці, які візьмуться за справу,\n' +
      'пов’язану з організацією транспортування тіл померлих або їхнього поховання.',
    button: 'Дізнатись вартість перевезення',
  },
  internationalServices: {
    title: 'Міжнародні ритуальні послуги',
    subText:
      'Громадяни України можуть стати жертвами злочинів, загинути в автокатастрофі або\n' +
      'померти на роботі чи від хвороби. Родичам цих людей потрібно виконати складні\n' +
      'завдання по транспортуванню померлих, бо їхні тіла знаходяться далеко від\n' +
      'Батьківщини. Щоб мати змогу організувати таке перевезення з-за кордону, потрібно\n' +
      'виконати певні дії:',
    list: {
      firstBold: 'Повідомити про смерть МЗС',
      first:
        'або представництво України у певній країні, де знаходився померлий родич. Потрібно\n' +
        'вказати ПІБ, його дату народження, місце реєстрації та проживання на території\n' +
        'України, номер закордонного паспорту та контакти найближчих родичів в Україні та\n' +
        'в інших країнах (якщо є).',
      secondBold: 'Надати всі потрібні документи.',
      second: 'Це має бути дозвіл на перевезення померлого родича з-за кордону на Батьківщину.',
      thirdBold: 'Оформити документи про факт смерті.',
      third: 'потрібно робити згідно з нормами законодавства України або закордонного права.',
      fourthBold: 'Надати дозвіл компанії.',
      fourth:
        'Люди можуть звернутись до певної компанії, яка транспортує тіло померлого в Україну або\n' +
        'організує поховання в країні, де це сталося.',
    },
    subText2:
      'У випадку смерті родича за кордоном потрібно звернутись до компанії, яка має великий\n' +
      'досвід щодо організації міжнародних ритуальних послуг. Фірма повинна мати широкий\n' +
      'штат кваліфікованих фахівців та надавати послуги за економічно обґрунтованими\n' +
      'цінами. Також вони повинні надати професійну консультацію щодо транспортування або\n' +
      'поховання тіл.',
    button: {
      mdUp: 'Безкоштовна консультація',
      mdDown: 'Написати нам',
    },
  },
  new:{
    one:'Заснування компанії. Ми заснували POHOVAI IN UA, щоб забезпечити можливість поховати близьких на рідній землі.',
    two:'Купівля спеціалізованого, ритуального транспорту з рефрижераторами, щоб забезпечувати найвищий рівень послуг.',
    three:'Офіційна співпраця з консульствами України в усіх країнах Європи.',
    four:'Розширення команди. Понад 20 професіоналів готові надати міжнародні ритуальні послуги на найвищому рівні.',
  }
}

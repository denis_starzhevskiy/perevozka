export const transportationOfTheDeadRu = {
  title: 'Транспортировка умерших из-за границы с компанией',
  subTitle:
    'Граждане Украины могут стать жертвами преступлений, погибнуть в автокатастрофе или умереть на работе или от болезни. Родственникам этих людей нужно выполнить сложные задачи по транспортировке погибших, потому что их тела находятся далеко от Родины. Чтобы иметь возможность организовать такую транспортировку за границу, нужно выполнить определенные действия:',
  steps: {
    step1: {
      title: 'Шаг 01.',
      text: 'Связаться с менеджером компании.',
      text2:
        'На сайте есть все контакты, которые можно использовать, чтобы позвонить в главный офис в Украине или в зарубежные представительства.',
    },
    step2: {
      title: 'Шаг 02.',
      text: 'Предоставить все необходимые документы.',
      text2:
        'Это должно быть разрешение на транспортировку умершего родственника за границу на Родину.',
    },
    step3: {
      title: 'Шаг 03.',
      text: 'Заключить соответствующий договор с представителями компании.',
      text2:
        'В договоре должны быть прописаны все условия, права и обязанности сторон, а также предусмотрены все риски по транспортировке погибших граждан Украины и других международных ритуальных услуг из других стран.',
    },
    step4: {
      title: 'Шаг 04.',
      text: 'Оплатить ритуальные услуги.',
      text2: 'Это можно сделать любым способом в соответствии с пунктами заключенного договора.',
    },
  },
}

import { LOCALES } from '../locales'
import { countriesUa } from './ua/countriesUa'
import { countriesRu } from './ru/countryRu'
import { pagesUA } from './ua/generalUa'
import { howWorkUa } from './ua/howWorkUa'
import { reviewsUa } from './ua/reviewsUa'
import { serviseAndAdvantagesUa } from './ua/serviseAndAdvantagesUa'
import { transportationOfTheDeadUa } from './ua/transportationOfTheDeadUa'
import { whyUsUa } from './ua/whyUsUa'
import { RuPages } from './ru/generalRu'
import { howWorkRu } from './ru/howWorkRu'
import { reviewsRu } from './ru/reviewsRu'
import { serviseAndAdvantagesRu } from './ru/serviseAndAdvantagesRu'
import { transportationOfTheDeadRu } from './ru/transportationOfTheDeadRu'
import { whyUsRu } from './ru/whyUsRu'
import { blogUa } from './ua/blogUa'
import { blogRu } from './ru/blogRu'
import { RitualAttributesUa } from './ua/RitualAttributesUa'
import { RitualAttributesRu } from './ru/RitualAttributesRu'
import { CalculatorUa } from './ua/calculatorUa'
import { calculatorRu } from './ru/calculatorRu'
import { czechPagesUa } from './ua/czechPagesUa'
import { czechPagesRu } from './ru/czechPagesRu'
export const messages = {
  [LOCALES.UA]: {
    countries: countriesUa,
    general: pagesUA,
    howWork: howWorkUa,
    czech: czechPagesUa,
    calculator: CalculatorUa,
    review: reviewsUa,
    RitualAttributes: RitualAttributesUa,
    serviseAndAdvantages: serviseAndAdvantagesUa,
    transportation: transportationOfTheDeadUa,
    whyUs: whyUsUa,
    blog: blogUa,
  },
  [LOCALES.RU]: {
    countries: countriesRu,
    general: RuPages,
    howWork: howWorkRu,
    calculator: calculatorRu,
    czech: czechPagesRu,
    review: reviewsRu,
    RitualAttributes: RitualAttributesRu,
    serviseAndAdvantages: serviseAndAdvantagesRu,
    transportation: transportationOfTheDeadRu,
    whyUs: whyUsRu,
    blog: blogRu,
  },
}

import Strapi from "strapi-sdk-js";

const strapi = new Strapi({
    url: process.env.NEXT_PUBLIC_CMS_URL || "http://localhost:1337",
});


export { strapi }
import { strapi } from './index'

export const getRitualAttributes = async () => {
  return await strapi.find('ritual-attributes', {
    populate: '*',
    pagination: { pageSize: 50 },
  })
}

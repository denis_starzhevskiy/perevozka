import React, { useEffect } from 'react'
import { IntlProvider } from 'react-intl'
import { LOCALES } from '../public/i18/locales'
import { messages } from '../public/i18/text/texts'
import { useLanguageStore } from './StateProvider'

const flattenMessages = (nestedObject, prefix = '') => {
  return Object.keys(nestedObject).reduce((flattened, key) => {
    const value = nestedObject[key]
    const prefixedKey = prefix ? `${prefix}.${key}` : key
    if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
      const nestedFlattened = flattenMessages(value, prefixedKey)
      Object.assign(flattened, nestedFlattened)
    } else {
      flattened[prefixedKey] = value
    }
    return flattened
  }, {})
}

const InternalizationProvider = ({ children }) => {
  const language = useLanguageStore(state => state.language)
  const changeLanguage = useLanguageStore(state => state.changeLanguage)

  console.log(language)
  useEffect(() => {
    const currentLanguage = localStorage.getItem('lang')
    if (!!currentLanguage && currentLanguage === LOCALES.RU) {
      changeLanguage(LOCALES.RU)
    } else {
      changeLanguage(LOCALES.UA)
    }
  }, [])

  console.log(flattenMessages(messages[language]))
  return (
    <IntlProvider
      locale={language}
      messages={flattenMessages(messages[language])}
      defaultLocale={LOCALES.UA}
    >
      {children}
    </IntlProvider>
  )
}

export default InternalizationProvider

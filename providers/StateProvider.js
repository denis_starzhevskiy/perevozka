import { create } from 'zustand'
import { LOCALES } from '../public/i18/locales'

export const useLanguageStore = create(set => ({
  language: LOCALES.UA,
  changeLanguage: lang => set(state => ({ language: lang })),
}))

// useLanguageStore.subscribe(state => {
//     localStorage.setItem('lang', state.language)
// })

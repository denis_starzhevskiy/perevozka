const isNullObjectKey = (i) => {
  let obj = i;

  for (var i in obj) {
    if (obj[i] === null) {
      delete obj[i];
    }
  }
  return obj;
};

const filterKeys = (array, filterKey) => {
  return array.filter(function (item) {
    for (let key in filterKey) {
      if (!item.attributes?.[key].includes(filterKey[key])) return false;
    }
    return true;
  });
};

export { filterKeys, isNullObjectKey };
